﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Specialized;
using System.Threading.Tasks;

namespace QuartzWithMySql
{
    class Program
    {
        static void Main(string[] args)
        {
            IntQuartz();
            Insert();

        }
        public static IScheduler Scheduler { get; set; }
        static void IntQuartz()
        {
            //1.首先创建一个作业调度池
            var properties = new NameValueCollection();
            //存储类型
            properties["quartz.jobStore.type"] = "Quartz.Impl.AdoJobStore.JobStoreTX,Quartz";
            //表明前缀
            properties["quartz.jobStore.tablePrefix"] = "QRTZ_";
            //驱动类型
            properties["quartz.jobStore.driverDelegateType"] = "Quartz.Impl.AdoJobStore.MySQLDelegate,Quartz";
            //数据源名称
            properties["quartz.jobStore.dataSource"] = "myDS";
            //连接字符串
            //properties["quartz.dataSource.myDS.connectionString"] = "Server=127.0.0.1;Database=test;Uid=root;Pwd=123456";
            //版本
            //连接字符串  根本没执行这个啊
            properties["quartz.dataSource.myDS.connectionString"] = "server=127.0.0.1;userid=root;password=123456;persistsecurityinfo=True;database=test"; //ConfigurationManager.AppSettings["connectionString"];
            //版本
            properties["quartz.dataSource.myDS.provider"] = "MySql";
            properties["quartz.scheduler.instanceId"] = "AUTO";
            properties["quartz.serializer.type"] = "binary";
            //properties["quartz.dataSource.myDS.provider"] = "MySql";
            //最大链接数
            //properties["quartz.dataSource.myDS.maxConnections"] = "5";
            // First we must get a reference to a scheduler
            var schedulerFactory = new StdSchedulerFactory(properties);
            var scheduler = schedulerFactory.GetScheduler().Result;
            Program.Scheduler = scheduler;
            scheduler.Start();
        }

        private static void Insert()
        {
            //这写法有问题，创建了一个新的scheduler
            //IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler().Result;

            IScheduler scheduler =Program.Scheduler;
            DateTime StarTime = DateTime.Now;
            DateTimeOffset starRunTime = DateBuilder.NextGivenSecondDate(StarTime, 1);
            DateTime EndTime = DateTime.MaxValue.AddDays(-1);
            DateTimeOffset endRunTime = DateBuilder.NextGivenSecondDate(EndTime, 1);
            IJobDetail job = JobBuilder.Create<DemoJob>()
                .WithIdentity("j1", "g1")
                .WithDescription("注释")
                .Build();
            ICronTrigger trigger = (ICronTrigger)TriggerBuilder.Create()
                .StartAt(starRunTime)//指定运行时间
                .EndAt(endRunTime)//指定结束时间
                .WithIdentity("j1", "g1")
                .WithCronSchedule("0/5 * * * * ?")//运行模式 每十秒钟运行一次
                .WithDescription("注释")
                .Build();
            scheduler.ScheduleJob(job, trigger);
            scheduler.Start();
        }
    }

    public class DemoJob : IJob
    {
        public Task Execute(IJobExecutionContext context)
        {
            Console.WriteLine(string.Format("{0}执行一次", DateTime.Now));
            return  Task.CompletedTask;
        }
    }
}
