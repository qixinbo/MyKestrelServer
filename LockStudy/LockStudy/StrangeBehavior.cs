﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace LockStudy
{
    static class StrangeBehavior
    {
        private static volatile bool stopWorker = false;

        static void Main(string[] args)
        {
            
            Console.WriteLine("begin!");
            Thread t = new Thread(worker);
            t.Start();
            Thread.Sleep(100);
            stopWorker = true;

            
            Console.WriteLine("wait to stop");
            t.Join();
        }

        private static void worker(object o)
        {
            int x = 0;

            while (!stopWorker)
            {
                x++;
            }
            Console.WriteLine($"stoped:x={x}");
        }
    }
}
