﻿using System;
using System.Text;
using RabbitMQ.Client;

namespace Send
{
    class Program
    {
        static void Main(string[] args)
        {
            //连接到本地机器上的消息代理
            var factory = new ConnectionFactory() { HostName = "localhost", UserName = "admin", Password = "123456" };
            //使用using结构，代码使用完毕及释放
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                //声明队列  hello为队列名
                //声明队列是一次性的，只有当它不存在时才会被创建
                channel.QueueDeclare(queue: "hello",
                    durable: false,
                    exclusive: false,//exclusive 排他的
                    autoDelete: false,
                    arguments: null);
                string message = "Hello World!";
                //Bytes流发送过去,可以使用任意编码模式，对应的解码模式相同即可
                var body = Encoding.UTF8.GetBytes(message);

                //发送消息  通过hello这个管道    
                channel.BasicPublish(exchange: "",
                    routingKey: "hello",
                    basicProperties: null,
                    body: body);
                Console.WriteLine(" [x] Sent {0}", message);
            }

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();


        }
    }
}
