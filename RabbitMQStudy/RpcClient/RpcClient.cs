﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Concurrent;
using System.Text;

namespace RpcClient
{
    public class RpcClient
    {
        private readonly IConnection connection;
        private readonly IModel channel;
        private readonly string replyQueueName;
        private readonly EventingBasicConsumer consumer;
        //声明一个独占的'回调'队列   
        // BlockingCollection是一个线程安全的生产者-消费者集合
        private readonly BlockingCollection<string> respQueue = new BlockingCollection<string>();
        //私有props，构造函数创造，call调用这个props
        private readonly IBasicProperties props;
        //构造函数
        public RpcClient()
        {
            //建立连接
            var factory = new ConnectionFactory() { HostName = "localhost" };
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            replyQueueName = channel.QueueDeclare().QueueName;
            consumer = new EventingBasicConsumer(channel);
            //
            props = channel.CreateBasicProperties();
            //系统生成一个correlationId
            var correlationId = Guid.NewGuid().ToString();
            props.CorrelationId = correlationId;
            //回调设为回调队列
            props.ReplyTo = replyQueueName;
            //订阅消费者的消费之后的返回信息
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                var response = Encoding.UTF8.GetString(body);
                if (ea.BasicProperties.CorrelationId == correlationId)
                {
                    respQueue.Add(response);
                }
            };
        }
        //正常的发送代码，这里面exchange是默认的
        public string Call(string message)
        {
            var messageBytes = Encoding.UTF8.GetBytes(message);
            channel.BasicPublish(
                exchange: "",
                routingKey: "rpc_queue",
                basicProperties: props,
                body: messageBytes);

            channel.BasicConsume(
                consumer: consumer,
                queue: replyQueueName,
                autoAck: true);
            //从回调队列中获得返回值，因为客户端一次只能处理一个数据，所以返回值应该是没问题的？
            //不一定啊,如果有N个客户端，最后回调的值，存入队列的顺序就有变化了。
            return respQueue.Take(); ;
        }

        public void Close()
        {
            connection.Close();
        }
    }

    public class Rpc
    {
        public static void Main()
        {
            var rpcClient = new RpcClient();

            Console.WriteLine(" [x] Requesting fib(30)");
            //如果我通过response这个实例连续调用，由于处理结果的不同，会造成我的调用顺序和结果顺序不一致啊。
            var response = rpcClient.Call("30");

            Console.WriteLine(" [.] Got '{0}'", response);
            rpcClient.Close();
        }
    }
}
