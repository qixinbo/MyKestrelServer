﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmitDelayQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "192.168.5.244", UserName = "admin", Password = "admin" };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    //dic.Add("x-expires", 30000);//队列过期时间   单位毫秒！！！
                    dic.Add("x-message-ttl", 12000);//队列上消息过期时间  单位毫秒！！！
                    dic.Add("x-dead-letter-exchange", "exchange-direct");//过期消息转向路由  
                    dic.Add("x-dead-letter-routing-key", "routing-delay");//过期消息转向路由相匹配routingkey  
                    //创建一个名叫"zzhello"的消息队列
                    channel.QueueDeclare(queue: "zzhello",
                        durable: true,
                        exclusive: false,
                        autoDelete: false,
                        arguments: dic);
                    var message = "Hello World!";
                    var body = Encoding.UTF8.GetBytes(message);
                    //向该消息队列发送消息message
                    channel.BasicPublish(exchange: "",
                        routingKey: "zzhello",
                        basicProperties: null,
                        body: body);
                    Console.WriteLine(" [x] Sent {0}", message);
                }
            }
            Console.ReadKey();
        }
    }
}
