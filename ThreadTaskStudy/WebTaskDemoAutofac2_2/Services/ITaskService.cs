﻿using BihuApiCore.Model.Response;
using System.Threading.Tasks;

namespace WebTaskDemo.Services
{
    public interface ITaskService
    {
        Task<BaseResponse> TaskMain();
    }
}
