﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ThreadTaskStudy
{
    class Program
    {
        static async Task Main(string[] args)
        {

            List<ModulusClass> list = new List<ModulusClass>();

            for (int i = 0; i < 100; i++)
            {
                ModulusClass item = new ModulusClass();
                item.Key = i % 5;
                item.Value = i;
            }
          
            Console.WriteLine("Hello World!");
        }
    }
}
