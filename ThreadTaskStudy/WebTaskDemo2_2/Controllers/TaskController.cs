﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using WebTaskDemo.Services;

namespace WebTaskDemo.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TaskController:ControllerBase
    {
    
        private readonly ILogger<TaskController> _logger;
        //private readonly  Lazy<TaskService> _taskServiceLazy=new Lazy<TaskService>();
            private readonly  ITaskService _taskService;
        public TaskController(ILogger<TaskController> logger,ITaskService taskService)
        {
             _taskService=taskService;
            _logger = logger;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("Test")]
        public async Task Test()
        {
            _logger.LogInformation("hello");
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("TaskLog")]
        public async Task TaskLog()
        {
            await _taskService.TaskMain();
        }

    }
}
