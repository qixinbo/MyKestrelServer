﻿using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebTaskDemo.Model;

namespace WebTaskDemo.Services
{
    public class TaskService: ITaskService
    {
        private readonly ILogger<TaskService> _logger;

        public TaskService(ILogger<TaskService> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task TaskMain()
        {
            List<ModulusClass> list = new List<ModulusClass>();

            
            for (int i = 0; i < 100000; i++)
            {
                ModulusClass item = new ModulusClass();
                item.Key = i % 10;
                item.Value = i;
                list.Add(item);
            }

            var group = list.GroupBy(c => c.Key).Select(g => g.ToList()).ToList();
   
            List<Task> taskList = new List<Task>();

            foreach (var item in group)
            {
                var w = Task.Run(async () =>
                {
                    await TaskLog(item);
                });

                taskList.Add(w);
            }
            var array = taskList.ToArray();

            _logger.LogInformation("hello");
         
        }

        public async Task TaskLog(List<ModulusClass> list)
        {
            foreach (var item in list)
            {
                _logger.LogInformation("hello"+item.Value);
                //Thread.Sleep(500);
            }
        }

    }
}
