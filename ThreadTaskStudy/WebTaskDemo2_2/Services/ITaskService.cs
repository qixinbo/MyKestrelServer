﻿using System.Threading.Tasks;

namespace WebTaskDemo.Services
{
    public interface ITaskService
    {
        Task TaskMain();
    }
}
