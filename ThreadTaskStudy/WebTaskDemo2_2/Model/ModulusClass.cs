﻿namespace WebTaskDemo.Model
{
    public class ModulusClass
    {
        public int Key { get; set; }

        public int Value { get; set; }
    }
}
