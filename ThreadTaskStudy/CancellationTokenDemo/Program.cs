﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace CancellationTokenDemo
{
    class Program
    {
        private static CancellationTokenSource cts = new CancellationTokenSource();
        static async Task Main(string[] args)
        {
            #region CancelAfter

            //HttpClient client = new HttpClient();
            //try
            //{
            //    CancellationTokenSource cts = new CancellationTokenSource();
            //    //millisecondsDelay
            //    cts.CancelAfter(1000);
            //    var res = await client.GetAsync("http://www.baidu.com", cts.Token);
            //    var result = await res.Content.ReadAsStringAsync();

            //    Console.WriteLine(result);
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine("发生异常" + ex.Source + Environment.NewLine + ex.StackTrace + Environment.NewLine + ex.Message + Environment.NewLine + ex.InnerException);
            //}

            #endregion

            #region Cancel

            var w = Task.Run(async () =>
             {
                 await TaskLog();
             }, cts.Token);

            Thread.Sleep(1000);
            Console.WriteLine("main go");

            cts.Cancel();
            await w;

            #endregion

            Console.ReadKey();
        }
        public static async Task TaskLog()
        {
            //while (!token.IsCancellationRequested)
            //{
            //    Console.WriteLine("11231");
            //    Thread.Sleep(500);
            //}

            //while (true)
            //{
            //    token.ThrowIfCancellationRequested();
            //    Console.WriteLine("11231");
            //    Thread.Sleep(500);
            //}
           
            while (!cts.Token.IsCancellationRequested)
            {
                Console.WriteLine("11231");
                Thread.Sleep(500);
            }
        }
    }
}
