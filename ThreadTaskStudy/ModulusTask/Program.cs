﻿using Infrastructure.Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ModulusTask
{
    class Program
    {
        static async Task Main(string[] args)
        {
            BaseResponseTask();
            Console.ReadLine();
        }

        static async Task BaseResponseTask()
        {
            List<ModulusClass> list = new List<ModulusClass>();

            //
            for (int i = 0; i < 100; i++)
            {
                ModulusClass item = new ModulusClass();
                item.Key = i % 5;
                item.Value = i;
                list.Add(item);
            }

            var group = list.GroupBy(c => c.Key);
            List<Task<BaseResponse>> taskList = new List<Task<BaseResponse>>();
            foreach (var item in group)
            {
               
                Worker worker = new Worker(item.ToList());
                //如果直接用worker.Run(); 不会新起现场，而是还是用主线程执行。
                var w = Task.Run(async () =>
                {
                    var a = await worker.RunBaseResponse();
                    return a;
                });

                taskList.Add(w);

                //这样是同步运行
                //taskList.Add(worker.RunBaseResponse());
            }

            //foreach (var item in group)
            //{
            //    Worker worker = new Worker(item.ToList());             
            //    taskList.Add(worker.Run());
            //}

            await Task.WhenAll(taskList);

            //这里如果有返回值，会对返回值进行判断
            foreach (var item in taskList)
            {
                if (item.Result.Code != 1)
                {
                    Console.WriteLine("False");
                    return;
                }
            }
            Console.WriteLine("Hello World!");
        }

        static async Task Simple()
        {
            List<ModulusClass> list = new List<ModulusClass>();

            //
            for (int i = 0; i < 100; i++)
            {
                ModulusClass item = new ModulusClass();
                item.Key = i % 5;
                item.Value = i;
                list.Add(item);
            }

            var group = list.GroupBy(c => c.Key);
            List<Task> taskList = new List<Task>();
            foreach (var item in group)
            {
                Worker worker = new Worker(item.ToList());

                //如果直接用worker.Run(); 不会新起现场，而是还是用主线程执行。
                var w = Task.Run(async () =>
                 {
                     await worker.Run();
                 });

                taskList.Add(w);
            }

            //foreach (var item in group)
            //{
            //    Worker worker = new Worker(item.ToList());             
            //    taskList.Add(worker.Run());
            //}

            await Task.WhenAll(taskList);

            //这里如果有返回值，会对返回值进行判断
            //foreach (var item in array)
            //{
            //    if (item.Result.Code != 1)
            //    {
            //        return item.Result;
            //    }
            //}

            Console.WriteLine("Hello World!");
        }

    }





    public class Worker
    {

        private readonly List<ModulusClass> _list;
        public Worker(List<ModulusClass> list)
        {
            _list = list;
        }
        public async Task Run()
        {
            foreach (var item in _list)
            {
                Console.WriteLine($"{item.Key}:{item.Value}");
                Thread.Sleep(500);
            }
        }

        public async Task<BaseResponse> RunBaseResponse()
        {
            foreach (var item in _list)
            {
                Console.WriteLine($"{item.Key}:{item.Value}");
                Thread.Sleep(500);
            }
            return BaseResponse.Ok();
        }
    }


}