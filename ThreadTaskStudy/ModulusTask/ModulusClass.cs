﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModulusTask
{
    public class ModulusClass
    {
        public int Key { get; set; }

        public int Value { get; set; }
    }
}
