﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QueueTask
{
    public class Worker
    {
        
        //定义队列
        public static ConcurrentQueue<ModulusClass> WorkQueue = new ConcurrentQueue<ModulusClass>();

        public async Task Run(int name)
        {
            SpinWait spin = new SpinWait();
            while (WorkQueue.Any())
            {
                ModulusClass item;

                while (!WorkQueue.TryDequeue(out item))
                {
                    spin.SpinOnce();
                }

                Console.WriteLine($"{item.Key}:{item.Value}");
                Thread.Sleep(500);
            }
            Console.WriteLine($"完成{name}");
        }
    }
}
