﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QueueTask
{
    class Program
    {
        static async Task Main(string[] args)
        {
            for (int i = 0; i < 100; i++)
            {
                ModulusClass item = new ModulusClass();
                item.Key = i % 5;
                item.Value = i;
                Worker.WorkQueue.Enqueue(item);
            }
            //Worker worker = new Worker();
            //await worker._do();


            List<Task> taskList = new List<Task>();
            for (int i = 0; i < 5; i++)
            {
                Worker worker = new Worker();

                //如果在异步代码中直接赋值i，i都会是5。。。。。哈哈  
                //这里申请了变量a，变量a是一个全新的地址，然后赋值给了run里面的代码，第二次for循环会申请另外一个变量a
                int a=i;
                //如果直接用worker.Run(); 不会新起现场，而是还是用主线程执行。
                //Task.Run(async () =>
                //{
                //    await worker.Run(a);
                //});


                //这样写会变成等待，也就是单线程
                //taskList.Add(worker.Run());
                //这样写会变成等待，也就是单线程
                //Task b=worker.Run(a);
                
                //这样使用w才能异步执行，并且把任务加到taskList，为啥正常的taskList.Add(worker.Run());就变成了同步代码呢？不懂
                var w =Task.Run(async () =>
                {
                    //这里循环后打印出来taskList.Count是0，说明for循环更快的完成了
                    //taskList.Add(worker.Run(a));
                    await worker.Run(a);
                });

                taskList.Add(w);

            }

            //这里打印出来的count 是0，说明for循环更快的完成了
            Console.WriteLine(taskList.Count);
            await Task.WhenAll(taskList);

            Console.WriteLine("Hello World!");
            Console.ReadLine();
        }
    }
}
