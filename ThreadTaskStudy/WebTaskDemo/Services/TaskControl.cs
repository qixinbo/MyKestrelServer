﻿using System.Threading;

namespace WebTaskDemo.Services
{
    public static class TaskControl
    {
        public static CancellationTokenSource CancellationControl { get; set; } = new CancellationTokenSource();

    }
}
