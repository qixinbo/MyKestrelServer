﻿using Infrastructure.Model.Response;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebTaskDemo.Model;

namespace WebTaskDemo.Services
{
    public class TaskService: ITaskService
    {
        private readonly ILogger<TaskService> _logger;

        public TaskService(ILogger<TaskService> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<BaseResponse> TaskMain()
        {
            List<ModulusClass> list = new List<ModulusClass>();

            
            for (int i = 0; i < 100000; i++)
            {
                ModulusClass item = new ModulusClass();
                item.Key = i % 10;
                item.Value = i;
                list.Add(item);
            }

            var group = list.GroupBy(c => c.Key).Select(g => g.ToList()).ToList();
   
            List<Task<BaseResponse>> taskList = new List<Task<BaseResponse>>();

            foreach (var item in group)
            {
                var w = Task.Run(async () =>
                {
                    return await TaskLog(item,TaskControl.CancellationControl.Token);
                });

                taskList.Add(w);
            }
            await Task.WhenAll(taskList);

            _logger.LogInformation("hello");
            return BaseResponse.Ok();
        }

        public async Task<BaseResponse> TaskLog(List<ModulusClass> list,CancellationToken ct)
        {
            foreach (var item in list)
            {
                if (ct.IsCancellationRequested)
                {
                    return BaseResponse.Ok("用户取消");
                }

                _logger.LogInformation("hello" + item.Value);
                //Thread.Sleep(500);
            }

            return BaseResponse.Ok();
        }

    }
}
