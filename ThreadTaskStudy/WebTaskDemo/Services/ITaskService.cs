﻿using Infrastructure.Model.Response;
using System.Threading.Tasks;

namespace WebTaskDemo.Services
{
    public interface ITaskService
    {
        Task<BaseResponse> TaskMain();
    }
}
