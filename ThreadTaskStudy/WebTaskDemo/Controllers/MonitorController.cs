﻿using Infrastructure.Model.Response;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebTaskDemo.Services;

namespace WebTaskDemo.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MonitorController : ControllerBase
    {
        [HttpGet("TaskCancel")]
        [ProducesResponseType(typeof(BaseResponse), 1)]
        public async Task<BaseResponse> TaskCancel()
        {
            TaskControl.CancellationControl.Cancel();
            TaskControl.CancellationControl = new System.Threading.CancellationTokenSource();
            return BaseResponse.Ok();
        }
    }
}
