﻿using Infrastructure.Model.Response;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using WebTaskDemo.Services;

namespace WebTaskDemo.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TaskController:ControllerBase
    {
    
        private readonly ILogger<TaskController> _logger;
        private readonly  ITaskService _taskService;
        public TaskController(ILogger<TaskController> logger,ITaskService taskService)
        {
            _taskService=taskService;
            _logger = logger;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("Test")]
        public async Task<BaseResponse> Test()
        {
            _logger.LogInformation("hello");
            return BaseResponse.Ok();
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("TaskLog")]
        public async Task<BaseResponse> TaskLog()
        {
            await _taskService.TaskMain();
            return BaseResponse.Ok();
        }

    }
}
