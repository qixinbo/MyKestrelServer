﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Microsoft.Extensions.Options;
using System;
using System.Text;

namespace LogCore2_2
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 2.2依赖注入的获取

            IServiceCollection serviceCollection = new ServiceCollection();
            serviceCollection.AddLogging(builder => builder
                .AddConsole()
                .AddFilter(level => level >= LogLevel.Information)
            );
            var loggerFactory = serviceCollection.BuildServiceProvider().GetService<ILoggerFactory>();

            ILogger logger = loggerFactory.CreateLogger(nameof(Program));
            int eventId = 3721;
            logger.LogInformation(eventId, "升级到最新.NET Core版本({version})", "1.0.0");
            logger.LogWarning(eventId, "并发量接近上限({maximum}) ", 200);
            logger.LogError(eventId, "数据库连接失败(数据库：{Database}，用户名：{User})", "TestDb", "sa");

            #endregion

            #region 依赖注入IOptionsMonitor配置

            //注册EncodingProvider实现对中文编码的支持
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            //Func<string, LogLevel, bool> filter = (category, level) => true;

            ILoggerFactory loggerFactory1 = new LoggerFactory();
            ConsoleLoggerOptions a =new ConsoleLoggerOptions();
            var provider = serviceCollection.BuildServiceProvider();
            var options = provider.GetRequiredService<IOptionsMonitor<ConsoleLoggerOptions>>();
            //这里会输出到控制台
            loggerFactory1.AddProvider(new ConsoleLoggerProvider(options));
            //这里会输出到debug
            //loggerFactory1.AddProvider(new DebugLoggerProvider(filter));
            ILogger logger1 = loggerFactory1.CreateLogger(nameof(Program));
          
            logger1.LogInformation(eventId, "升级到最新.NET Core版本({version})", "1.0.0");
            logger1.LogWarning(eventId, "并发量接近上限({maximum}) ", 200);
            logger1.LogError(eventId, "数据库连接失败(数据库：{Database}，用户名：{User})", "TestDb", "sa");

            #endregion

            #region 静态方法
            
            var loggerFactory2 = LoggerFactory.Create(builder =>
            {
                builder
                    .AddFilter("Microsoft", LogLevel.Warning)
                    .AddFilter("System", LogLevel.Warning)
                    .AddFilter("LoggingConsoleApp.Program", LogLevel.Debug)
                    .AddConsole()
                    .AddEventLog();
            });
            ILogger logger2 = loggerFactory.CreateLogger<Program>();
            logger2.LogInformation("Example log message");

            #endregion

            Console.WriteLine("Hello World!");

            Console.ReadLine();

        }
    }
}
