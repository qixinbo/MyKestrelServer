﻿using Microsoft.Extensions.Logging;
using System;

namespace LoggingDemo
{
    internal class DebugLoggerProvider : ILoggerProvider
    {
        private Func<string, LogLevel, bool> filter;

        public DebugLoggerProvider(Func<string, LogLevel, bool> filter)
        {
            this.filter = filter;
        }
    }
}