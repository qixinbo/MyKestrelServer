﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using System;

namespace LoggingDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //获取一个新的factory实例
            var loggerFactory = LoggerFactory.Create(builder =>
            {
                builder
                    .AddFilter("Microsoft", LogLevel.Warning)
                    .AddFilter("System", LogLevel.Warning)
                    .AddFilter("LoggingConsoleApp.Program", LogLevel.Debug)
                    //ConsoleLoggerProvider
                    .AddConsole()
                    //DebugLoggerProvider
                    .AddEventLog()
                    ;
            });
            ILogger logger = loggerFactory.CreateLogger<Program>();
            logger.LogInformation("Example log message");
            logger.LogError("Example log message");

            Console.WriteLine("Hello World!");

            
      




            Console.ReadLine();




        }
    }
}
