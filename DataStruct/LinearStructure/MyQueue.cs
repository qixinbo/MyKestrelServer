﻿using System;
using System.Collections;

namespace LinearStructure
{
    public class MyQueue : ICollection, ICloneable
    {
        #region 属性和私有变量

        /// <summary>
        /// 内部维护的数组，实际进行数据的存放
        /// </summary>
        private Object[] _array;

        /// <summary>
        /// First valid element in the MyQueue  表头
        /// </summary>
        private int _head;
        /// <summary>
        /// Last valid element in the MyQueue 表尾
        /// </summary>
        private int _tail;
        /// <summary>
        /// Number of elements.  队列元素数量
        /// </summary>
        private int _size;
        public virtual int Count
        {
            get { return _size; }
        }

        private int _growFactor; // 100 == 1.0, 130 == 1.3, 200 == 2.0
        private int _version;
        [NonSerialized]
        private Object _syncRoot;

        /// <summary>
        /// 最小的增量限制
        /// </summary>
        private const int _MinimumGrow = 4;
        private const int _ShrinkThreshold = 32;

        #endregion

        #region 构造函数

        // Creates a MyQueue with room for capacity objects. The default initial
        // capacity and grow factor are used.
        public MyQueue()
            : this(32, (float)2.0)
        {
        }

        // Creates a MyQueue with room for capacity objects. The default grow factor
        // is used.
        public MyQueue(int capacity)
            : this(capacity, (float)2.0)
        {
        }

        // Creates a MyQueue with room for capacity objects. When full, the new
        // capacity is set to the old capacity * growFactor.
        public MyQueue(int capacity, float growFactor)
        {
            if (capacity < 0)
                throw new ArgumentOutOfRangeException();
            if (!(growFactor >= 1.0 && growFactor <= 10.0))
                throw new ArgumentOutOfRangeException();

            _array = new Object[capacity];
            _head = 0;
            _tail = 0;
            _size = 0;
            _growFactor = (int)(growFactor * 100);
        }

        // Fills a MyQueue with the elements of an ICollection.  Uses the enumerator
        // to get each of the elements.
        public MyQueue(ICollection col) : this((col == null ? 32 : col.Count))
        {
            if (col == null)
                throw new ArgumentNullException("col");
            IEnumerator en = col.GetEnumerator();
            while (en.MoveNext())
                EnMyQueue(en.Current);
        }

        #endregion

        #region ICollection实现

        public virtual Object Clone()
        {
            MyQueue q = new MyQueue(_size);
            q._size = _size;

            int numToCopy = _size;
            int firstPart = (_array.Length - _head < numToCopy) ? _array.Length - _head : numToCopy;
            Array.Copy(_array, _head, q._array, 0, firstPart);
            numToCopy -= firstPart;
            if (numToCopy > 0)
                Array.Copy(_array, 0, q._array, _array.Length - _head, numToCopy);

            q._version = _version;
            return q;
        }

        public virtual bool IsSynchronized
        {
            get { return false; }
        }

        public virtual Object SyncRoot
        {
            get
            {
                if (_syncRoot == null)
                {
                    System.Threading.Interlocked.CompareExchange(ref _syncRoot, new Object(), null);
                }
                return _syncRoot;
            }
        }

        // CopyTo copies a collection into an Array, starting at a particular
        // index into the array.
        // 
        public virtual void CopyTo(Array array, int index)
        {
            if (array == null)
                throw new ArgumentNullException("array");
            if (array.Rank != 1)
                throw new ArgumentException();
            if (index < 0)
                throw new ArgumentOutOfRangeException();

            int arrayLen = array.Length;
            if (arrayLen - index < _size)
                throw new ArgumentException();

            int numToCopy = _size;
            if (numToCopy == 0)
                return;
            int firstPart = (_array.Length - _head < numToCopy) ? _array.Length - _head : numToCopy;
            Array.Copy(_array, _head, array, index, firstPart);
            numToCopy -= firstPart;
            if (numToCopy > 0)
                Array.Copy(_array, 0, array, index + _array.Length - _head, numToCopy);
        }

        #endregion

        #region 出队和清空

        // Removes the object at the head of the MyQueue and returns it. If the MyQueue
        // is empty, this method simply returns null.
        public virtual Object DeMyQueue()
        {
            if (Count == 0)
                throw new InvalidOperationException();
            //返回出队对象
            Object removed = _array[_head];
            //指向设置为空
            _array[_head] = null;

            //重新获取新的队列头部位置
            _head = (_head + 1) % _array.Length;
            _size--;
            _version++;
            return removed;
        }

        // Removes all Objects from the MyQueue.
        public virtual void Clear()
        {
            if (_head < _tail)
                Array.Clear(_array, _head, _size);
            else
            {
                Array.Clear(_array, _head, _array.Length - _head);
                Array.Clear(_array, 0, _tail);
            }

            _head = 0;
            _tail = 0;
            _size = 0;
            _version++;
        }

        #endregion

        #region 入队

        // Adds obj to the tail of the MyQueue.
        public virtual void EnMyQueue(Object obj)
        {
            //这时候需要扩容
            if (_size == _array.Length)
            {
                //使用_growFactor计算新的容量
                int newcapacity = (int)((long)_array.Length * (long)_growFactor / 100);
                if (newcapacity < _array.Length + _MinimumGrow)
                {
                    newcapacity = _array.Length + _MinimumGrow;
                }
                SetCapacity(newcapacity);
            }
            //尾部指针位置赋值
            _array[_tail] = obj;
            //循环队列，要进行余数运算
            _tail = (_tail + 1) % _array.Length;
            _size++;
            _version++;
        }

        #endregion

        #region 查询

        // Returns the object at the head of the MyQueue. The object remains in the
        // MyQueue. If the MyQueue is empty, this method throws an 
        // InvalidOperationException.
        public virtual Object Peek()
        {
            if (Count == 0)
                throw new InvalidOperationException();
            return _array[_head];
        }

        // Returns true if the MyQueue contains at least one object equal to obj.
        // Equality is determined using obj.Equals().
        //
        // Exceptions: ArgumentNullException if obj == null.
        public virtual bool Contains(Object obj)
        {
            //从同步开始遍历，遍历次数为_size
            int index = _head;
            int count = _size;

            while (count-- > 0)
            {
                if (obj == null)
                {
                    if (_array[index] == null)
                        return true;
                }
                else if (_array[index] != null && _array[index].Equals(obj))
                {
                    return true;
                }
                //求模方式获取下个位置
                index = (index + 1) % _array.Length;
            }

            return false;
        }

        internal Object GetElement(int i)
        {
            return _array[(_head + i) % _array.Length];
        }

        #endregion

        #region IEnumerator

        // GetEnumerator returns an IEnumerator over this MyQueue.  This
        // Enumerator will support removing.
        public virtual IEnumerator GetEnumerator()
        {
            return new MyQueueEnumerator(this);
        }

        // Implements an enumerator for a MyQueue.  The enumerator uses the
        // internal version number of the list to ensure that no modifications are
        // made to the list while an enumeration is in progress.
        [Serializable]
        private class MyQueueEnumerator : IEnumerator, ICloneable
        {
            private MyQueue _q;
            private int _index;
            private int _version;
            private Object currentElement;

            internal MyQueueEnumerator(MyQueue q)
            {
                _q = q;
                _version = _q._version;
                _index = 0;
                currentElement = _q._array;
                if (_q._size == 0)
                    _index = -1;
            }

            public Object Clone()
            {
                return MemberwiseClone();
            }

            public virtual bool MoveNext()
            {
                if (_version != _q._version) throw new InvalidOperationException();

                if (_index < 0)
                {
                    currentElement = _q._array;
                    return false;
                }

                currentElement = _q.GetElement(_index);
                _index++;

                if (_index == _q._size)
                    _index = -1;
                return true;
            }

            public virtual Object Current
            {
                get
                {
                    if (currentElement == _q._array)
                    {
                        if (_index == 0)
                            throw new InvalidOperationException();
                        else
                            throw new InvalidOperationException();
                    }
                    return currentElement;
                }
            }

            public virtual void Reset()
            {
                if (_version != _q._version) throw new InvalidOperationException();
                if (_q._size == 0)
                    _index = -1;
                else
                    _index = 0;
                currentElement = _q._array;
            }
        }

        #endregion

        #region Synchronized

        // Returns a synchronized MyQueue.  Returns a synchronized wrapper
        // class around the MyQueue - the caller must not use references to the
        // original MyQueue.
        public static MyQueue Synchronized(MyQueue MyQueue)
        {
            if (MyQueue == null)
                throw new ArgumentNullException("MyQueue");
            return new SynchronizedMyQueue(MyQueue);
        }

        // Implements a synchronization wrapper around a MyQueue.
        [Serializable]
        private class SynchronizedMyQueue : MyQueue
        {
            private MyQueue _q;
            private Object root;

            internal SynchronizedMyQueue(MyQueue q)
            {
                this._q = q;
                root = _q.SyncRoot;
            }

            public override bool IsSynchronized
            {
                get { return true; }
            }

            public override Object SyncRoot
            {
                get
                {
                    return root;
                }
            }

            public override int Count
            {
                get
                {
                    lock (root)
                    {
                        return _q.Count;
                    }
                }
            }

            public override void Clear()
            {
                lock (root)
                {
                    _q.Clear();
                }
            }

            public override Object Clone()
            {
                lock (root)
                {
                    return new SynchronizedMyQueue((MyQueue)_q.Clone());
                }
            }

            public override bool Contains(Object obj)
            {
                lock (root)
                {
                    return _q.Contains(obj);
                }
            }

            public override void CopyTo(Array array, int arrayIndex)
            {
                lock (root)
                {
                    _q.CopyTo(array, arrayIndex);
                }
            }

            public override void EnMyQueue(Object value)
            {
                lock (root)
                {
                    _q.EnMyQueue(value);
                }
            }

            // Thread safety problems with precondition - can't express the precondition as of Dev10.
            public override Object DeMyQueue()
            {
                lock (root)
                {
                    return _q.DeMyQueue();
                }
            }

            public override IEnumerator GetEnumerator()
            {
                lock (root)
                {
                    return _q.GetEnumerator();
                }
            }

            public override Object Peek()
            {
                lock (root)
                {
                    return _q.Peek();
                }
            }

            public override Object[] ToArray()
            {
                lock (root)
                {
                    return _q.ToArray();
                }
            }

            public override void TrimToSize()
            {
                lock (root)
                {
                    _q.TrimToSize();
                }
            }
        }

        #endregion

        #region 设置队列

        // PRIVATE Grows or shrinks the buffer to hold capacity objects. Capacity
        // must be >= _size.
        /// <summary>
        /// 设置队列的容量
        /// </summary>
        /// <param name="capacity"></param>
        private void SetCapacity(int capacity)
        {
            Object[] newarray = new Object[capacity];
            if (_size > 0)
            {
                if (_head < _tail) //正序的情况直接复制
                {
                    Array.Copy(_array, _head, newarray, 0, _size);
                }
                else
                {
                    //先把头部到数组结束的数据复制
                    Array.Copy(_array, _head, newarray, 0, _array.Length - _head);
                    //再复制0到尾部的数据
                    Array.Copy(_array, 0, newarray, _array.Length - _head, _tail);
                }
            }

            _array = newarray;
            _head = 0;
            //_size == capacity尾部设置为0，这样下次新增的时候是需要扩容的
            _tail = (_size == capacity) ? 0 : _size;
            _version++;
        }

        public virtual void TrimToSize()
        {
            SetCapacity(_size);
        }

        // Iterates over the objects in the MyQueue, returning an array of the
        // objects in the MyQueue, or an empty array if the MyQueue is empty.
        // The order of elements in the array is first in to last in, the same
        // order produced by successive calls to DeMyQueue.
        public virtual Object[] ToArray()
        {
            Object[] arr = new Object[_size];
            if (_size == 0)
                return arr;

            if (_head < _tail)
            {
                Array.Copy(_array, _head, arr, 0, _size);
            }
            else
            {
                Array.Copy(_array, _head, arr, 0, _array.Length - _head);
                Array.Copy(_array, 0, arr, _array.Length - _head, _tail);
            }

            return arr;
        }

        #endregion

    }
}
