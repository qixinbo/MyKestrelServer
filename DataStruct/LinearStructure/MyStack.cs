﻿using System;
using System.Collections;

namespace LinearStructure
{
    public class MyStack : ICollection, ICloneable
    {
        #region 属性和变量

        /// <summary>
        /// Storage for MyStack elements
        /// </summary>
        private Object[] _array;

        /// <summary>
        /// Number of items in the MyStack.
        /// </summary>
        private int _size;       
        public virtual int Count
        {
            get
            {
                return _size;
            }
        }
        // Used to keep enumerator in [....] w/ collection.
        private int _version;    
        
        [NonSerialized]
        private Object _syncRoot;

        /// <summary>
        /// 默认容量
        /// </summary>
        private const int _defaultCapacity = 10;

        #endregion

        #region 构造函数

        public MyStack()
        {
            _array = new Object[_defaultCapacity];
            _size = 0;
            _version = 0;
        }

        // Create a MyStack with a specific initial capacity.  The initial capacity
        // must be a non-negative number.
        public MyStack(int initialCapacity)
        {
            if (initialCapacity < 0)
                throw new ArgumentOutOfRangeException();

            if (initialCapacity < _defaultCapacity)
                initialCapacity = _defaultCapacity;  // Simplify doubling logic in Push.
            _array = new Object[initialCapacity];
            _size = 0;
            _version = 0;
        }

        // Fills a MyStack with the contents of a particular collection.  The items are
        // pushed onto the MyStack in the same order they are read by the enumerator.
        public MyStack(ICollection col) : this((col == null ? 32 : col.Count))
        {
            if (col == null)
                throw new ArgumentNullException("col");

            IEnumerator en = col.GetEnumerator();
            while (en.MoveNext())
                Push(en.Current);
        }

        #endregion

        #region 查询

        // Returns the top object on the MyStack without removing it.  If the MyStack
        // is empty, Peek throws an InvalidOperationException.
        public virtual Object Peek()
        {
            if (_size == 0)
                throw new InvalidOperationException();
            return _array[_size - 1];
        }

        public virtual bool Contains(Object obj)
        {
            int count = _size;

            while (count-- > 0)
            {
                if (obj == null)
                {
                    if (_array[count] == null)
                        return true;
                }
                else if (_array[count] != null && _array[count].Equals(obj))
                {
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region 入栈出栈

        // Pops an item from the top of the MyStack.  If the MyStack is empty, Pop
        // throws an InvalidOperationException.
        public virtual Object Pop()
        {
            if (_size == 0)
                throw new InvalidOperationException();
            _version++;
            Object obj = _array[--_size];
            _array[_size] = null;     // Free memory quicker.
            return obj;
        }

        // Pushes an item to the top of the MyStack.
        public virtual void Push(Object obj)
        {
            if (_size == _array.Length)
            {
                //以二倍新增
                Object[] newArray = new Object[2 * _array.Length];
                Array.Copy(_array, 0, newArray, 0, _size);
                _array = newArray;
            }
            _array[_size++] = obj;
            _version++;
        }

        #endregion

        #region 数组方法

        // Removes all Objects from the MyStack.
        public virtual void Clear()
        {
            Array.Clear(_array, 0, _size); // Don't need to doc this but we clear the elements so that the gc can reclaim the references.
            _size = 0;
            _version++;
        }


        public virtual Object Clone()
        {
            MyStack s = new MyStack(_size);
            s._size = _size;
            Array.Copy(_array, 0, s._array, 0, _size);
            s._version = _version;
            return s;
        }

        // Copies the MyStack into an array.
        public virtual void CopyTo(Array array, int index)
        {
            if (array == null)
                throw new ArgumentNullException("array");
            if (array.Rank != 1)
                throw new ArgumentException();
            if (index < 0)
                throw new ArgumentOutOfRangeException();
            if (array.Length - index < _size)
                throw new ArgumentException();

            int i = 0;
            if (array is Object[])
            {
                Object[] objArray = (Object[])array;
                while (i < _size)
                {
                    objArray[i + index] = _array[_size - i - 1];
                    i++;
                }
            }
            else
            {
                while (i < _size)
                {
                    array.SetValue(_array[_size - i - 1], i + index);
                    i++;
                }
            }
        }

        // Copies the MyStack to an array, in the same order Pop would return the items.
        public virtual Object[] ToArray()
        {
            Object[] objArray = new Object[_size];
            int i = 0;
            while (i < _size)
            {
                objArray[i] = _array[_size - i - 1];
                i++;
            }
            return objArray;
        }

        #endregion

        #region Enumerator

        // Returns an IEnumerator for this MyStack.
        public virtual IEnumerator GetEnumerator()
        {
            return new MyStackEnumerator(this);
        }

        [Serializable]
        private class MyStackEnumerator : IEnumerator, ICloneable
        {
            private MyStack _MyStack;
            private int _index;
            private int _version;
            private Object currentElement;

            internal MyStackEnumerator(MyStack MyStack)
            {
                _MyStack = MyStack;
                _version = _MyStack._version;
                _index = -2;
                currentElement = null;
            }

            public Object Clone()
            {
                return MemberwiseClone();
            }

            public virtual bool MoveNext()
            {
                bool retval;
                if (_version != _MyStack._version) throw new InvalidOperationException();
                if (_index == -2)
                {  // First call to enumerator.
                    _index = _MyStack._size - 1;
                    retval = (_index >= 0);
                    if (retval)
                        currentElement = _MyStack._array[_index];
                    return retval;
                }
                if (_index == -1)
                {  // End of enumeration.
                    return false;
                }

                retval = (--_index >= 0);
                if (retval)
                    currentElement = _MyStack._array[_index];
                else
                    currentElement = null;
                return retval;
            }

            public virtual Object Current
            {
                get
                {
                    if (_index == -2) throw new InvalidOperationException();
                    if (_index == -1) throw new InvalidOperationException();
                    return currentElement;
                }
            }

            public virtual void Reset()
            {
                if (_version != _MyStack._version) throw new InvalidOperationException();
                _index = -2;
                currentElement = null;
            }
        }

        #endregion

        #region Synchronized

        public virtual bool IsSynchronized
        {
            get { return false; }
        }

        public virtual Object SyncRoot
        {
            get
            {
                if (_syncRoot == null)
                {
                    System.Threading.Interlocked.CompareExchange<Object>(ref _syncRoot, new Object(), null);
                }
                return _syncRoot;
            }
        }

        // Returns a synchronized MyStack.
        public static MyStack Synchronized(MyStack MyStack)
        {
            if (MyStack == null)
                throw new ArgumentNullException("MyStack");
            return new SyncMyStack(MyStack);
        }

        [Serializable]
        private class SyncMyStack : MyStack
        {
            private MyStack _s;
            private Object _root;

            internal SyncMyStack(MyStack MyStack)
            {
                _s = MyStack;
                _root = MyStack.SyncRoot;
            }

            public override bool IsSynchronized
            {
                get { return true; }
            }

            public override Object SyncRoot
            {
                get
                {
                    return _root;
                }
            }

            public override int Count
            {
                get
                {
                    lock (_root)
                    {
                        return _s.Count;
                    }
                }
            }

            public override bool Contains(Object obj)
            {
                lock (_root)
                {
                    return _s.Contains(obj);
                }
            }

            public override Object Clone()
            {
                lock (_root)
                {
                    return new SyncMyStack((MyStack)_s.Clone());
                }
            }

            public override void Clear()
            {
                lock (_root)
                {
                    _s.Clear();
                }
            }

            public override void CopyTo(Array array, int arrayIndex)
            {
                lock (_root)
                {
                    _s.CopyTo(array, arrayIndex);
                }
            }

            public override void Push(Object value)
            {
                lock (_root)
                {
                    _s.Push(value);
                }
            }

            //Thread safety problems with precondition - can't express the precondition as of Dev10.
            public override Object Pop()
            {
                lock (_root)
                {
                    return _s.Pop();
                }
            }

            public override IEnumerator GetEnumerator()
            {
                lock (_root)
                {
                    return _s.GetEnumerator();
                }
            }

            // Thread safety problems with precondition - can't express the precondition as of Dev10.
            public override Object Peek()
            {
                lock (_root)
                {
                    return _s.Peek();
                }
            }

            public override Object[] ToArray()
            {
                lock (_root)
                {
                    return _s.ToArray();
                }
            }
        }

        #endregion

    }
}
