﻿using System.Collections.Generic;

namespace LinearStructure.MyHashSet
{
    class MyHashSetEqualityComparer<T> : IEqualityComparer<MyHashSet<T>?>
    {
        private readonly IEqualityComparer<T> _comparer;

        public MyHashSetEqualityComparer()
        {
            _comparer = EqualityComparer<T>.Default;
        }

        // using m_comparer to keep equals properties in tact; don't want to choose one of the comparers
        public bool Equals(MyHashSet<T>? x, MyHashSet<T>? y)
        {
            return MyHashSet<T>.HashSetEquals(x, y, _comparer);
        }

        public int GetHashCode(MyHashSet<T>? obj)
        {
            int hashCode = 0;
            if (obj != null)
            {
                foreach (T t in obj)
                {
                    if (t != null)
                    {
                        hashCode ^= (_comparer.GetHashCode(t) & 0x7FFFFFFF);
                    }
                }
            } // else returns hashcode of 0 for null hashsets
            return hashCode;
        }

        // Equals method for the comparer itself.
        public override bool Equals(object? obj)
        {
            MyHashSetEqualityComparer<T>? comparer = obj as MyHashSetEqualityComparer<T>;
            if (comparer == null)
            {
                return false;
            }
            return (_comparer == comparer._comparer);
        }

        public override int GetHashCode()
        {
            return _comparer.GetHashCode();
        }
    }
}
