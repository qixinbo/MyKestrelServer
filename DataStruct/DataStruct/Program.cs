﻿using MyHashtable;
using System;

namespace DataStruct
{
    class Program
    {
        static void Main(string[] args)
        {  
            MyHash hash = new MyHash();
            hash["A1"] = DateTime.Now;
            hash["A2"] = 1;
            Console.WriteLine(Convert.ToString(hash["A1"]));
            Console.WriteLine(Convert.ToString(hash["A2"]));
        }
    }
}
