﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;

namespace DataStruct
{
    public class MyHashtable : IDictionary, ISerializable, IDeserializationCallback, ICloneable
    {
        #region 属性及变量

        /// <summary>
        /// 哈希函数使用的固定101的素数
        /// </summary>
        internal const Int32 HashPrime = 101;
        private const Int32 InitialSize = 3;
        private const String LoadFactorName = "LoadFactor";
        private const String VersionName = "Version";
        private const String ComparerName = "Comparer";
        private const String HashCodeProviderName = "HashCodeProvider";
        private const String HashSizeName = "HashSize";  // Must save buckets.Length
        private const String KeysName = "Keys";
        private const String ValuesName = "Values";
        private const String KeyComparerName = "KeyComparer";

        // The hash table data.
        // This cannot be serialised
        private struct bucket
        {
            public Object key;
            public Object val;
            public int hash_coll;   // Store hash code; sign bit means there was a collision.
        }

        private bucket[] buckets;

        // The total number of entries in the hash table.
        private int count;

        // The total number of collision(碰撞) bits set in the hashtable
        private int occupancy;

        /// <summary>
        /// bucket数组的长度乘以loadFactor(loadFactor不能大于1，所以是小于bucket数组长度的) 
        /// </summary>
        private int loadsize;

        /// <summary>
        /// 0.72f 乘以用户自定义的值
        /// </summary>
        private float loadFactor;

        private volatile int version;
        /// <summary>
        /// 控制并发使用，方法中在写入时会赋值yes  写入完成赋值false
        /// </summary>
        private volatile bool isWriterInProgress;

        private ICollection keys;
        private ICollection values;

        private IEqualityComparer _keycomparer;
        protected IEqualityComparer EqualityComparer
        {
            get
            {
                return _keycomparer;
            }
        }

        private Object _syncRoot;

        /// <summary>
        /// 
        /// </summary>
        protected IHashCodeProvider hcp
        {
            get
            {
                if (_keycomparer is CompatibleComparer)
                {
                    return ((CompatibleComparer)_keycomparer).HashCodeProvider;
                }
                else if (_keycomparer == null)
                {
                    return null;
                }
                else
                {
                    throw new ArgumentException("Arg_CannotMixComparisonInfrastructure");
                }
            }
            set
            {
                if (_keycomparer is CompatibleComparer)
                {
                    CompatibleComparer keyComparer = (CompatibleComparer)_keycomparer;
                    _keycomparer = new CompatibleComparer(keyComparer.Comparer, value);
                }
                else if (_keycomparer == null)
                {
                    _keycomparer = new CompatibleComparer((IComparer)null, value);
                }
                else
                {
                    throw new ArgumentException("Arg_CannotMixComparisonInfrastructure");
                }
            }
        }

        protected IComparer comparer
        {
            get
            {
                if (_keycomparer is CompatibleComparer)
                {
                    return ((CompatibleComparer)_keycomparer).Comparer;
                }
                else if (_keycomparer == null)
                {
                    return null;
                }
                else
                {
                    throw new ArgumentException("Arg_CannotMixComparisonInfrastructure");
                }
            }
            set
            {
                if (_keycomparer is CompatibleComparer)
                {
                    CompatibleComparer keyComparer = (CompatibleComparer)_keycomparer;
                    _keycomparer = new CompatibleComparer(value, keyComparer.HashCodeProvider);
                }
                else if (_keycomparer == null)
                {
                    _keycomparer = new CompatibleComparer(value, (IHashCodeProvider)null);
                }
                else
                {
                    throw new ArgumentException("Arg_CannotMixComparisonInfrastructure");
                }
            }
        }


        #endregion

        #region 构造函数

        // Note: this constructor is a bogus constructor that does nothing
        // and is for use only with SyncHashtable.
        internal MyHashtable(bool trash)
        {
        }

        public MyHashtable() : this(0, 1.0f)
        {
        }

        public MyHashtable(int capacity) : this(capacity, 1.0f)
        {
        }

        public MyHashtable(int capacity, float loadFactor)
        {
            if (capacity < 0)
                throw new ArgumentOutOfRangeException("capacity", "ArgumentOutOfRange_NeedNonNegNum");
            if (!(loadFactor >= 0.1f && loadFactor <= 1.0f))
                throw new ArgumentOutOfRangeException("ArgumentOutOfRange_HashtableLoadFactor");

            // Based on perf work, .72 is the optimal load factor for this table.  
            this.loadFactor = 0.72f * loadFactor;

            double rawsize = capacity / this.loadFactor;
            if (rawsize > Int32.MaxValue)
                throw new ArgumentException("Arg_HTCapacityOverflow");

            // Avoid awfully(非常; 极其;) small sizes
            int hashsize = (rawsize > InitialSize) ? HashHelpers.GetPrime((int)rawsize) : InitialSize;
            buckets = new bucket[hashsize];

            loadsize = (int)(this.loadFactor * hashsize);
            isWriterInProgress = false;
        }


        public MyHashtable(IDictionary d, float loadFactor)
           : this(d, loadFactor, (IEqualityComparer)null)
        {
        }

        [Obsolete("Please use Hashtable(IDictionary, IEqualityComparer) instead.")]
        public MyHashtable(IDictionary d, IHashCodeProvider hcp, IComparer comparer)
            : this(d, 1.0f, hcp, comparer)
        {
        }
        public MyHashtable(int capacity, IEqualityComparer equalityComparer)
      : this(capacity, 1.0f, equalityComparer)
        {
        }


        public MyHashtable(IDictionary d, IEqualityComparer equalityComparer)
            : this(d, 1.0f, equalityComparer)
        {
        }

        public MyHashtable(IDictionary d, float loadFactor, IHashCodeProvider hcp, IComparer comparer)
            : this((d != null ? d.Count : 0), loadFactor, hcp, comparer)
        {
            if (d == null)
                throw new ArgumentNullException("d", "ArgumentNull_Dictionary");

            IDictionaryEnumerator e = d.GetEnumerator();
            while (e.MoveNext()) Add(e.Key, e.Value);
        }

        public MyHashtable(IDictionary d, float loadFactor, IEqualityComparer equalityComparer)
            : this((d != null ? d.Count : 0), loadFactor, equalityComparer)
        {
            if (d == null)
                throw new ArgumentNullException("d", "ArgumentNull_Dictionary");

            IDictionaryEnumerator e = d.GetEnumerator();
            while (e.MoveNext()) Add(e.Key, e.Value);
        }

        public MyHashtable(int capacity, float loadFactor, IEqualityComparer equalityComparer) : this(capacity, loadFactor)
        {
            this._keycomparer = equalityComparer;
        }

        protected MyHashtable(SerializationInfo info, StreamingContext context)
        {
            //We can't do anything with the keys and values until the entire graph has been deserialized
            //and we have a reasonable estimate that GetHashCode is not going to fail.  For the time being,
            //we'll just cache this.  The graph is not valid until OnDeserialization has been called.
            HashHelpers.SerializationInfoTable.Add(this, info);
        }

        // Constructs a new hashtable with the given initial capacity and load
        // factor. The capacity argument serves as an indication of the
        // number of entries the hashtable will contain. When this number (or an
        // approximation) is known, specifying it in the constructor can eliminate
        // a number of resizing operations that would otherwise be performed when
        // elements are added to the hashtable. The loadFactor argument
        // indicates the maximum ratio of hashtable entries to hashtable buckets.
        // Smaller load factors cause faster average lookup times at the cost of
        // increased memory consumption. A load factor of 1.0 generally provides
        // the best balance between speed and size.  The hcp argument
        // is used to specify an Object that will provide hash codes for all
        // the Objects in the table.  Using this, you can in effect override
        // GetHashCode() on each Object using your own hash function.  The 
        // comparer argument will let you specify a custom function for
        // comparing keys.  By specifying user-defined objects for hcp
        // and comparer, users could make a hash table using strings
        // as keys do case-insensitive lookups.
        //     
        public MyHashtable(int capacity, float loadFactor, IHashCodeProvider hcp, IComparer comparer) : this(capacity, loadFactor)
        {
            if (hcp == null && comparer == null)
            {
                this._keycomparer = null;
            }
            else
            {
                this._keycomparer = new CompatibleComparer(comparer, hcp);
            }
        }

        //这里是双重散列法的哈希函数
        private uint InitHash(Object key, int hashsize, out uint seed, out uint incr)
        {
            // Hashcode must be positive.  Also, we must not use the sign bit, since
            // that is used for the collision bit.
            uint hashcode = (uint)GetHash(key) & 0x7FFFFFFF;
            seed = (uint)hashcode;
            // Restriction: incr MUST be between 1 and hashsize - 1, inclusive for
            // the modular（单元） arithmetic to work correctly.  This guarantees you'll
            // visit every bucket in the table exactly once within hashsize 
            // iterations.  Violate this and it'll cause obscure bugs forever.
            // If you change this calculation for h2(key), update putEntry too!
            //第二个哈希函数h_2(k)的增量
            incr = (uint)(1 + ((seed * HashPrime) % ((uint)hashsize - 1)));
            return hashcode;
        }




        #endregion

        #region Add

        public virtual void Add(Object key, Object value)
        {
            Insert(key, value, true);
        }

        private void Insert(Object key, Object nvalue, bool add)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key", "ArgumentNull_Key");
            }

            if (count >= loadsize)
            {
                expand();
            }
            //这里碰撞次数怎么会超过总容量呢？一直插入的数据hash到同一个key？？
            else if (occupancy > loadsize && count > 100)
            {
                rehash();
            }

            uint seed;
            uint incr;
            // Assume we only have one thread writing concurrently.  Modify
            // buckets to contain new data, as long as we insert in the right order.
            uint hashcode = InitHash(key, buckets.Length, out seed, out incr);
            //记录几次尝试
            int ntry = 0;

            // We use the empty slot number to cache the first empty slot. We chose to reuse slots
            //记录第一个寻址到的可用插槽。
            int emptySlotNumber = -1;
            // create by remove that have the collision bit set over using up new slots.
            int bucketNumber = (int)(seed % (uint)buckets.Length);
            do
            {
                // Set emptySlot number to current bucket if it is the first available bucket that we have seen
                // that once contained an entry and also has had a collision.
                // We need to search this entire collision chain because we have to ensure that there are no 
                // duplicate entries in the table.
                if (emptySlotNumber == -1 && (buckets[bucketNumber].key == buckets) && (buckets[bucketNumber].hash_coll < 0))//(((buckets[bucketNumber].hash_coll & unchecked(0x80000000))!=0)))
                    emptySlotNumber = bucketNumber;

                // Insert the key/value pair into this bucket if this bucket is empty and has never contained an entry
                // OR
                // This bucket once contained an entry but there has never been a collision
                if ((buckets[bucketNumber].key == null) ||
                    (buckets[bucketNumber].key == buckets && ((buckets[bucketNumber].hash_coll & unchecked(0x80000000)) == 0)))
                {

                    // If we have found an available bucket that has never had a collision, but we've seen an available
                    // bucket in the past that has the collision bit set, use the previous bucket instead
                    if (emptySlotNumber != -1) // Reuse slot
                        bucketNumber = emptySlotNumber;

                    // We pretty much have to insert in this order.  Don't set hash
                    // code until the value & key are set appropriately.
#if !FEATURE_CORECLR
                    Thread.BeginCriticalRegion();
#endif

                    isWriterInProgress = true;
                    buckets[bucketNumber].val = nvalue;
                    buckets[bucketNumber].key = key;
                    buckets[bucketNumber].hash_coll |= (int)hashcode;
                    count++;
                    UpdateVersion();
                    isWriterInProgress = false;
#if !FEATURE_CORECLR
                    Thread.EndCriticalRegion();
#endif                    
                    return;
                }

                // The current bucket is in use
                // OR
                // it is available, but has had the collision bit set and we have already found an available bucket
                //已经存在，而且key也相同，如果是新增就抛出异常，如果是更新，就改变值。
                if (((buckets[bucketNumber].hash_coll & 0x7FFFFFFF) == hashcode) &&
                    KeyEquals(buckets[bucketNumber].key, key))
                {
                    if (add)
                    {
                        throw new ArgumentException("Argument_AddingDuplicate__");
                    }
#if !FEATURE_CORECLR
                    Thread.BeginCriticalRegion();
#endif          
                    isWriterInProgress = true;
                    buckets[bucketNumber].val = nvalue;
                    UpdateVersion();
                    isWriterInProgress = false;
#if !FEATURE_CORECLR
                    Thread.EndCriticalRegion();
#endif
                    return;
                }

                // The current bucket is full, and we have therefore collided.  We need to set the collision bit
                // UNLESS
                // we have remembered an available slot previously.
                if (emptySlotNumber == -1)
                {
                    // We don't need to set the collision bit here since we already have an empty slot
                    if (buckets[bucketNumber].hash_coll >= 0)
                    {
                        buckets[bucketNumber].hash_coll |= unchecked((int)0x80000000);
                        occupancy++;
                    }
                }
                //查找下个位置
                bucketNumber = (int)(((long)bucketNumber + incr) % (uint)buckets.Length);
            } while (++ntry < buckets.Length);

            // This code is here if and only if there were no buckets without a collision bit set in the entire table
            if (emptySlotNumber != -1)
            {
                // We pretty much have to insert in this order.  Don't set hash
                // code until the value & key are set appropriately.

                isWriterInProgress = true;
                buckets[emptySlotNumber].val = nvalue;
                buckets[emptySlotNumber].key = key;
                buckets[emptySlotNumber].hash_coll |= (int)hashcode;
                count++;
                UpdateVersion();
                isWriterInProgress = false;

                return;
            }
            throw new InvalidOperationException("InvalidOperation_HashInsertFailed");
        }

        private void expand()
        {
            int rawsize = HashHelpers.ExpandPrime(buckets.Length);
            rehash(rawsize, false);
        }

        // We occationally need to rehash the table to clean up the collision bits.
        private void rehash()
        {
            rehash(buckets.Length, false);
        }

        private void UpdateVersion()
        {
            // Version might become negative when version is Int32.MaxValue, but the oddity will be still be correct. 
            // So we don't need to special case this. 
            version++;
        }


        private void rehash(int newsize, bool forceNewHashCode)
        {
            // reset occupancy
            occupancy = 0;

            // Don't replace any internal state until we've finished adding to the 
            // new bucket[].  This serves two purposes: 
            //   1) Allow concurrent readers to see valid hashtable contents 
            //      at all times
            //   2) Protect against an OutOfMemoryException while allocating this 
            //      new bucket[].
            bucket[] newBuckets = new bucket[newsize];

            // rehash table into new buckets
            int nb;
            for (nb = 0; nb < buckets.Length; nb++)
            {
                bucket oldb = buckets[nb];
                if ((oldb.key != null) && (oldb.key != buckets))
                {
                    int hashcode = ((forceNewHashCode ? GetHash(oldb.key) : oldb.hash_coll) & 0x7FFFFFFF);
                    putEntry(newBuckets, oldb.key, oldb.val, hashcode);
                }
            }

            // New bucket[] is good to go - replace buckets and other internal state.
#if !FEATURE_CORECLR
            Thread.BeginCriticalRegion();
#endif
            isWriterInProgress = true;
            buckets = newBuckets;
            loadsize = (int)(loadFactor * newsize);
            UpdateVersion();
            isWriterInProgress = false;
#if !FEATURE_CORECLR            
            Thread.EndCriticalRegion();
#endif
            // minimun size of hashtable is 3 now and maximum loadFactor is 0.72 now.

            return;
        }

        private void putEntry(bucket[] newBuckets, Object key, Object nvalue, int hashcode)
        {
            uint seed = (uint)hashcode;
            uint incr = (uint)(1 + ((seed * HashPrime) % ((uint)newBuckets.Length - 1)));
            int bucketNumber = (int)(seed % (uint)newBuckets.Length);
            do
            {

                if ((newBuckets[bucketNumber].key == null) || (newBuckets[bucketNumber].key == buckets))
                {
                    newBuckets[bucketNumber].val = nvalue;
                    newBuckets[bucketNumber].key = key;
                    newBuckets[bucketNumber].hash_coll |= hashcode;
                    return;
                }

                if (newBuckets[bucketNumber].hash_coll >= 0)
                {
                    newBuckets[bucketNumber].hash_coll |= unchecked((int)0x80000000);
                    occupancy++;
                }
                bucketNumber = (int)(((long)bucketNumber + incr) % (uint)newBuckets.Length);
            } while (true);
        }

        #endregion

        #region 删除


        public virtual void Clear()
        {
            if (count == 0 && occupancy == 0)
                return;

#if !FEATURE_CORECLR
            Thread.BeginCriticalRegion();
#endif
            isWriterInProgress = true;
            for (int i = 0; i < buckets.Length; i++)
            {
                buckets[i].hash_coll = 0;
                buckets[i].key = null;
                buckets[i].val = null;
            }

            count = 0;
            occupancy = 0;
            UpdateVersion();
            isWriterInProgress = false;
#if !FEATURE_CORECLR
            Thread.EndCriticalRegion();
#endif
        }

        // Removes an entry from this hashtable. If an entry with the specified
        // key exists in the hashtable, it is removed. An ArgumentException is
        // thrown if the key is null.
        public virtual void Remove(Object key)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key", "ArgumentNull_Key");
            }

            uint seed;
            uint incr;
            // Assuming only one concurrent writer, write directly into buckets.
            uint hashcode = InitHash(key, buckets.Length, out seed, out incr);
            int ntry = 0;

            bucket b;
            int bn = (int)(seed % (uint)buckets.Length);  // bucketNumber
            do
            {
                b = buckets[bn];
                if (((b.hash_coll & 0x7FFFFFFF) == hashcode) &&
                    KeyEquals(b.key, key))
                {
#if !FEATURE_CORECLR
                    Thread.BeginCriticalRegion();
#endif
                    isWriterInProgress = true;
                    // Clear hash_coll field, then key, then value
                    buckets[bn].hash_coll &= unchecked((int)0x80000000);
                    if (buckets[bn].hash_coll != 0)
                    {
                        buckets[bn].key = buckets;
                    }
                    else
                    {
                        buckets[bn].key = null;
                    }
                    buckets[bn].val = null;  // Free object references sooner & simplify ContainsValue.
                    count--;
                    UpdateVersion();
                    isWriterInProgress = false;
#if !FEATURE_CORECLR
                    Thread.EndCriticalRegion();
#endif
                    return;
                }
                bn = (int)(((long)bn + incr) % (uint)buckets.Length);
            } while (b.hash_coll < 0 && ++ntry < buckets.Length);

            //throw new ArgumentException(Environment.GetResourceString("Arg_RemoveArgNotFound"));
        }

        #endregion

        #region 字典方法

        public virtual ICollection Values
        {
            get
            {
                if (values == null) values = new ValueCollection(this);
                return values;
            }
        }

        public virtual ICollection Keys
        {
            get
            {
                if (keys == null) keys = new KeyCollection(this);
                return keys;
            }
        }

         // Is this Hashtable read-only?
        public virtual bool IsReadOnly {
            get { return false; }
        }


        public virtual bool IsFixedSize
        {
            get { return false; }
        }

        // Is this Hashtable synchronized?  See SyncRoot property
        public virtual bool IsSynchronized
        {
            get { return false; }
        }

        // Clone returns a virtually identical copy of this hash table.  This does
        // a shallow copy - the Objects in the table aren't cloned, only the references
        // to those Objects.
        public virtual Object Clone()
        {
            bucket[] lbuckets = buckets;
            MyHashtable ht = new MyHashtable(count, _keycomparer);
            ht.version = version;
            ht.loadFactor = loadFactor;
            ht.count = 0;

            int bucket = lbuckets.Length;
            while (bucket > 0)
            {
                bucket--;
                Object keyv = lbuckets[bucket].key;
                if ((keyv != null) && (keyv != lbuckets))
                {
                    ht[keyv] = lbuckets[bucket].val;
                }
            }

            return ht;
        }

        // Checks if this hashtable contains the given key.

        public virtual bool Contains(Object key)
        {
            return ContainsKey(key);
        }

        // Checks if this hashtable contains an entry with the given key.  This is
        // an O(1) operation.
        // 
        public virtual bool ContainsKey(Object key)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key", "ArgumentNull_Key");
            }

            uint seed;
            uint incr;
            // Take a snapshot of buckets, in case another thread resizes table
            bucket[] lbuckets = buckets;
            uint hashcode = InitHash(key, lbuckets.Length, out seed, out incr);
            int ntry = 0;

            bucket b;
            int bucketNumber = (int)(seed % (uint)lbuckets.Length);
            do
            {
                b = lbuckets[bucketNumber];
                if (b.key == null)
                {
                    return false;
                }
                if (((b.hash_coll & 0x7FFFFFFF) == hashcode) &&
                    KeyEquals(b.key, key))
                    return true;
                bucketNumber = (int)(((long)bucketNumber + incr) % (uint)lbuckets.Length);
            } while (b.hash_coll < 0 && ++ntry < lbuckets.Length);
            return false;
        }

        // Checks if this hashtable contains an entry with the given value. The
        // values of the entries of the hashtable are compared to the given value
        // using the Object.Equals method. This method performs a linear
        // search and is thus be substantially slower than the ContainsKey
        // method.
        // 
        public virtual bool ContainsValue(Object value)
        {

            if (value == null)
            {
                for (int i = buckets.Length; --i >= 0;)
                {
                    if (buckets[i].key != null && buckets[i].key != buckets && buckets[i].val == null)
                        return true;
                }
            }
            else
            {
                for (int i = buckets.Length; --i >= 0;)
                {
                    Object val = buckets[i].val;
                    if (val != null && val.Equals(value)) return true;
                }
            }
            return false;
        }

        // Copies the keys of this hashtable to a given array starting at a given
        // index. This method is used by the implementation of the CopyTo method in
        // the KeyCollection class.
        private void CopyKeys(Array array, int arrayIndex)
        {

            bucket[] lbuckets = buckets;
            for (int i = lbuckets.Length; --i >= 0;)
            {
                Object keyv = lbuckets[i].key;
                if ((keyv != null) && (keyv != buckets))
                {
                    array.SetValue(keyv, arrayIndex++);
                }
            }
        }

        // Copies the keys of this hashtable to a given array starting at a given
        // index. This method is used by the implementation of the CopyTo method in
        // the KeyCollection class.
        private void CopyEntries(Array array, int arrayIndex)
        {

            bucket[] lbuckets = buckets;
            for (int i = lbuckets.Length; --i >= 0;)
            {
                Object keyv = lbuckets[i].key;
                if ((keyv != null) && (keyv != buckets))
                {
                    DictionaryEntry entry = new DictionaryEntry(keyv, lbuckets[i].val);
                    array.SetValue(entry, arrayIndex++);
                }
            }
        }

        // Copies the values in this hash table to an array at
        // a given index.  Note that this only copies values, and not keys.
        public virtual void CopyTo(Array array, int arrayIndex)
        {
            if (array == null)
                throw new ArgumentNullException("array", "ArgumentNull_Array");
            if (array.Rank != 1)
                throw new ArgumentException("Arg_RankMultiDimNotSupported");
            if (arrayIndex < 0)
                throw new ArgumentOutOfRangeException("arrayIndex", "ArgumentOutOfRange_NeedNonNegNum");
            if (array.Length - arrayIndex < Count)
                throw new ArgumentException("Arg_ArrayPlusOffTooSmall");

            CopyEntries(array, arrayIndex);
        }

        // Copies the values in this Hashtable to an KeyValuePairs array.
        // KeyValuePairs is different from Dictionary Entry in that it has special
        // debugger attributes on its fields.

        internal virtual KeyValuePairs[] ToKeyValuePairsArray()
        {

            KeyValuePairs[] array = new KeyValuePairs[count];
            int index = 0;
            bucket[] lbuckets = buckets;
            for (int i = lbuckets.Length; --i >= 0;)
            {
                Object keyv = lbuckets[i].key;
                if ((keyv != null) && (keyv != buckets))
                {
                    array[index++] = new KeyValuePairs(keyv, lbuckets[i].val);
                }
            }

            return array;
        }


        // Copies the values of this hashtable to a given array starting at a given
        // index. This method is used by the implementation of the CopyTo method in
        // the ValueCollection class.
        private void CopyValues(Array array, int arrayIndex)
        {

            bucket[] lbuckets = buckets;
            for (int i = lbuckets.Length; --i >= 0;)
            {
                Object keyv = lbuckets[i].key;
                if ((keyv != null) && (keyv != buckets))
                {
                    array.SetValue(lbuckets[i].val, arrayIndex++);
                }
            }
        }

        // Returns the value associated with the given key. If an entry with the
        // given key is not found, the returned value is null.
        // 
        public virtual Object this[Object key]
        {
            get
            {
                if (key == null)
                {
                    throw new ArgumentNullException("key", "ArgumentNull_Key");
                }


                uint seed;
                uint incr;


                // Take a snapshot of buckets, in case another thread does a resize
                bucket[] lbuckets = buckets;
                uint hashcode = InitHash(key, lbuckets.Length, out seed, out incr);
                int ntry = 0;

                bucket b;
                int bucketNumber = (int)(seed % (uint)lbuckets.Length);
                do
                {
                    int currentversion;

                    //     A read operation on hashtable has three steps:
                    //        (1) calculate the hash and find the slot number.
                    //        (2) compare the hashcode, if equal, go to step 3. Otherwise end.
                    //        (3) compare the key, if equal, go to step 4. Otherwise end.
                    //        (4) return the value contained in the bucket.
                    //     After step 3 and before step 4. A writer can kick in a remove the old item and add a new one 
                    //     in the same bukcet. So in the reader we need to check if the hash table is modified during above steps.
                    //
                    // Writers (Insert, Remove, Clear) will set 'isWriterInProgress' flag before it starts modifying 
                    // the hashtable and will ckear the flag when it is done.  When the flag is cleared, the 'version'
                    // will be increased.  We will repeat the reading if a writer is in progress or done with the modification 
                    // during the read.
                    //
                    // Our memory model guarantee if we pick up the change in bucket from another processor, 
                    // we will see the 'isWriterProgress' flag to be true or 'version' is changed in the reader.
                    //                    
                    int spinCount = 0;
                    do
                    {
                        // this is violate read, following memory accesses can not be moved ahead of it.
                        currentversion = version;
                        b = lbuckets[bucketNumber];

                        // The contention between reader and writer shouldn't happen frequently.
                        // But just in case this will burn CPU, yield the control of CPU if we spinned a few times.
                        // 8 is just a random number I pick. 
                        if ((++spinCount) % 8 == 0)
                        {
                            Thread.Sleep(1);   // 1 means we are yeilding control to all threads, including low-priority ones.
                        }
                    } while (isWriterInProgress || (currentversion != version));

                    if (b.key == null)
                    {
                        return null;
                    }
                    if (((b.hash_coll & 0x7FFFFFFF) == hashcode) &&
                        KeyEquals(b.key, key))
                        return b.val;
                    bucketNumber = (int)(((long)bucketNumber + incr) % (uint)lbuckets.Length);
                } while (b.hash_coll < 0 && ++ntry < lbuckets.Length);
                return null;
            }

            set
            {
                Insert(key, value, false);
            }
        }

        // Returns the object to synchronize on for this hash table.
        public virtual Object SyncRoot
        {
            get
            {
                if (_syncRoot == null)
                {
                    System.Threading.Interlocked.CompareExchange<Object>(ref _syncRoot, new Object(), null);
                }
                return _syncRoot;
            }
        }

        // Returns the number of associations in this hashtable.
        // 
        public virtual int Count
        {
            get { return count; }
        }

        //// Returns a thread-safe wrapper for a Hashtable.
        //public static Hashtable Synchronized(Hashtable table) {
        //    if (table==null)
        //        throw new ArgumentNullException("table");

        //    return new SyncHashtable(table);
        //}


        // The ISerializable Implementation
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            // This is imperfect - it only works well if all other writes are
            // also using our synchronized wrapper.  But it's still a good idea.
            lock (SyncRoot)
            {
                // This method hasn't been fully tweaked to be safe for a concurrent writer.
                int oldVersion = version;
                info.AddValue(LoadFactorName, loadFactor);
                info.AddValue(VersionName, version);

                //
                // We need to maintain serialization compatibility with Everett and RTM.
                // If the comparer is null or a compatible comparer, serialize Hashtable
                // in a format that can be deserialized on Everett and RTM.            
                //
                // Also, if the Hashtable is using randomized hashing, serialize the old
                // view of the _keycomparer so perevious frameworks don't see the new types
#pragma warning disable 618
#if FEATURE_RANDOMIZED_STRING_HASHING
            IEqualityComparer keyComparerForSerilization = (IEqualityComparer) HashHelpers.GetEqualityComparerForSerialization(_keycomparer);
#else
                IEqualityComparer keyComparerForSerilization = _keycomparer;
#endif

                if (keyComparerForSerilization == null)
                {
                    info.AddValue(ComparerName, null, typeof(IComparer));
                    info.AddValue(HashCodeProviderName, null, typeof(IHashCodeProvider));
                }
                else if (keyComparerForSerilization is CompatibleComparer)
                {
                    CompatibleComparer c = keyComparerForSerilization as CompatibleComparer;
                    info.AddValue(ComparerName, c.Comparer, typeof(IComparer));
                    info.AddValue(HashCodeProviderName, c.HashCodeProvider, typeof(IHashCodeProvider));
                }
                else
                {
                    info.AddValue(KeyComparerName, keyComparerForSerilization, typeof(IEqualityComparer));
                }
#pragma warning restore 618

                info.AddValue(HashSizeName, buckets.Length); //This is the length of the bucket array.
                Object[] serKeys = new Object[count];
                Object[] serValues = new Object[count];
                CopyKeys(serKeys, 0);
                CopyValues(serValues, 0);
                info.AddValue(KeysName, serKeys, typeof(Object[]));
                info.AddValue(ValuesName, serValues, typeof(Object[]));

                // Explicitly check to see if anyone changed the Hashtable while we 
                // were serializing it.  That's a ---- in their code.
                if (version != oldVersion)
                    throw new InvalidOperationException();
            }
        }

        //
        // DeserializationEvent Listener 
        //
        public virtual void OnDeserialization(Object sender)
        {
            if (buckets != null)
            {
                // Somebody had a dependency on this hashtable and fixed us up before the ObjectManager got to it.
                return;
            }

            SerializationInfo siInfo;
            HashHelpers.SerializationInfoTable.TryGetValue(this, out siInfo);

            if (siInfo == null)
            {
                throw new SerializationException();
            }

            int hashsize = 0;
            IComparer c = null;

#pragma warning disable 618
            IHashCodeProvider hcp = null;
#pragma warning restore 618        

            Object[] serKeys = null;
            Object[] serValues = null;

            SerializationInfoEnumerator enumerator = siInfo.GetEnumerator();

            while (enumerator.MoveNext())
            {
                switch (enumerator.Name)
                {
                    case LoadFactorName:
                        loadFactor = siInfo.GetSingle(LoadFactorName);
                        break;
                    case HashSizeName:
                        hashsize = siInfo.GetInt32(HashSizeName);
                        break;
                    case KeyComparerName:
                        _keycomparer = (IEqualityComparer)siInfo.GetValue(KeyComparerName, typeof(IEqualityComparer));
                        break;
                    case ComparerName:
                        c = (IComparer)siInfo.GetValue(ComparerName, typeof(IComparer));
                        break;
                    case HashCodeProviderName:
#pragma warning disable 618
                        hcp = (IHashCodeProvider)siInfo.GetValue(HashCodeProviderName, typeof(IHashCodeProvider));
#pragma warning restore 618        
                        break;
                    case KeysName:
                        serKeys = (Object[])siInfo.GetValue(KeysName, typeof(Object[]));
                        break;
                    case ValuesName:
                        serValues = (Object[])siInfo.GetValue(ValuesName, typeof(Object[]));
                        break;
                }
            }

            loadsize = (int)(loadFactor * hashsize);

            // V1 object doesn't has _keycomparer field.
            if ((_keycomparer == null) && ((c != null) || (hcp != null)))
            {
                _keycomparer = new CompatibleComparer(c, hcp);
            }

            buckets = new bucket[hashsize];

            if (serKeys == null)
            {
                throw new SerializationException();
            }
            if (serValues == null)
            {
                throw new SerializationException();
            }
            if (serKeys.Length != serValues.Length)
            {
                throw new SerializationException();
            }
            for (int i = 0; i < serKeys.Length; i++)
            {
                if (serKeys[i] == null)
                {
                    throw new SerializationException();
                }
                Insert(serKeys[i], serValues[i], true);
            }

            version = siInfo.GetInt32(VersionName);

            HashHelpers.SerializationInfoTable.Remove(this);
        }

        // Implements a Collection for the keys of a hashtable. An instance of this
        // class is created by the GetKeys method of a hashtable.
        [Serializable]
        private class KeyCollection : ICollection
        {
            private MyHashtable _hashtable;

            internal KeyCollection(MyHashtable hashtable)
            {
                _hashtable = hashtable;
            }

            public virtual void CopyTo(Array array, int arrayIndex)
            {
                if (array == null)
                    throw new ArgumentNullException("array");
                if (array.Rank != 1)
                    throw new ArgumentException("Arg_RankMultiDimNotSupported");
                if (arrayIndex < 0)
                    throw new ArgumentOutOfRangeException("arrayIndex", "ArgumentOutOfRange_NeedNonNegNum");

                if (array.Length - arrayIndex < _hashtable.count)
                    throw new ArgumentException("Arg_ArrayPlusOffTooSmall");
                _hashtable.CopyKeys(array, arrayIndex);
            }


            public virtual IEnumerator GetEnumerator()
            {
                return new HashtableEnumerator(_hashtable, HashtableEnumerator.Keys);
            }

            public virtual bool IsSynchronized
            {
                get { return _hashtable.IsSynchronized; }
            }

            public virtual Object SyncRoot
            {
                get { return _hashtable.SyncRoot; }
            }

            public virtual int Count
            {
                get { return _hashtable.count; }
            }
        }

        // Implements a Collection for the values of a hashtable. An instance of
        // this class is created by the GetValues method of a hashtable.
        [Serializable]
        private class ValueCollection : ICollection
        {
            private MyHashtable _hashtable;

            internal ValueCollection(MyHashtable hashtable)
            {
                _hashtable = hashtable;
            }

            public virtual void CopyTo(Array array, int arrayIndex)
            {
                if (array == null)
                    throw new ArgumentNullException("array");
                if (array.Rank != 1)
                    throw new ArgumentException();
                if (arrayIndex < 0)
                    throw new ArgumentOutOfRangeException();

                if (array.Length - arrayIndex < _hashtable.count)
                    throw new ArgumentException();
                _hashtable.CopyValues(array, arrayIndex);
            }


            public virtual IEnumerator GetEnumerator()
            {
                return new HashtableEnumerator(_hashtable, HashtableEnumerator.Values);
            }

            public virtual bool IsSynchronized
            {
                get { return _hashtable.IsSynchronized; }
            }

            public virtual Object SyncRoot
            {
                get { return _hashtable.SyncRoot; }
            }

            public virtual int Count
            {
                get { return _hashtable.count; }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new HashtableEnumerator(this, HashtableEnumerator.DictEntry);
        }

        // Returns a dictionary enumerator for this hashtable.
        // If modifications made to the hashtable while an enumeration is
        // in progress, the MoveNext and Current methods of the
        // enumerator will throw an exception.
        //

        public virtual IDictionaryEnumerator GetEnumerator()
        {
            return new HashtableEnumerator(this, HashtableEnumerator.DictEntry);
        }

        #endregion

        #region Synchronized wrapper

        //// Synchronized wrapper for hashtable
        //[Serializable]
        //private class SyncHashtable : MyHashtable, IEnumerable
        //{
        //    protected MyHashtable _table;

        //    internal SyncHashtable(MyHashtable table) : base(false)
        //    {
        //        _table = table;
        //    }

        //    internal SyncHashtable(SerializationInfo info, StreamingContext context) : base(info, context)
        //    {
        //        _table = (Hashtable)info.GetValue("ParentTable", typeof(Hashtable));
        //        if (_table == null)
        //        {
        //            throw new SerializationException("Serialization_InsufficientState");
        //        }
        //    }


        //    /*================================GetObjectData=================================
        //    **Action: Return a serialization info containing a reference to _table.  We need
        //    **        to implement this because our parent HT does and we don't want to actually
        //    **        serialize all of it's values (just a reference to the table, which will then
        //    **        be serialized separately.)
        //    **Returns: void
        //    **Arguments: info -- the SerializationInfo into which to store the data.
        //    **           context -- the StreamingContext for the current serialization (ignored)
        //    **Exceptions: ArgumentNullException if info is null.
        //    ==============================================================================*/
        //    [System.Security.SecurityCritical]  // auto-generated
        //    public override void GetObjectData(SerializationInfo info, StreamingContext context)
        //    {
        //        if (info == null)
        //        {
        //            throw new ArgumentNullException("info");
        //        }

        //        // Our serialization code hasn't been fully tweaked to be safe 
        //        // for a concurrent writer.
        //        lock (_table.SyncRoot)
        //        {
        //            info.AddValue("ParentTable", _table, typeof(Hashtable));
        //        }
        //    }

        //    public override int Count
        //    {
        //        get { return _table.Count; }
        //    }

        //    public override bool IsReadOnly
        //    {
        //        get { return _table.IsReadOnly; }
        //    }

        //    public override bool IsFixedSize
        //    {
        //        get { return _table.IsFixedSize; }
        //    }

        //    public override bool IsSynchronized
        //    {
        //        get { return true; }
        //    }

        //    public override Object this[Object key]
        //    {

        //        get
        //        {
        //            return _table[key];
        //        }
        //        set
        //        {
        //            lock (_table.SyncRoot)
        //            {
        //                _table[key] = value;
        //            }
        //        }
        //    }

        //    public override Object SyncRoot
        //    {
        //        get { return _table.SyncRoot; }
        //    }

        //    public override void Add(Object key, Object value)
        //    {
        //        lock (_table.SyncRoot)
        //        {
        //            _table.Add(key, value);
        //        }
        //    }

        //    public override void Clear()
        //    {
        //        lock (_table.SyncRoot)
        //        {
        //            _table.Clear();
        //        }
        //    }

        //    public override bool Contains(Object key)
        //    {
        //        return _table.Contains(key);
        //    }


        //    public override bool ContainsKey(Object key)
        //    {
        //        if (key == null)
        //        {
        //            throw new ArgumentNullException("key", "ArgumentNull_Key");
        //        }
        //        return _table.ContainsKey(key);
        //    }

        //    public override bool ContainsValue(Object key)
        //    {
        //        lock (_table.SyncRoot)
        //        {
        //            return _table.ContainsValue(key);
        //        }
        //    }

        //    public override void CopyTo(Array array, int arrayIndex)
        //    {
        //        lock (_table.SyncRoot)
        //        {
        //            _table.CopyTo(array, arrayIndex);
        //        }
        //    }

        //    public override Object Clone()
        //    {
        //        lock (_table.SyncRoot)
        //        {
        //            return Hashtable.Synchronized((Hashtable)_table.Clone());
        //        }
        //    }

        //    IEnumerator IEnumerable.GetEnumerator()
        //    {
        //        return _table.GetEnumerator();
        //    }

        //    public override IDictionaryEnumerator GetEnumerator()
        //    {
        //        return _table.GetEnumerator();
        //    }

        //    public override ICollection Keys
        //    {
        //        get
        //        {
        //            lock (_table.SyncRoot)
        //            {
        //                return _table.Keys;
        //            }
        //        }
        //    }

        //    public override ICollection Values
        //    {
        //        get
        //        {
        //            lock (_table.SyncRoot)
        //            {
        //                return _table.Values;
        //            }
        //        }
        //    }

        //    public override void Remove(Object key)
        //    {
        //        lock (_table.SyncRoot)
        //        {
        //            _table.Remove(key);
        //        }
        //    }

        //    /*==============================OnDeserialization===============================
        //    **Action: Does nothing.  We have to implement this because our parent HT implements it,
        //    **        but it doesn't do anything meaningful.  The real work will be done when we
        //    **        call OnDeserialization on our parent table.
        //    **Returns: void
        //    **Arguments: None
        //    **Exceptions: None
        //    ==============================================================================*/
        //    public override void OnDeserialization(Object sender)
        //    {
        //        return;
        //    }

        //    internal override KeyValuePairs[] ToKeyValuePairsArray()
        //    {
        //        return _table.ToKeyValuePairsArray();
        //    }

        //}

        #endregion

        #region HashtableEnumerator

        // Implements an enumerator for a hashtable. The enumerator uses the
        // internal version number of the hashtabke to ensure that no modifications
        // are made to the hashtable while an enumeration is in progress.
        [Serializable]
        private class HashtableEnumerator : IDictionaryEnumerator, ICloneable
        {
            private MyHashtable hashtable;
            private int bucket;
            private int version;
            private bool current;
            private int getObjectRetType;   // What should GetObject return?
            private Object currentKey;
            private Object currentValue;

            internal const int Keys = 1;
            internal const int Values = 2;
            internal const int DictEntry = 3;

            internal HashtableEnumerator(MyHashtable hashtable, int getObjRetType)
            {
                this.hashtable = hashtable;
                bucket = hashtable.buckets.Length;
                version = hashtable.version;
                current = false;
                getObjectRetType = getObjRetType;
            }

            public Object Clone()
            {
                return MemberwiseClone();
            }

            public virtual Object Key
            {
                get
                {
                    if (current == false) throw new InvalidOperationException();
                    return currentKey;
                }
            }

            public virtual bool MoveNext()
            {
                if (version != hashtable.version) throw new InvalidOperationException();
                while (bucket > 0)
                {
                    bucket--;
                    Object keyv = hashtable.buckets[bucket].key;
                    if ((keyv != null) && (keyv != hashtable.buckets))
                    {
                        currentKey = keyv;
                        currentValue = hashtable.buckets[bucket].val;
                        current = true;
                        return true;
                    }
                }
                current = false;
                return false;
            }

            public virtual DictionaryEntry Entry
            {
                get
                {
                    if (current == false) throw new InvalidOperationException();
                    return new DictionaryEntry(currentKey, currentValue);
                }
            }


            public virtual Object Current
            {
                get
                {
                    if (current == false) throw new InvalidOperationException();

                    if (getObjectRetType == Keys)
                        return currentKey;
                    else if (getObjectRetType == Values)
                        return currentValue;
                    else
                        return new DictionaryEntry(currentKey, currentValue);
                }
            }

            public virtual Object Value
            {

                get
                {
                    if (current == false) throw new InvalidOperationException();
                    return currentValue;
                }
            }

            public virtual void Reset()
            {
                if (version != hashtable.version) throw new InvalidOperationException();
                current = false;
                bucket = hashtable.buckets.Length;
                currentKey = null;
                currentValue = null;
            }
        }

        #endregion

        #region 辅助方法

        protected virtual int GetHash(Object key)
        {
            if (_keycomparer != null)
                return _keycomparer.GetHashCode(key);
            return key.GetHashCode();
        }

        protected virtual bool KeyEquals(Object item, Object key)
        {

            if (Object.ReferenceEquals(buckets, item))
            {
                return false;
            }

            if (Object.ReferenceEquals(item, key))
                return true;

            if (_keycomparer != null)
                return _keycomparer.Equals(item, key);
            return item == null ? false : item.Equals(key);
        }


        #endregion


    }


}
