﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace BaseStruct
{
    internal static class EmptyArray<T>
    {
        public static readonly T[] Value = new T[0];
    }
    public class MyArray: IList, ICloneable
    {
        private Object[] _items;
       
        private int _size;
        // Read-only property describing how many elements are in the List.
        public virtual int Count
        {
            get
            {
                return _size;
            }
        }

        private int _version;
        [NonSerialized]
        private Object _syncRoot;

        private const int _defaultCapacity = 4;

        //全局统一的空数组实例
        private static readonly Object[] emptyArray = EmptyArray<Object>.Value;

        // Note: this constructor is a bogus constructor that does nothing
        // and is for use only with SyncMyArray.
        internal MyArray(bool trash)
        {
        }

        // Constructs a MyArray. The list is initially empty and has a capacity
        // of zero. Upon adding the first element to the list the capacity is
        // increased to _defaultCapacity, and then increased in multiples of two as required.
        public MyArray()
        {
            _items = emptyArray;
        }

        // Constructs a MyArray with a given initial capacity. The list is
        // initially empty, but will have room for the given number of elements
        // before any reallocations are required.
        // 
        public MyArray(int capacity)
        {
            if (capacity < 0) throw new ArgumentOutOfRangeException();
         
            if (capacity == 0)
                _items = emptyArray;
            else
                _items = new Object[capacity];
        }

        // Constructs a MyArray, copying the contents of the given collection. The
        // size and capacity of the new list will both be equal to the size of the
        // given collection.
        // 
        public MyArray(ICollection c)
        {
            if (c == null)
                throw new ArgumentNullException();
          
            int count = c.Count;
            if (count == 0)
            {
                _items = emptyArray;
            }
            else
            {
                _items = new Object[count];
                AddRange(c);
            }
        }

        // Gets and sets the capacity of this list.  The capacity is the size of
        // the internal array used to hold items.  When set, the internal 
        // array of the list is reallocated to the given capacity.
        // 
        public virtual int Capacity
        {
            get
            {
                return _items.Length;
            }
            set
            {
                if (value < _size)
                {
                    throw new ArgumentOutOfRangeException();
                }
                // We don't want to update the version number when we change the capacity.
                // Some existing applications have dependency on this.
                if (value != _items.Length)
                {
                    if (value > 0)
                    {
                        Object[] newItems = new Object[value];
                        if (_size > 0)
                        {
                            Array.Copy(_items, 0, newItems, 0, _size);
                        }
                        _items = newItems;
                    }
                    else
                    {
                        _items = new Object[_defaultCapacity];
                    }
                }
            }
        }

  

        public virtual bool IsFixedSize
        {
            get { return false; }
        }


        // Is this MyArray read-only?
        public virtual bool IsReadOnly
        {
            get { return false; }
        }

        // Is this MyArray synchronized (thread-safe)?
        public virtual bool IsSynchronized
        {
            get { return false; }
        }

        // Synchronization root for this object.
        public virtual Object SyncRoot
        {
            get
            {
                if (_syncRoot == null)
                {
                    System.Threading.Interlocked.CompareExchange<Object>(ref _syncRoot, new Object(), null);
                }
                return _syncRoot;
            }
        }

        // Sets or Gets the element at the given index.
        // 
        public virtual Object this[int index]
        {
            get
            {
                if (index < 0 || index >= _size) throw new ArgumentOutOfRangeException();
            
                return _items[index];
            }
            set
            {
                if (index < 0 || index >= _size) throw new ArgumentOutOfRangeException();
      
                _items[index] = value;
                _version++;
            }
        }

       

        // Adds the given object to the end of this list. The size of the list is
        // increased by one. If required, the capacity of the list is doubled
        // before adding the new element.
        public virtual int Add(Object value)
        {
            if (_size == _items.Length) EnsureCapacity(_size + 1);
            _items[_size] = value;
            _version++;
            return _size++;
        }

        // Adds the elements of the given collection to the end of this list. If
        // required, the capacity of the list is increased to twice the previous
        // capacity or the new size, whichever is larger.
        public virtual void AddRange(ICollection c)
        {
            InsertRange(_size, c);
        }

        // Clears the contents of MyArray.
        public virtual void Clear()
        {
            if (_size > 0)
            {
                Array.Clear(_items, 0, _size); // Don't need to doc this but we clear the elements so that the gc can reclaim the references.
                _size = 0;
            }
            _version++;
        }

        // Clones this MyArray, doing a shallow copy.  (A copy is made of all
        // Object references in the MyArray, but the Objects pointed to 
        // are not cloned).
        public virtual Object Clone()
        {
            MyArray la = new MyArray(_size);
            la._size = _size;
            la._version = _version;
            Array.Copy(_items, 0, la._items, 0, _size);
            return la;
        }


        // Contains returns true if the specified element is in the MyArray.
        // It does a linear, O(n) search.  Equality is determined by calling
        // item.Equals().
        public virtual bool Contains(Object item)
        {
            if (item == null)
            {
                for (int i = 0; i < _size; i++)
                    if (_items[i] == null)
                        return true;
                return false;
            }
            else
            {
                for (int i = 0; i < _size; i++)
                    if ((_items[i] != null) && (_items[i].Equals(item)))
                        return true;
                return false;
            }
        }

        // Copies this MyArray into array, which must be of a 
        // compatible array type.  
        public virtual void CopyTo(Array array)
        {
            CopyTo(array, 0);
        }

        // Copies this MyArray into array, which must be of a 
        // compatible array type.  
        public virtual void CopyTo(Array array, int arrayIndex)
        {
            if ((array != null) && (array.Rank != 1))
                throw new ArgumentException();
            // Delegate rest of error checking to Array.Copy.
            Array.Copy(_items, 0, array, arrayIndex, _size);
        }

        // Copies a section of this list to the given array at the given index.
        // The method uses the Array.Copy method to copy the elements.
        public virtual void CopyTo(int index, Array array, int arrayIndex, int count)
        {
            if (_size - index < count)
                throw new ArgumentException();
            if ((array != null) && (array.Rank != 1))
                throw new ArgumentException();
            // Delegate rest of error checking to Array.Copy.
            Array.Copy(_items, index, array, arrayIndex, count);
        }

        // Ensures that the capacity of this list is at least the given minimum
        // value. If the currect capacity of the list is less than min, the
        // capacity is increased to twice the current capacity or to min,
        // whichever is larger.
        private void EnsureCapacity(int min)
        {
            if (_items.Length < min)
            {
                int newCapacity = _items.Length == 0 ? _defaultCapacity : _items.Length * 2;
                // Allow the list to grow to maximum possible capacity (~2G elements) before encountering overflow.
                // Note that this check works even when _items.Length overflowed thanks to the (uint) cast
                //0X7FEFFFFF为Array.MaxArrayLength  等于2146435071
                if ((uint)newCapacity > 0X7FEFFFFF) newCapacity = 0X7FEFFFFF;
                if (newCapacity < min) newCapacity = min;
                Capacity = newCapacity;
            }
        }

        // Returns an enumerator for this list with the given
        // permission for removal of elements. If modifications made to the list 
        // while an enumeration is in progress, the MoveNext and 
        // GetObject methods of the enumerator will throw an exception.
        public virtual IEnumerator GetEnumerator()
        {
            return new MyArrayEnumeratorSimple(this);
        }
        public virtual IEnumerator GetEnumerator(int index, int count)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException();
            if (count < 0)
                throw new ArgumentOutOfRangeException();
            if (_size - index < count)
                throw new ArgumentException();
            return new MyArrayEnumerator(this, index, count);
        }

        // Returns the index of the first occurrence of a given value in a range of
        // this list. The list is searched forwards from beginning to end.
        // The elements of the list are compared to the given value using the
        // Object.Equals method.
        // This method uses the Array.IndexOf method to perform the
        // search.
        public virtual int IndexOf(Object value)
        {
            return Array.IndexOf((Array)_items, value, 0, _size);
        }

        // Returns the index of the first occurrence of a given value in a range of
        // this list. The list is searched forwards, starting at index
        // startIndex and ending at count number of elements. The
        // elements of the list are compared to the given value using the
        // Object.Equals method.
        // 
        // This method uses the Array.IndexOf method to perform the
        // search.
        // 
        public virtual int IndexOf(Object value, int startIndex)
        {
            if (startIndex > _size)
                throw new ArgumentOutOfRangeException();
           
            return Array.IndexOf((Array)_items, value, startIndex, _size - startIndex);
        }

        // Returns the index of the first occurrence of a given value in a range of
        // this list. The list is searched forwards, starting at index
        // startIndex and upto count number of elements. The
        // elements of the list are compared to the given value using the
        // Object.Equals method.
        // 
        // This method uses the Array.IndexOf method to perform the
        // search.
        // 
        public virtual int IndexOf(Object value, int startIndex, int count)
        {
            if (startIndex > _size)
                throw new ArgumentOutOfRangeException();
            if (count < 0 || startIndex > _size - count) throw new ArgumentOutOfRangeException();
           
            return Array.IndexOf((Array)_items, value, startIndex, count);
        }

        // Inserts an element into this list at a given index. The size of the list
        // is increased by one. If required, the capacity of the list is doubled
        // before inserting the new element.
        // 
        public virtual void Insert(int index, Object value)
        {
            // Note that insertions at the end are legal.
            if (index < 0 || index > _size) throw new ArgumentOutOfRangeException();
            if (_size == _items.Length) EnsureCapacity(_size + 1);
            if (index < _size)
            {
                Array.Copy(_items, index, _items, index + 1, _size - index);
            }
            _items[index] = value;
            _size++;
            _version++;
        }

        // Inserts the elements of the given collection at a given index. If
        // required, the capacity of the list is increased to twice the previous
        // capacity or the new size, whichever is larger.  Ranges may be added
        // to the end of the list by setting index to the MyArray's size.
        //
        public virtual void InsertRange(int index, ICollection c)
        {
            if (c == null)
                throw new ArgumentNullException();
            if (index < 0 || index > _size) throw new ArgumentOutOfRangeException();
            int count = c.Count;
            if (count > 0)
            {
                EnsureCapacity(_size + count);
                // shift existing items
                if (index < _size)
                {
                    Array.Copy(_items, index, _items, index + count, _size - index);
                }

                Object[] itemsToInsert = new Object[count];
                c.CopyTo(itemsToInsert, 0);
                itemsToInsert.CopyTo(_items, index);
                _size += count;
                _version++;
            }
        }

        // Returns the index of the last occurrence of a given value in a range of
        // this list. The list is searched backwards, starting at the end 
        // and ending at the first element in the list. The elements of the list 
        // are compared to the given value using the Object.Equals method.
        // 
        // This method uses the Array.LastIndexOf method to perform the
        // search.
        // 
        public virtual int LastIndexOf(Object value)
        {
            return LastIndexOf(value, _size - 1, _size);
        }

        // Returns the index of the last occurrence of a given value in a range of
        // this list. The list is searched backwards, starting at index
        // startIndex and ending at the first element in the list. The 
        // elements of the list are compared to the given value using the 
        // Object.Equals method.
        // 
        // This method uses the Array.LastIndexOf method to perform the
        // search.
        // 
        public virtual int LastIndexOf(Object value, int startIndex)
        {
            if (startIndex >= _size)
                throw new ArgumentOutOfRangeException();
            return LastIndexOf(value, startIndex, startIndex + 1);
        }

        // Returns the index of the last occurrence of a given value in a range of
        // this list. The list is searched backwards, starting at index
        // startIndex and upto count elements. The elements of
        // the list are compared to the given value using the Object.Equals
        // method.
        // 
        // This method uses the Array.LastIndexOf method to perform the
        // search.
        // 
        public virtual int LastIndexOf(Object value, int startIndex, int count)
        {
            if (Count != 0 && (startIndex < 0 || count < 0))
                throw new ArgumentOutOfRangeException();
            if (_size == 0)  // Special case for an empty list
                return -1;

            if (startIndex >= _size || count > startIndex + 1)
                throw new ArgumentOutOfRangeException();

            return Array.LastIndexOf((Array)_items, value, startIndex, count);
        }

        public virtual void Remove(Object obj)
        {
            int index = IndexOf(obj);
            if (index >= 0)
                RemoveAt(index);
        }

        // Removes the element at the given index. The size of the list is
        // decreased by one.
        public virtual void RemoveAt(int index)
        {
            if (index < 0 || index >= _size) throw new ArgumentOutOfRangeException();
            _size--;
            if (index < _size)
            {
                // Copies length elements from sourceArray, starting at sourceIndex, to
                // destinationArray, starting at destinationIndex.
                Array.Copy(_items, index + 1, _items, index, _size - index);
            }
            _items[_size] = null;
            _version++;
        }

        // Removes a range of elements from this list.
        public virtual void RemoveRange(int index, int count)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException();
            if (count < 0)
                throw new ArgumentOutOfRangeException();
            if (_size - index < count)
                throw new ArgumentException();

            if (count > 0)
            {
                int i = _size;
                _size -= count;
                if (index < _size)
                {
                    Array.Copy(_items, index + count, _items, index, _size - index);
                }
                while (i > _size) _items[--i] = null;
                _version++;
            }
        }

        // Returns an IList that contains count copies of value.
        public static MyArray Repeat(Object value, int count)
        {
            if (count < 0)
                throw new ArgumentOutOfRangeException();

            MyArray list = new MyArray((count > _defaultCapacity) ? count : _defaultCapacity);
            for (int i = 0; i < count; i++)
                list.Add(value);
            return list;
        }

        // Reverses the elements in this list.
        public virtual void Reverse()
        {
            Reverse(0, Count);
        }

        // Reverses the elements in a range of this list. Following a call to this
        // method, an element in the range given by index and count
        // which was previously located at index i will now be located at
        // index index + (index + count - i - 1).
        // 
        // This method uses the Array.Reverse method to reverse the
        // elements.
        public virtual void Reverse(int index, int count)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException();
            if (count < 0)
                throw new ArgumentOutOfRangeException();
            if (_size - index < count)
                throw new ArgumentException();
         
            Array.Reverse(_items, index, count);
            _version++;
        }

        // Sets the elements starting at the given index to the elements of the
        // given collection.
        public virtual void SetRange(int index, ICollection c)
        {
            if (c == null) throw new ArgumentNullException();
  
            int count = c.Count;
            if (index < 0 || index > _size - count) throw new ArgumentOutOfRangeException();

            if (count > 0)
            {
                c.CopyTo(_items, index);
                _version++;
            }
        }

        // Sorts the elements in this list.  Uses the default comparer and 
        // Array.Sort.
        public virtual void Sort()
        {
            Sort(0, Count, Comparer.Default);
        }

        // Sorts the elements in this list.  Uses Array.Sort with the
        // provided comparer.
        public virtual void Sort(IComparer comparer)
        {
            Sort(0, Count, comparer);
        }

        // Sorts the elements in a section of this list. The sort compares the
        // elements to each other using the given IComparer interface. If
        // comparer is null, the elements are compared to each other using
        // the IComparable interface, which in that case must be implemented by all
        // elements of the list.
        // This method uses the Array.Sort method to sort the elements.
        public virtual void Sort(int index, int count, IComparer comparer)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException();
            if (count < 0)
                throw new ArgumentOutOfRangeException();
            if (_size - index < count)
                throw new ArgumentException();

            Array.Sort(_items, index, count, comparer);
            _version++;
        }

        private sealed class MyArrayEnumerator : IEnumerator, ICloneable
        {
            private MyArray list;
            private int index;
            private int endIndex;       // Where to stop.
            private int version;
            private Object currentElement;
            private int startIndex;     // Save this for Reset.

            internal MyArrayEnumerator(MyArray list, int index, int count)
            {
                this.list = list;
                startIndex = index;
                this.index = index - 1;
                endIndex = this.index + count;  // last valid index
                version = list._version;
                currentElement = null;
            }

            public Object Clone()
            {
                return MemberwiseClone();
            }

            public bool MoveNext()
            {
                if (version != list._version) throw new InvalidOperationException();
                if (index < endIndex)
                {
                    currentElement = list[++index];
                    return true;
                }
                else
                {
                    index = endIndex + 1;
                }

                return false;
            }

            public Object Current
            {
                get
                {
                    if (index < startIndex)
                        throw new InvalidOperationException();
                    else if (index > endIndex)
                    {
                        throw new InvalidOperationException();
                    }
                    return currentElement;
                }
            }

            public void Reset()
            {
                if (version != list._version) throw new InvalidOperationException();
                index = startIndex - 1;
            }
        }

        [Serializable]
        private sealed class MyArrayEnumeratorSimple : IEnumerator, ICloneable
        {
            private MyArray list;
            private int index;
            private int version;
            private Object currentElement;
            [NonSerialized]
            private bool isArrayList;
            // this object is used to indicate enumeration has not started or has terminated
            static Object dummyObject = new Object();

            internal MyArrayEnumeratorSimple(MyArray list)
            {
                this.list = list;
                this.index = -1;
                version = list._version;
                isArrayList = (list.GetType() == typeof(MyArray));
                currentElement = dummyObject;
            }

            public Object Clone()
            {
                return MemberwiseClone();
            }

            public bool MoveNext()
            {
                if (version != list._version)
                {
                    throw new InvalidOperationException();
                }

                if (isArrayList)
                {  // avoid calling virtual methods if we are operating on ArrayList to improve performance
                    if (index < list._size - 1)
                    {
                        currentElement = list._items[++index];
                        return true;
                    }
                    else
                    {
                        currentElement = dummyObject;
                        index = list._size;
                        return false;
                    }
                }
                else
                {
                    if (index < list.Count - 1)
                    {
                        currentElement = list[++index];
                        return true;
                    }
                    else
                    {
                        index = list.Count;
                        currentElement = dummyObject;
                        return false;
                    }
                }
            }

            public Object Current
            {
                get
                {
                    object temp = currentElement;
                    if (dummyObject == temp)
                    { // check if enumeration has not started or has terminated
                        if (index == -1)
                        {
                            throw new InvalidOperationException();
                        }
                        else
                        {
                            throw new InvalidOperationException();
                        }
                    }
                    return temp;
                }
            }

            public void Reset()
            {
                if (version != list._version)
                {
                    throw new InvalidOperationException();
                }
                currentElement = dummyObject;
                index = -1;
            }
        }
    }
}
