﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EnumerableStudy
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] strList = new string[] { "123", "1231" };
            MyIEnumerable my = new MyIEnumerable(strList);
           
            var enumerator = my.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current);
                //enumerator.Current="";  这会报错
            }
            Console.WriteLine("-------------------------------");
            foreach (var item in my)
            {
                Console.WriteLine(item);
            }

            //FirstOrDefault 又是怎么实现的呢？
            Console.WriteLine(my.MyWhere(c=>c=="123").FirstOrDefault());
        }
    }
}
