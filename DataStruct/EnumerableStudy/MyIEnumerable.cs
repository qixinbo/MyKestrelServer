﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace EnumerableStudy
{
    public class MyIEnumerable : IEnumerable
    {
        private string[] strList;
        public MyIEnumerable(string[] strList)
        {
            this.strList = strList;
        }
        public IEnumerator GetEnumerator()
        {
            //return new MyIEnumerator(strList);
            //这种采用了yield  延时返回。
            for (int i = 0; i < strList.Length; i++)
            {
                yield return strList[i];
            }
        }

        public IEnumerable<string> MyWhere(Func<string, bool> func)
        {
            foreach (string item in this)
            {
                if (func(item))
                {
                    yield return item;
                }
            }
        }
    }
    public class MyIEnumerator : IEnumerator
    {
        private string[] strList;
        private int position;
        public MyIEnumerator(string[] strList)
        {
            this.strList = strList;
            position = -1;
        }
        public object Current
        {
            get { return strList[position]; }
        }
        public bool MoveNext()
        {
            position++;
            if (position < strList.Length)
            {
                return true;
            }
            return false;
        }
        public void Reset()
        {
            position = -1;
        }
    }
}
