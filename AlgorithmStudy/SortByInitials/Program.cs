﻿using System;
using System.Collections;
using System.Text;

namespace SortByInitials
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入要排序的字符串...");
            string str = Console.ReadLine();
            string[] strs = str.ToString().Split(' ');
            string[] resultStr = new string[strs.Length];
            for (int i = 0; i < strs.Length; i++)
            {
                ArrayList list = new ArrayList();
                StringBuilder tempStr = new StringBuilder();
                for (int j = 0; j < strs[i].Length; j++)
                {                   
                    list.Add(strs[i][j].ToString().Substring(0, 1));
                    strs[i][j].ToString().Substring(1);
                }
                list.Sort();
                for (int k = 0; k < list.Count; k++)
                {
                    tempStr.Append(list[k].ToString());
                }
                resultStr[i] = tempStr.ToString();
                
            }
            Console.WriteLine("排序后的结果：");
            for (int k = 0; k < resultStr.Length; k++)
            {
                Console.Write(resultStr[k]+" ");
            }
            Console.ReadLine(); 
        }
    }
}
