﻿using System;
using System.Collections.Generic;

namespace ClassSortAdapter
{
    class Program
    {
        static void Main()
        {
            Employee [] employees = new Employee[4];
            for (int i = 0; i < 4; i++)
            {
                employees[i] = new Employee();
            }
            employees[0].Age = 12;
            employees[1].Age = 34;
            employees[2].Age = 64;
            employees[3].Age = 53;
            Array.Sort(employees, new EmployeeSortAdapter());
            foreach (Employee emp in employees)
            {
                Console.WriteLine(emp.Age);
            }
            Console.Read();
        }
    }
    public class Employee
    {
        public int Age { get; set; }
    }
    class EmployeeSortAdapter :IComparer<Employee>
    {
        public int Compare(Employee emp1, Employee emp2)
        {
            if (emp1==null&&emp2==null)
            {
                return 0;
            }

            if (emp1==null)
            {
                return -1;
            }

            if (emp2==null)
            {
                return 1;
            }
      

            if (emp1.Age > emp2.Age)
            {
                return 1;
            }

            else if (emp1.Age < emp2.Age)
            {
                return -1;
            }
            return 0;
        }
    }
    
   
}
