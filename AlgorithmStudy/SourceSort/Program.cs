﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SourceSort
{
    class Program
    {
        static async Task Main()
        {

            Console.WriteLine(MultiChannels.GetScore(2222));
            CSRedis.CSRedisClient csredis = new CSRedis.CSRedisClient("192.168.5.244,password=,defaultDatabase=0,poolsize=100,ssl=false,writeBuffer=20480,prefix=");

            #region 基本使用

            RedisHelper.Initialization(csredis);

            #endregion

            //多渠道报价，记录报价的状态，  然后另外一个key(guid+_result)记录返回结果
            string guid = new Guid().ToString();

            string redisKey = guid + "_record";

            string redisResultKey = guid + "_result";

            await RedisHelper.SetAsync(redisKey, JsonConvert.SerializeObject(MultiChannels.Get()), TimeSpan.FromMinutes(30));

            #region 平安

            //这时候收到了平安的报价 source =1
            long pinganReceived = 1;

            await RedisHelper.SAddAsync(redisResultKey, pinganReceived);

            //如果是第一，直接发送  不是等都返回
            var res=await CheckScore(redisKey, pinganReceived);
            if (res.Code==1)
            {
                Console.WriteLine("推送报价成功");
            }

            #endregion

            #region 未知保司

            //这时候收到了平安的报价 source =1
            long unkonwReceived = 1000;

            await RedisHelper.Instance.SAddAsync(redisResultKey, unkonwReceived);
            //如果是第一，直接发送  不是第一的话看整个集合是什么情况
            if (MultiChannels.GetScore(unkonwReceived) == 1)
            {
                Console.WriteLine("推送报价成功");
            }
            else
            {
                await CheckScore(redisKey, unkonwReceived);
            }

            #endregion


            #region 超时事件

            await TimeOutEvent(guid);

            #endregion

          

        }

        public static async Task<BaseResponse> CheckScore(string key, long source)
        {
            var all = await RedisHelper.GetAsync(key);

            if (!string.IsNullOrEmpty(all))
            {
                var list = JsonConvert.DeserializeObject<List<MultiChannels>>(all);

                long min =list.Select(c=>MultiChannels.GetScore(c.Source)).Min();

                if (min==MultiChannels.GetScore(source))
                {
                    return BaseResponse.Ok();
                }
            }
            return BaseResponse.Failed();
        }

        public static async Task<BaseResponse> TimeOutEvent(string guid)
        {
            string redisKey = guid + "_record";
            string redisResultKey = guid + "_result";

            var quoteall = await RedisHelper.GetAsync(redisKey);
            var quotealllist = JsonConvert.DeserializeObject<List<MultiChannels>>(quoteall);

            var resultAll = await RedisHelper.SMembersAsync<long>(redisResultKey);

            var resultList =quotealllist.Where(c=>resultAll.Contains(c.Source)).ToList();

            if (resultList.Any())
            {
                MultiChannels minMultiChannels=resultList.FirstOrDefault(); 
                long minScore=minMultiChannels.ThisScore;
                foreach (var item in resultList)
                {
                    if (item.ThisScore<minScore)
                    {
                        minScore=item.ThisScore;
                        minMultiChannels=item;
                    }
                }

                Console.WriteLine( "最终推送的保司"+minMultiChannels.Source );
            }

            await RedisHelper.DelAsync(redisResultKey);
            var resultDelete = await RedisHelper.SMembersAsync<long>(redisResultKey);
            foreach (var item in resultDelete)
            {
                Console.WriteLine("删除失败");
            }
            

            return BaseResponse.Failed();
        }

    }
}
