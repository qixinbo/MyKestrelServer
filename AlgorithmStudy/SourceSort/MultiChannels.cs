﻿using System;
using System.Collections.Generic;

namespace SourceSort
{
    public class MultiChannels
    {
        /// <summary>
        /// 渠道id
        /// </summary>
        public string ChannelId { get; set; }
        /// <summary>
        /// 渠道名称
        /// </summary>
        public string ChannelName { get; set; }

        /// <summary>
        /// 保险公司
        /// </summary>
        public long Source { get; set; }

        /// <summary>
        ///  折扣系数是否修改 0未修改1已修改
        /// </summary>
        public int? DiscountChange { get; set; }

        public static List<MultiChannels> Get()
        {
            List<MultiChannels> list=new  List<MultiChannels> ()
            {
                new MultiChannels(){ Source=1,ChannelName="太平洋"},
                new MultiChannels(){ Source=2,ChannelName="平安"},
                new MultiChannels(){ Source=32,ChannelName="不知道谁"}
            };

            return list;
        }

        //人 太 平 国  不再这个序列就是-1
        public static Dictionary<long, long> Score = new Dictionary<long, long>() { { 4, 1 }, { 1, 2 }, { 2, 3 }, { 8, 3 } };

        public static long GetScore(long source)
        {
            if (Score.TryGetValue(source,out var score))
            {
                return score;
            }
            else
            {
                return Int64.MaxValue;
            }
        }


        public long ThisScore => GetScore(Source);


    }




}
