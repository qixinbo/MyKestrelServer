﻿namespace SourceSort
{
    public class BaseResponse
    {
        #region 属性

        public int Code { get; set; }
        public string Message { get; set; }

        #endregion

        #region 构造函数

        public BaseResponse()
        {

        }

        public BaseResponse(int code) : this(code, "")
        {

        }

        public BaseResponse(int code, string message)
        {
            Code = code;
            Message = message;
        }

        #endregion

        #region 扩展方法

        public static BaseResponse GetBaseResponse(int code)
        {
            return new BaseResponse(code);
        }

        public static BaseResponse GetBaseResponse(int code, string message)
        {
            return new BaseResponse(code, message);
        }

        #endregion

        #region 快捷方法

        /// <summary>
        ///     返回成功结果
        /// </summary>
        /// <param name="message">结果信息</param>
        /// <returns></returns>
        public static BaseResponse Failed(string message = null)
        {
            return GetBaseResponse(0);
        }

        /// <summary>
        ///     返回成功结果
        /// </summary>
        /// <param name="message">结果信息</param>
        /// <returns></returns>
        public static BaseResponse Ok(string message = null)
        {
           
            return GetBaseResponse(1);
        }

        #endregion      

    }


}
