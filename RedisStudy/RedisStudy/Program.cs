﻿using System;
using System.Threading.Tasks;

namespace RedisStudy
{
    class Program
    {
        static void Main(string[] args)
        {
            CSRedis.CSRedisClient csredis = new CSRedis.CSRedisClient("192.168.5.244,password=,defaultDatabase=0,poolsize=100,ssl=false,writeBuffer=20480,prefix=");

            StackExchange.Redis.ConnectionMultiplexer seredis = StackExchange.Redis.ConnectionMultiplexer.Connect("192.168.5.244,password=");
            StackExchange.Redis.IDatabase stackExchangeDb = seredis.GetDatabase();

         
            Task.Run(() =>
            {
                var times = new[] { 1000, 10000, 100000 };

                foreach (var time in times)
                {
                    var dt_csredis_set = DateTime.Now;
                    for (var a = 0; a < time; a++)
                    {
                        csredis.Set($"benchmark_csredis_{a}", Guid.NewGuid().ToString());
                    }
                    var ts_csredis_set = DateTime.Now.Subtract(dt_csredis_set);

                    var dt_seredis_set = DateTime.Now;
                    for (var a = 0; a < time; a++)
                    {
                        stackExchangeDb.StringSet($"benchmark_seredis_{a}", Guid.NewGuid().ToString());
                    }
                    var ts_seredis_set = DateTime.Now.Subtract(dt_seredis_set);


                    var dt_seredis_setAsync = DateTime.Now;
                    for (var a = 0; a < time; a++)
                    {
                        stackExchangeDb.StringSetAsync($"benchmark_seredis_Async{a}", Guid.NewGuid().ToString()).GetAwaiter().GetResult();
                    }
                    var ts_seredis_setAsync = DateTime.Now.Subtract(dt_seredis_setAsync);

                   
                    var dt_csredis_get = DateTime.Now;
                    for (var a = 0; a < time; a++)
                    {
                        csredis.Get($"benchmark_csredis_{a}");
                    }
                    var ts_csredis_get = DateTime.Now.Subtract(dt_csredis_get);

                    var dt_seredis_get = DateTime.Now;
                    for (var a = 0; a < time; a++)
                    {
                        stackExchangeDb.StringGet($"benchmark_seredis_{a}");
                    }
                    var ts_seredis_get = DateTime.Now.Subtract(dt_seredis_get);

                    var dt_seredis_getAsync = DateTime.Now;
                    for (var a = 0; a < time; a++)
                    {
                        stackExchangeDb.StringGetAsync($"benchmark_seredis_Async{a}").GetAwaiter().GetResult();
                    }
                    var ts_seredis_getAsync = DateTime.Now.Subtract(dt_seredis_getAsync);

                    Console.WriteLine($@"循环次数: {time}
    csredis set: {ts_csredis_set.TotalMilliseconds}ms
    statckexchange.redis StringSet: {ts_seredis_set.TotalMilliseconds}ms
    statckexchange.redis StringSettAsync: {ts_seredis_setAsync.TotalMilliseconds}ms

    csredis get: {ts_csredis_get.TotalMilliseconds}ms
    statckexchange.redis StringGet: {ts_seredis_get.TotalMilliseconds}ms
    statckexchange.redis StringGetAsync: {ts_seredis_getAsync.TotalMilliseconds}ms
");

                }
            });

            Console.ReadKey();
        }
    }
}
