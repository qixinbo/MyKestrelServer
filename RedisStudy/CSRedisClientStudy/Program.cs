﻿using System;
using System.Threading.Tasks;

namespace CSRedisClientStudy
{
    class Program
    {
        static  async Task Main(string[] args)
        {
            CSRedis.CSRedisClient csredis = new CSRedis.CSRedisClient("192.168.5.244,password=,defaultDatabase=0,poolsize=100,ssl=false,writeBuffer=20480,prefix=");

            #region 基本使用

            RedisHelper.Initialization(csredis);

            #endregion

            #region 基础

            string redisKeySubmitQuote = "_SubmitQuote_" ;
            var a =await RedisHelper.DelAsync(redisKeySubmitQuote);
            Console.WriteLine(a);

            #endregion


            #region hash

            //RedisHelper.SetAsync

            //await RedisHelper.Instance.HMSetAsync("key1","string1", "1231","class1",new TestClass { Id = 1, Name = "Class名称", CreateTime = DateTime.Now, TagId = new[] { 1, 3, 3, 3, 3 } });
            //var a= await RedisHelper.Instance.HMGetAsync("key1","string1");
            //Console.WriteLine(a[0]);

            //var b= await RedisHelper.Instance.HMGetAsync("key1","class1");
            //Console.WriteLine(b[0]);

            //var all =await RedisHelper.Instance.HValsAsync("key1");
            //var allKey =await RedisHelper.Instance.HKeysAsync("key1");

            //foreach (var item in allKey)
            //{
            //    Console.WriteLine(item);
            //}


            //foreach (var item in all)
            //{
            //    Console.WriteLine(item);
            //}

            //await RedisHelper.Instance.HMSetAsync("key2","TestClassNoToString",new TestClassNoToString { Id = 1, Name = "Class名称", CreateTime = DateTime.Now, TagId = new[] { 1, 3, 3, 3, 3 } });

            //var c= await RedisHelper.Instance.HMGetAsync("key2","TestClassNoToString");
            //Console.WriteLine(c[0]);

            #endregion

            #region set

            //await RedisHelper.Instance.SAddAsync("TestSPopWithCount", "string1", "string2", "string3");

            //var count =await RedisHelper.Instance.SCardAsync("TestSPopWithCount");

            //await RedisHelper.Instance.SRemAsync("TestSPopWithCount","string1");

            //var all=await RedisHelper.Instance.SMembersAsync<string>("TestSPopWithCount");

            //foreach (var item in all)
            //{
            //    Console.WriteLine(item);
            //}

            #endregion

        }
    }

    public class TestClass {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateTime { get; set; }

        public int[] TagId { get; set; }

        public override string ToString() {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }

    public class TestClassNoToString {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateTime { get; set; }

        public int[] TagId { get; set; }

    
    }
}
