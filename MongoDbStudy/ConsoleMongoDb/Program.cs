﻿using System;
using MongoDB.Bson;
using MongoDB.Driver;

namespace ConsoleMongoDb
{
    class Program
    {
        static void Main(string[] args)
        {

            var client = new MongoClient(
                "mongodb://192.168.4.43:27017"
            );
            IMongoDatabase database = client.GetDatabase("hello");

            var collection = database.GetCollection<BsonDocument>("test");
         
            var documentFirstOrDefault = collection.Find(new BsonDocument()).FirstOrDefault();
            Console.WriteLine(documentFirstOrDefault);
            var filter = Builders<BsonDocument>.Filter.Eq("name", "12321231");

            //有点类似表达式树啊
            FilterDefinitionBuilder<BsonDocument> filterBuilder = Builders<BsonDocument>.Filter;
            var filter2 = filterBuilder.Gt("i", 50) & filterBuilder.Lte("i", 100);

            //FirstOrDefault和first区别
            var documentFilter = collection.Find(filter).FirstOrDefault();
            Console.WriteLine(documentFilter);
            //first 如果没有数据会报错
            //var documentFilterFirst = collection.Find(filter).First();
            //Console.WriteLine(documentFilterFirst);

            var document = collection.Find(new BsonDocument()).ToList();
            Console.WriteLine(document.ToString());
            foreach (var item in document)
            {
                Console.WriteLine(item);
            }

            var document1 = new BsonDocument
            {
                { "name", "MongoDB" },
                { "info", new BsonDocument
                {
                    { "x", 203 },
                    { "y", 102 }
                }}
            };

            collection.InsertOne(document1);
            //await collection.InsertOneAsync(document1);

            var count = collection.CountDocuments(new BsonDocument());
            Console.WriteLine(count); 


            FilterDefinition<BsonDocument> filterInfo = Builders<BsonDocument>.Filter.Exists("info");
            var sort = Builders<BsonDocument>.Sort.Descending("name");
           
            var document2 = collection.Find(filterInfo).ToList();
            Console.WriteLine(document.ToString());
            foreach (var item in document2)
            {
                Console.WriteLine(item);
            }
        }
    }
}
