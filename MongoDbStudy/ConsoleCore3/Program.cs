﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text.Json;
using System.Text.Unicode;

namespace ConsoleCore3
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new MongoClient(
               "mongodb://192.168.4.43:27017"
           );
            IMongoDatabase database = client.GetDatabase("hello");

            var collection = database.GetCollection<BsonDocument>("test");

            var document = collection.Find(new BsonDocument()).ToList();

            foreach (var item in document)
            {
                Console.WriteLine(item);
            }

            ProjectionDefinition<BsonDocument> projection = "{ name: 1, info:1  }";
            //ProjectionDefinition<BsonDocument> projection = new BsonDocument("name", "MongoDB");
            //ProjectionDefinition<BsonDocument> projection = Builders<Test>.Projection.Include("name").Include("info:y").Exclude("_id");
            //var projection = Builders<BsonDocument>.Projection.Expression(x => new { X = x.Info, Y = x.Name });

            var filter = Builders<BsonDocument>.Filter.Eq("name", "12321231");
            var filter2 = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId("5e170805f74a0afa3dba5da9"));
            var w =filter&filter2;
            var update = Builders<BsonDocument>.Update.Set("name", "asd");
            UpdateResult result = collection.UpdateOne(filter, update);

            var filter1 = Builders<BsonDocument>.Filter.Eq("_id", new ObjectId("5e170805f74a0afa3dba5da9"));
            var a = collection.DeleteOne(filter1);
            Console.WriteLine(a.DeletedCount);

            var documentProjection = collection.Find(new BsonDocument()).Project(projection).ToList();

            foreach (var item in documentProjection)
            {
                Console.WriteLine(item);
            }

            IMongoCollection<Test> context = database.GetCollection<Test>("test");
             var options = new JsonSerializerOptions ();
            options.Encoder = System.Text.Encodings.Web.JavaScriptEncoder.Create (UnicodeRanges.All);

            var filterTest = Builders<Test>.Filter.Eq("_id", new ObjectId("5e195dc5227f74526815fb84"));
            var wwww = context.Find(filterTest).FirstOrDefault();
            Console.WriteLine(JsonSerializer.Serialize(wwww,options));


            var getList =Program.GetList(context,x=>x.Name=="孙悟空",0,0);
           
            foreach (var item in getList)
            {
                Console.WriteLine( JsonSerializer.Serialize(item,options));
            }

           
            var sort = Builders<BsonDocument>.Sort.Descending("_id");
            var documentSort = collection.Find(new BsonDocument()).Sort(sort).ToList();

            foreach (var item in documentSort)
            {
                Console.WriteLine( JsonSerializer.Serialize(item,options));
            }

        }

        private static List<Test> GetList(IMongoCollection<Test> context, Expression<Func<Test, bool>> ex, int skip = 0, int count = 0)
        {
            var result = context.Find(ex)
                .Skip(skip)
                .Limit(count)
                .ToList();
            return result;
        }
    }

}
