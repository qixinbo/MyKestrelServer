﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ConsoleCore3
{
    public class Test
    {
        public ObjectId Id { get; set; }

        [BsonElement("name")]
        public string  Name { get; set; }

        [BsonElement("info")]
        public Info Info { get; set; }
    }

    public class Info
    {
        [BsonElement("x")]
        public int X { get; set; }

        [BsonElement("y")]
        public int Y { get; set; }
    }

}
