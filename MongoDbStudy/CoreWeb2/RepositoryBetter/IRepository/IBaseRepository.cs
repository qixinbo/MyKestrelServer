﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreWeb2.RepositoryBetter.IRepository
{
    public interface IBaseRepository<T> where T: IEntityBase
    {
        /// <summary>
        /// 添加一个数据  todo 增加返回值
        /// </summary>
        /// <param name="addData">添加的数据</param>
        /// <returns></returns>
        Task AddAsync(T addData);

        /// <summary>
        /// 获取所有数据
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<T>> AllAsync();

        /// <summary>
        /// 根据Id获取一条数据
        /// </summary>
        /// <param name="id">数据Guid</param>
        /// <returns></returns>
        Task<T> GetOneAsync(Guid id);

        /// <summary>
        /// 删除一条数据
        /// </summary>
        /// <param name="id">Guid</param>
        /// <returns></returns>
        Task<DeleteResult> DeleteAsync(Guid id);

        /// <summary>
        /// 修改一条完整的数据
        /// </summary>
        /// <param name="addData">修改的数据</param>
        /// <returns></returns>
        Task UpdateOneAsync(T addData);
    }

}
