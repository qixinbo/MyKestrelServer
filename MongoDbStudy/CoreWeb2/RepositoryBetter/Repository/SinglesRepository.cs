﻿using CoreWeb2.RepositoryBetter.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace CoreWeb2.RepositoryBetter.Repository
{
    public class SinglesRepository : BaseRepository<Single>, ISinglesRepository
    {
        public SinglesRepository(MongoClient mongoClient) : base(mongoClient)
        {
            Context = Datebase.ContactSingles;
        }



    }

}
