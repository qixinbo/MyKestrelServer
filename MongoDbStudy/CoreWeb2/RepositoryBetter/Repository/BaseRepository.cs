﻿using CoreWeb2.RepositoryBetter.IRepository;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CoreWeb2.Infrastructure;

namespace CoreWeb2.RepositoryBetter.Repository
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : IEntityBase
    {
        /// <summary>
        /// 文档
        /// </summary>
        protected IMongoCollection<T> Context;

        /// <summary>
        /// 数据库
        /// </summary>
        protected MongoContextService Datebase;

        /// <summary>
        /// 构成函数
        /// </summary>
        /// <param name="mongoClient"></param>
        public BaseRepository(MongoClient mongoClient)
        {
            Datebase = new MongoContextService(mongoClient);
        }
  
        public async Task AddAsync(T data)
        {
            await Context.InsertOneAsync(data);
        }

        public async Task<IEnumerable<T>> AllAsync()
        {
            return await Context.Find(_ => true).ToListAsync();
        }

        public async Task<DeleteResult> DeleteAsync(Guid id)
        {
            return await Context.DeleteOneAsync(filter => filter.Id == id);
        }

        public async Task<T> GetOneAsync(Guid id)
        {
            return await Context.Find(f => f.Id == id).FirstAsync();
        }

        public Task UpdateOneAsync(T addData)
        {
            throw new NotImplementedException();
        }
    }

}
