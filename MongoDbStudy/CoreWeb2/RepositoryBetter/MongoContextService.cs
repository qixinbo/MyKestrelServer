﻿using CoreWeb2.Infrastructure;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace CoreWeb2.RepositoryBetter
{
    public class MongoContextService
    {
        private readonly IMongoDatabase _datebase;

        /// <summary>
        /// 连接数据库
        /// </summary>
        /// <param name="mongoClient">.ner core 设置的链接字符串</param>
        public MongoContextService(MongoClient mongoClient)
        {
            _datebase = mongoClient.GetDatabase(ConfigureManage.GetValue("MongoDB:Database"));
        }

        /// <summary>
        /// 判断文档是否存在 不存在创建
        /// </summary>
        /// <param name="collectionName">文档名称</param>
        /// <param name="setBsonClassMap">首次创建文档字段映射与约束设置</param>
        private void CheckAndCreateCollection(string collectionName, Action setBsonClassMap)
        {
            // 获取数据库中的所有文档
            var collectionList = _datebase.ListCollections().ToList();
            // 保存文档名称
            var collectionNames = new List<string>();
            // 便利获取文档名称
            collectionList.ForEach(b => collectionNames.Add(b["name"].AsString));
            // 判断文档是否存在
            if (!collectionNames.Contains(collectionName))
            {
                // 首次创建文档字段映射与约束设置
                setBsonClassMap();
                // 创建文档
                _datebase.CreateCollection(collectionName);
            }
        }

        /// <summary>
        /// 获取ContactSingles文档
        /// </summary>
        public IMongoCollection<Single> ContactSingles
        {
            get
            {
                CheckAndCreateCollection("ContactSingles", SetBsonClassMapSingles);
                return _datebase.GetCollection<Single>("ContactSingles");
            }
        }

        /// <summary>
        /// ContactSingles文档字段映射
        /// </summary>
        private static void SetBsonClassMapSingles()
        {
            BsonClassMap.RegisterClassMap((BsonClassMap<Single> cm) =>
            {
                cm.AutoMap();
                cm.MapIdMember(x => x.Id).SetIdGenerator(CombGuidGenerator.Instance); // 使用Guid作为文档id
            });
        }
    }

}
