﻿using System;

namespace CoreWeb2.RepositoryBetter
{
    public interface IEntityBase
    {
        Guid Id { get; set; }
    }

}
