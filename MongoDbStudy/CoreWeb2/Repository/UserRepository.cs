﻿using CoreWeb2.Domain;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreWeb2.Repository
{
    public class UserRepository : IRepository<User>
    {
        private readonly IContext<User> _context;
        /// <summary>
        /// IOC
        /// </summary>
        /// <param name="context"></param>
        public UserRepository(IContext<User> context)
        {
            _context = context;
        }
        public async Task<string> Create(User t)
        { 
            await  _context.Entities.InsertOneAsync(t);
            return t.Id;
        }

        public async Task<bool> Delete(string id)
        {
            var deleteResult =  await _context.Entities.DeleteOneAsync(x => x.Id == id);
            return deleteResult.DeletedCount != 0;
        }

        public async Task<List<User>> GetList(int skip = 0, int count = 0)
        {
            var result = await _context.Entities.Find(x => true)
                .Skip(skip)
                .Limit(count)
                .ToListAsync();
            return result;
        }

        public async Task<List<User>> GetListByField(string fieldName, string fieldValue)
        {
            FilterDefinition<User> filter = Builders<User>.Filter.Eq(fieldName, fieldValue);
            var result = await _context.Entities.Find(filter).ToListAsync(); 
            return result;
        }

        public async  Task<User> GetOneById(string id)
        {
            return await _context.Entities.Find(x => x.Id.ToString() == id).FirstOrDefaultAsync();
        }

        public async Task<bool> Update(User t)
        {
            //var filter = Builders<User>.Filter.Eq(x=>x.Id,t.Id);
            var replaceOneResult = await _context.Entities.ReplaceOneAsync(doc => doc.Id == t.Id,t); 
            return replaceOneResult.ModifiedCount != 0;
        }
    }

}
