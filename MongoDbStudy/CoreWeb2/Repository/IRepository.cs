﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreWeb2.Repository
{
    public interface IRepository<T>
    {
        Task<string> Create(T t);
       
        Task<bool> Delete(string id);

        Task<List<T>> GetList(int skip = 0, int count = 0);

        Task<List<T>> GetListByField(string fieldName, string fieldValue);
       
        Task<T> GetOneById(string id);
      
        Task<bool> Update(T t);
        
    }
}
