﻿using CoreWeb2.Domain;
using CoreWeb2.Infrastructure;
using MongoDB.Driver;

namespace CoreWeb2.Repository
{
    public class UserContext : IContext<User>
    {
        private readonly IMongoDatabase _db;
        public UserContext(MongoClient mongoClient)
        {
            _db = mongoClient.GetDatabase(ConfigureManage.GetValue("MongoDB:Database"));
        }
        public IMongoCollection<User> Entities => _db.GetCollection<User>("users");
    }

}
