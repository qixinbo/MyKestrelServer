﻿using CoreWeb2.Domain;
using MongoDB.Driver;

namespace CoreWeb2.Repository
{
    public interface IContext<T> where T:Entity
    {
        IMongoCollection<T> Entities { get;}
    }

}
