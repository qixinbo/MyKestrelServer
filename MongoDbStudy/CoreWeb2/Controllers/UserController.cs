﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreWeb2.Domain;
using CoreWeb2.Repository;
using Microsoft.AspNetCore.Mvc;

namespace CoreWeb2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IRepository<User> _userRepository;
        public UsersController(IRepository<User> userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpGet("List")]
        public async Task<List<User>> List()
        {
            return await _userRepository.GetList();
        }

        [HttpPost("Add")]
        public async Task<string> Add([FromBody] User user)
        {
            var id = await _userRepository.Create(user);
            return id;
        }

        [HttpPost("Delete")]
        public async Task<bool> Delete([FromBody]string id)
        {
            return  await _userRepository.Delete(id);        
        }
    }

}
