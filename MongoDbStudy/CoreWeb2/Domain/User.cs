﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace CoreWeb2.Domain
{
    /// <summary>
    /// User POCO
    /// </summary>
    [BsonIgnoreExtraElements]
    public class User:Entity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("username")]
        public string Username { get; set; }
        [BsonElement("password")]
        public string Password { get; set; }
        [BsonElement("realName")]
        public string RealName { get; set; }
    }

}
