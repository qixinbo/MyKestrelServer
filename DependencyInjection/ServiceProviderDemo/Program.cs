﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace ServiceProviderDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //IServiceCollection services = new ServiceCollection()
            //     .AddSingleton<IFoo, Foo>()
            //     .AddSingleton<IBar>(new Bar())
            //     .AddSingleton<IBaz>(a => new Baz())
            //     .AddSingleton<IGux, Gux>();

            //IServiceProvider serviceProvider = services.BuildServiceProvider();
            //Console.WriteLine("serviceProvider.GetService<IFoo>(): {0}", serviceProvider.GetService<IFoo>());
            //Console.WriteLine("serviceProvider.GetService<IBar>(): {0}", serviceProvider.GetService<IBar>());
            //Console.WriteLine("serviceProvider.GetService<IBaz>(): {0}", serviceProvider.GetService<IBaz>());
            //Console.WriteLine("serviceProvider.GetService<IGux>(): {0}", serviceProvider.GetService<IGux>());

            //IServiceProvider serviceProvider = new ServiceCollection().BuildServiceProvider();
            //Debug.Assert(object.ReferenceEquals(serviceProvider, serviceProvider.GetService<IServiceProvider>()));
            ////Debug.Assert(object.ReferenceEquals(serviceProvider, serviceProvider.GetServices<IServiceProvider>().Single()));
            

          //  IServiceProvider serviceProvider = new ServiceCollection()
          //.AddTransient<IFoo, Foo>()
          //.AddTransient<IBar, Bar>()
          //.AddTransient(typeof(IFoobar<,>), typeof(Foobar<,>))
          //.BuildServiceProvider();
          //  Console.WriteLine("serviceProvider.GetService<IFoobar<IFoo, IBar>>().Foo: {0}", serviceProvider.GetService<IFoobar<IFoo, IBar>>().Foo);
          //  Console.WriteLine("serviceProvider.GetService<IFoobar<IFoo, IBar>>().Bar: {0}", serviceProvider.GetService<IFoobar<IFoo, IBar>>().Bar);

        //     IServiceProvider serviceProvider1 = new ServiceCollection().BuildServiceProvider();
        //IServiceProvider serviceProvider2 = serviceProvider1.GetService<IServiceScopeFactory>().CreateScope().ServiceProvider;
 
        //object root = serviceProvider2.GetType().GetField("_root", BindingFlags.Instance| BindingFlags.NonPublic).GetValue(serviceProvider2);
        //Debug.Assert(object.ReferenceEquals(serviceProvider1, root));   


        }
    }

    public interface IFoobar<T1, T2>
    {
        T1 Foo { get; }
        T2 Bar { get; }
    }
    public class Foobar<T1, T2> : IFoobar<T1, T2>
    {
        public T1 Foo { get; private set; }
        public T2 Bar { get; private set; }
        public Foobar(T1 foo, T2 bar)
        {
            this.Foo = foo;
            this.Bar = bar;
        }
    }


}
