﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace NewCat
{
    public enum Lifetime
    {
        Singlelton,
        Self,
        Transient
    }
    public class ServiceRegistry
    {
        public Type ServiceType { get; }
        public Lifetime Lifetime { get; }
        /// <summary>
        /// core的依赖注入还有实例模式，这里只有工厂模式做展示
        /// </summary>
        public Func<Cat, Type[], object> Factory { get; }
        /// <summary>
        /// 同一个服务类型（ServiceType属性相同）的多个ServiceRegistry组成一个链表
        /// </summary>
        internal ServiceRegistry Next { get; set; }

        public ServiceRegistry(Type serviceType, Lifetime lifetime, Func<Cat, Type[], object> factory)
        {
            ServiceType = serviceType;
            Lifetime = lifetime;
            Factory = factory;
        }

        public IEnumerable<ServiceRegistry> AsEnumerable()
        {
            var list = new List<ServiceRegistry>();
            for (var self = this; self != null; self = self.Next)
            {
                list.Add(self);
            }
            return list;
        }
    }
}
