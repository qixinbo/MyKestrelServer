﻿using System;

namespace NewCat
{
    [AttributeUsage( AttributeTargets.Constructor)]
    internal class InjectionAttribute : Attribute
    {
    }
}
