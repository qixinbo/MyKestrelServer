﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace NewCat
{
    public class Cat : IServiceProvider, IDisposable
    {
        internal Cat _root;
        /// <summary>
        /// 类型注册到容器中时候，用来存储的集合
        /// </summary>
        internal ConcurrentDictionary<Type, ServiceRegistry> _registries;
        /// <summary>
        /// 获取类型实例的集合
        /// </summary>
        private ConcurrentDictionary<ServiceRegistry, object> _services;
        private ConcurrentBag<IDisposable> _disposables;
        /// <summary>
        ///  Dispose()时候设置为true
        /// </summary>
        private volatile bool _disposed;

        /// <summary>
        /// 根容器的构造函数
        /// </summary>
        public Cat()
        {
            _registries = new ConcurrentDictionary<Type, ServiceRegistry>();
            _root = this;
            _services = new ConcurrentDictionary<ServiceRegistry, object>();
            _disposables = new ConcurrentBag<IDisposable>();
        }

        /// <summary>
        /// 创建子容器的构造函数
        /// </summary>
        /// <param name="parent"></param>
        internal Cat(Cat parent)
        {
            _root = parent._root;
            _registries = _root._registries;
            _services = new ConcurrentDictionary<ServiceRegistry, object>();
            _disposables = new ConcurrentBag<IDisposable>();
        }

        public Cat Register(ServiceRegistry registry)
        {
            EnsureNotDisposed();
            //判断是否有同类型的注册
            if (_registries.TryGetValue(registry.ServiceType, out var existing))
            {
                _registries[registry.ServiceType] = registry;
                registry.Next = existing;
            }
            else
            {
                _registries[registry.ServiceType] = registry;
            }
            return this;
        }

        /// <summary>
        /// 如果没注册过类型，会返回null
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public object GetService(Type serviceType)
        {
            EnsureNotDisposed();
            if (serviceType == typeof(Cat))
            {
                return this;
            }
            ServiceRegistry registry;
            if (serviceType.IsGenericType && serviceType.GetGenericTypeDefinition() == typeof(IEnumerable<>))
            {
                var elementType = serviceType.GetGenericArguments()[0];
                if (!_registries.TryGetValue(elementType, out  registry))
                {
                    //没有就直接创建
                    return Array.CreateInstance(elementType, 0);
                }

                var registries = registry.AsEnumerable();
                object[] services = registries.Select(it => GetServiceCore(it, new Type[0])).ToArray();
                //创建一个数组，然后把所有注册到容器中的工厂方法的实例返回
                Array array = Array.CreateInstance(elementType, services.Length);
                services.CopyTo(array, 0);
                return array;
            }

            if (serviceType.IsGenericType && !_registries.ContainsKey(serviceType))
            {
                var definition = serviceType.GetGenericTypeDefinition();
                return _registries.TryGetValue(definition, out registry)
                    //泛型需要获取对应类型参数
                    ? GetServiceCore(registry, serviceType.GetGenericArguments())
                    : null;
            }

            return _registries.TryGetValue(serviceType, out registry)
                    ? GetServiceCore(registry, new Type[0])
                    : null;
        }

        /// <summary>
        /// 实际进行实例创造的方法
        /// </summary>
        /// <param name="registry"></param>
        /// <param name="genericArguments"></param>
        /// <returns></returns>
        private object GetServiceCore(ServiceRegistry registry, Type[] genericArguments)
        {
            var serviceType = registry.ServiceType;
            //方法内部的方法，我还真的很少这么写。
            object GetOrCreate(ConcurrentDictionary<ServiceRegistry, object> services, ConcurrentBag<IDisposable> disposables)
            {
                if (services.TryGetValue(registry, out var service))
                {
                    return service;
                }
                service = registry.Factory(this, genericArguments);
                services[registry] = service;
                var disposable = service as IDisposable;
                if (null != disposable)
                {
                    disposables.Add(disposable);
                }
                return service;
            }

            switch (registry.Lifetime)
            {
                case Lifetime.Singlelton: return GetOrCreate(_root._services, _root._disposables);   
                case Lifetime.Self: return GetOrCreate(_services, _disposables);
                default:
                    //如果是Transient类型，直接创建实例，然后判断是否是IDisposable，如果是加入IDisposable数组
                    //会在容器Disposable时候，遍历Disposable
                    {
                        var service = registry.Factory(this, genericArguments);
                        var disposable = service as IDisposable;
                        if (null != disposable)
                        {
                            _disposables.Add(disposable);
                        }
                        return service;
                    }
            }
        }

        public void Dispose()
        {
            _disposed = true;
            foreach(var disposable in _disposables)
            {
                disposable.Dispose();
            }
            while (!_disposables.IsEmpty)
            {
                _disposables.TryTake(out _);
            }
            _services.Clear();
        }

        private void EnsureNotDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException("Cat");
            }
        }
    }
}
