﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Diagnostics;
using System.Reflection;

namespace ServiceProviderDemoCore2
{
    class Program
    {
        static void Main(string[] args)
        {

            //IServiceProvider root = new ServiceCollection()
            // .AddTransient<IFoo, Foo>()
            // .AddScoped<IBar, Bar>()
            // .AddSingleton<IBaz, Baz>()
            // .BuildServiceProvider();
            //IServiceProvider child1 = root.GetService<IServiceScopeFactory>().CreateScope().ServiceProvider;
            //IServiceProvider child2 = root.GetService<IServiceScopeFactory>().CreateScope().ServiceProvider;

            //Console.WriteLine("ReferenceEquals(root.GetService<IFoo>(), root.GetService<IFoo>() = {0}", ReferenceEquals(root.GetService<IFoo>(), root.GetService<IFoo>()));
            //Console.WriteLine("ReferenceEquals(child1.GetService<IBar>(), child1.GetService<IBar>() = {0}", ReferenceEquals(child1.GetService<IBar>(), child1.GetService<IBar>()));
            //Console.WriteLine("ReferenceEquals(child1.GetService<IBar>(), child2.GetService<IBar>() = {0}", ReferenceEquals(child1.GetService<IBar>(), child2.GetService<IBar>()));
            //Console.WriteLine("ReferenceEquals(child1.GetService<IBaz>(), child2.GetService<IBaz>() = {0}", ReferenceEquals(child1.GetService<IBaz>(), child2.GetService<IBaz>()));

            IServiceProvider serviceProvider = new ServiceCollection()
         .AddTransient<IFoobar, Foobar>()
         .BuildServiceProvider();

            serviceProvider.GetService<IFoobar>().Dispose();
            GC.Collect();

            Console.WriteLine("----------------");
            using (IServiceScope serviceScope = serviceProvider.GetService<IServiceScopeFactory>().CreateScope())
            {
                serviceScope.ServiceProvider.GetService<IFoobar>();
            }
            GC.Collect();

            //Console.Read();

            Console.ReadLine();
        }
    }
    public interface IFoo { }
    public interface IBar { }
    public interface IBaz { }

    public class Foo : IFoo { }
    public class Bar : IBar { }
    public class Baz : IBaz { }
    public interface IFoobar : IDisposable
    { }

    public class Foobar : IFoobar
    {
        ~Foobar()
        {
            Console.WriteLine("Foobar.Finalize()");
        }

        public void Dispose()
        {
            Console.WriteLine("Foobar.Dispose()");
        }
    }

}
