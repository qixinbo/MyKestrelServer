﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace ReBuidServiceProvider
{
    internal class ServiceProvider : IServiceProvider, IDisposable
    {

        public ServiceProvider(ServiceProvider parent)
        {
            this.Root = parent.Root;
            this.ServiceTable = parent.ServiceTable;
        }

        /// <summary>
        /// 根的引用  对于一个独立的ServiceProvider来说，这个根就是它自己
        /// </summary>
        public ServiceProvider Root { get; private set; }
        /// <summary>
        /// ServiceCollection创建的ServiceTable对象
        /// </summary>
        public ServiceTable ServiceTable { get; private set; }

        /// <summary>
        /// ServiceCallSite Buid之后的委托保存在RealizedServices
        /// </summary>
        public ConcurrentDictionary<Type, Func<ServiceProvider, object>> RealizedServices { get; private set; } = new ConcurrentDictionary<Type, Func<ServiceProvider, object>>();

        /// <summary>
        /// 采用Transient模式，对于提供过的服务实例，如果自身类型实现了IDisposble接口,会加入下面的集合中
        /// </summary>
        public IList<IDisposable> TransientDisposableServices { get; private set; } = new List<IDisposable>();

        /// <summary>
        /// Scoped模式提供的服务实例，需要自行对它们进行维护，它们会和对应的Service对象之间的映射关系会保存在下面集合
        /// </summary>
        public ConcurrentDictionary<IService, object> ResolvedServices { get; private set; } = new ConcurrentDictionary<IService, object>();

        public ServiceProvider(IServiceCollection services)
        {
            this.Root = this;
            this.ServiceTable = new ServiceTable(services);
        }

        /// <summary>
        /// 根据类型获取对应的实例
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public object GetService(Type serviceType)
        {
            Func<ServiceProvider, object> serviceAccessor;
            //如果创建过对应的委托
            if (this.RealizedServices.TryGetValue(serviceType, out serviceAccessor))
            {
                return serviceAccessor(this);
            }
            //看看ServiceTable是否有对应的类型
            IServiceCallSite serviceCallSite = GetServiceCallSite(serviceType, new HashSet<Type>());
            if (null != serviceCallSite)
            {
                var providerExpression = Expression.Parameter(typeof(ServiceProvider), "provider");
                //记录这个委托
                RealizedServices[serviceType] = Expression.Lambda<Func<ServiceProvider, object>>(serviceCallSite.Build(providerExpression), providerExpression).Compile();
                return serviceCallSite.Invoke(this);
            }
            //没有就返回空类型
            RealizedServices[serviceType] = _ => null;
            return null;
        }

        public IServiceCallSite GetServiceCallSite(Type serviceType, ISet<Type> callSiteChain)
        {
            try
            {
                if (callSiteChain.Contains(serviceType))
                {
                    throw new InvalidOperationException(string.Format("A circular dependency was detected for the service of type '{0}'", serviceType.FullName));
                }
                callSiteChain.Add(serviceType);
                ServiceEntry serviceEntry;
                if (this.ServiceTable.ServieEntries.TryGetValue(serviceType, out serviceEntry))
                {
                    return serviceEntry.Last.CreateCallSite(this, callSiteChain);
                }

                if (serviceType.IsGenericType && serviceType.GetGenericTypeDefinition() == typeof(IEnumerable<>))
                {
                    Type elementType = serviceType.GetGenericArguments()[0];
                    IServiceCallSite[] serviceCallSites = this.ServiceTable.ServieEntries.TryGetValue(elementType, out serviceEntry)
                        ? serviceEntry.All.Select(it => it.CreateCallSite(this, callSiteChain)).ToArray()
                        : new IServiceCallSite[0];
                    return new EnumerableCallSite(elementType, serviceCallSites);
                }

                return null;
            }
            finally
            {
                callSiteChain.Remove(serviceType);
            }
        }

        public void Dispose()
        {
            Array.ForEach(this.TransientDisposableServices.ToArray(), _ => _.Dispose());
            Array.ForEach(this.ResolvedServices.Values.ToArray(), _ => (_ as IDisposable)?.Dispose());
            this.TransientDisposableServices.Clear();
            this.ResolvedServices.Clear();
        }
        //其他成员
        public object CaptureDisposable(object instance)
        {
            IDisposable disposable = instance as IDisposable;
            if (null != disposable)
            {
                this.TransientDisposableServices.Add(disposable);
            }
            return instance;
        }
    }

}
