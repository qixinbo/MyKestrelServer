﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ReBuidServiceProvider
{
    internal class InstanceCallSite : IServiceCallSite
    {
        public object Instance { get; private set; }
        public InstanceCallSite(object instance)
        {
            this.Instance = instance;
        }
        public Expression Build(Expression provider)
        {
            return Expression.Constant(this.Instance);
        }
        public object Invoke(ServiceProvider provider)
        {
            return Instance;
        }
    }
    internal class FactoryCallSite : IServiceCallSite
    {
        public Func<IServiceProvider, object> Factory { get; private set; }
        public FactoryCallSite(Func<IServiceProvider, object> factory)
        {
            Factory = factory;
        }
        public Expression Build(Expression provider)
        {
            Expression<Func<IServiceProvider, object>> factory = p => Factory(p);
            return Expression.Invoke(factory, provider);
        }
        public object Invoke(ServiceProvider provider)
        {
            return Factory(provider);
        }
    }

    internal class ConstructorCallSite : IServiceCallSite
    {
        public ConstructorInfo ConstructorInfo { get; private set; }
        public IServiceCallSite[] Parameters { get; private set; }

        public ConstructorCallSite(ConstructorInfo constructorInfo, IServiceCallSite[] parameters)
        {
            this.ConstructorInfo = constructorInfo;
            this.Parameters = parameters;
        }

        public Expression Build(Expression provider)
        {
            ParameterInfo[] parameters = ConstructorInfo.GetParameters();
            return Expression.New(ConstructorInfo, Parameters.Select((p, index) => Expression.Convert(p.Build(provider),
                parameters[index].ParameterType)).ToArray());
        }

        public object Invoke(ServiceProvider provider)
        {
            return ConstructorInfo.Invoke(Parameters.Select(p => p.Invoke(provider)).ToArray());
        }
    }


}
