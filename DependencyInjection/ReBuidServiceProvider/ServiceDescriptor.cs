﻿using System;
using System.Collections.Generic;

namespace ReBuidServiceProvider
{
    public class ServiceDescriptor
    {
        public Type ImplementationType { get; }
        public object ImplementationInstance { get; }
        public Func<IServiceProvider, object> ImplementationFactory { get; }
    }
    /// <summary>
    /// Specifies the contract for a collection of service descriptors.
    /// </summary>
    public interface IServiceCollection : IList<ServiceDescriptor>
    {
        IEnumerable<object> GroupBy(Func<object, object> p);
    }

}
