﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ReBuidServiceProvider
{
    /// <summary>
    /// The <see cref="System.IDisposable.Dispose"/> method ends the scope lifetime. Once Dispose
    /// is called, any scoped services that have been resolved from
    /// <see cref="Microsoft.Extensions.DependencyInjection.IServiceScope.ServiceProvider"/> will be
    /// disposed.
    /// </summary>
    public interface IServiceScope : IDisposable
    {
        /// <summary>
        /// The <see cref="System.IServiceProvider"/> used to resolve dependencies from the scope.
        /// </summary>
        IServiceProvider ServiceProvider { get; }
    }

    internal class ServiceScope : IServiceScope
    {
        public IServiceProvider ServiceProvider { get; private set; }

        public ServiceScope(ServiceProvider serviceProvider)
        {
            this.ServiceProvider = serviceProvider;
        }

        public void Dispose()
        {
            (this.ServiceProvider as IDisposable)?.Dispose();
        }
    }
    internal class ServiceScopeFactory : IServiceScopeFactory
    {
        public ServiceProvider ServiceProvider { get; private set; }

        public ServiceScopeFactory(ServiceProvider serviceProvider)
        {
            this.ServiceProvider = serviceProvider;
        }

        public IServiceScope CreateScope()
        {
            return new ServiceScope(this.ServiceProvider);
        }
    }
    public interface IServiceScopeFactory
    {
        /// <summary>
        /// Create an <see cref="Microsoft.Extensions.DependencyInjection.IServiceScope"/> which
        /// contains an <see cref="System.IServiceProvider"/> used to resolve dependencies from a
        /// newly created scope.
        /// </summary>
        /// <returns>
        /// An <see cref="Microsoft.Extensions.DependencyInjection.IServiceScope"/> controlling the
        /// lifetime of the scope. Once this is disposed, any scoped services that have been resolved
        /// from the <see cref="Microsoft.Extensions.DependencyInjection.IServiceScope.ServiceProvider"/>
        /// will also be disposed.
        /// </returns>
        IServiceScope CreateScope();
    }


    internal class ServiceScopeFactoryService : IService, IServiceCallSite
    {
        public ServiceLifetime Lifetime => ServiceLifetime.Scoped;
        public IService Next { get; set; }

        public IServiceCallSite CreateCallSite(ServiceProvider provider, ISet<Type> callSiteChain)
        {
            return this;
        }

        public Expression Build(Expression provider)
        {
            return Expression.New(typeof(ServiceScopeFactory).GetConstructors().Single(), provider);
        }

        public object Invoke(ServiceProvider provider)
        {
            return new ServiceScopeFactory(provider);
        }
    }
       

}
