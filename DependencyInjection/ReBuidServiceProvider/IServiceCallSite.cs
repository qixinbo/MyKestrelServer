﻿using System.Linq.Expressions;

namespace ReBuidServiceProvider
{
    internal interface IServiceCallSite
    {
        object Invoke(ServiceProvider provider);
        Expression Build(Expression provider);
    }

}
