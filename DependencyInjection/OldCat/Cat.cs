﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;

namespace OldCat
{
    public class Cat
    {
        /// <summary>
        /// 线程安全的集合去保存所有的类型
        /// </summary>
        private ConcurrentDictionary<Type, Type> typeMapping = new ConcurrentDictionary<Type, Type>();
        //注册到容器中
        public void Register(Type from, Type to)
        {
            typeMapping[from] = to;
        }
        //这个是核心方法
        public object GetService(Type serviceType)
        {
            #region 构造函数注入

            Type type;
            if (!typeMapping.TryGetValue(serviceType, out type))
            {
                type = serviceType;
            }
            if (type.IsInterface || type.IsAbstract)
            {
                return null;
            }
            //获取构造函数的基本信息。
            ConstructorInfo constructor = this.GetConstructor(type);
            if (null == constructor)
            {
                return null;
            }
            //这里递归的获取了所有需要的实例
            object[] arguments = constructor.GetParameters().Select(p => GetService(p.ParameterType)).ToArray();
            //根据获得的参数调用构造函数 得到所需对象service
            object service = constructor.Invoke(arguments);
            
            #endregion

            #region 属性及方法注入
            
            //如果有属性注入，这里就会调用
            InitializeInjectedProperties(service);
            //如果有方法调用这里就会执行
            InvokeInjectedMethods(service);

            #endregion

            return service;
        }

        /// <summary>
        /// 获取对应类型的构造器
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        protected virtual ConstructorInfo GetConstructor(Type type)
        {
            ConstructorInfo[] constructors = type.GetConstructors();
            //如果我们有标记了InjectionAttribute 那么就用标记了的构造器
            return constructors.FirstOrDefault(c => c.GetCustomAttribute<InjectionAttribute>() != null)
                ?? constructors.FirstOrDefault();
        }

        /// <summary>
        /// 设置属性值
        /// </summary>
        /// <param name="service"></param>
        protected virtual void InitializeInjectedProperties(object service)
        {
            //获取带有InjectionAttribute属性的Properties
            PropertyInfo[] properties = service.GetType().GetProperties()
                .Where(p => p.CanWrite && p.GetCustomAttribute<InjectionAttribute>() != null)
                .ToArray();
            //还是调用GetService方法获取对应类型的值
            Array.ForEach(properties, p => p.SetValue(service, GetService(p.PropertyType)));
        }

        /// <summary>
        /// Invoke方法
        /// </summary>
        /// <param name="service"></param>
        protected virtual void InvokeInjectedMethods(object service)
        {
            MethodInfo[] methods = service.GetType().GetMethods()
                .Where(m => m.GetCustomAttribute<InjectionAttribute>() != null)
                .ToArray();
            Array.ForEach(methods, m =>
            {
                object[] arguments = m.GetParameters().Select(p => GetService(p.ParameterType)).ToArray();
                m.Invoke(service, arguments);
            });
        }
    }
    public static class CatExtensions
    {
        public static T GetService<T>(this Cat cat)
        {
            if (cat == null)
            {
                throw new ArgumentNullException("collection");
            }
            return (T)cat.GetService(typeof(T));
        }

        public static void Register<TService, TImplementation>(this Cat cat)
        {
            if (cat == null)
            {
                throw new ArgumentNullException("collection");
            }
            cat.Register(typeof(TService), typeof(TImplementation));
        }
    }
}
