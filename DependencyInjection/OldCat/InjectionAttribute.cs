﻿using System;

namespace OldCat
{
    [AttributeUsage(AttributeTargets.Constructor |
        AttributeTargets.Property |
        AttributeTargets.Method,
        AllowMultiple = false)]
    public class InjectionAttribute : Attribute { }

}
