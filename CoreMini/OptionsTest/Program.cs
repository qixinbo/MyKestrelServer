﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System.Diagnostics;

namespace OptionsTest
{
    class Program
    {
        //static void Main()
        //{
        //    var random = new Random();
        //    var serviceProvider = new ServiceCollection()
        //        .AddOptions()
        //        .Configure<FoobarOptions>(foobar =>
        //        {
        //            foobar.Foo = random.Next(1, 100);
        //            foobar.Bar = random.Next(1, 100);
        //        })
        //        .BuildServiceProvider();

        //    Action<IServiceProvider> printOptions = provider =>
        //    {
        //        var scopedProvider = provider
        //            .GetRequiredService<IServiceScopeFactory>()
        //            .CreateScope()
        //            .ServiceProvider;

        //        var options = scopedProvider
        //            .GetRequiredService<IOptions<FoobarOptions>>()
        //            .Value;
        //        var optionsSnapshot1 = scopedProvider
        //            .GetRequiredService<IOptionsSnapshot<FoobarOptions>>()
        //            .Value;
        //        var optionsSnapshot2 = scopedProvider
        //            .GetRequiredService<IOptionsSnapshot<FoobarOptions>>()
        //            .Value;
        //        Console.WriteLine($"options:{options}");
        //        Console.WriteLine($"optionsSnapshot1:{optionsSnapshot1}");
        //        Console.WriteLine($"optionsSnapshot2:{optionsSnapshot2}\n");
        //    };

        //    printOptions(serviceProvider);
        //    printOptions(serviceProvider);
        //}
        //static void Main()
        //{
        //    var foobar1 = new FoobarOptions(1, 1);
        //    var foobar2 = new FoobarOptions(2, 2);
        //    var foobar3 = new FoobarOptions(3, 3);

        //    var options = new ServiceCollection()
        //        .AddOptions()
        //        .Configure<FakeOptions>("fakeoptions.json")
        //        .BuildServiceProvider()
        //        .GetRequiredService<IOptions<FakeOptions>>()
        //        .Value;

        //    Debug.Assert(options.Foobar.Equals(foobar1));

        //    Debug.Assert(options.Array[0].Equals(foobar1));
        //    Debug.Assert(options.Array[1].Equals(foobar2));
        //    Debug.Assert(options.Array[2].Equals(foobar3));

        //    Debug.Assert(options.List[0].Equals(foobar1));
        //    Debug.Assert(options.List[1].Equals(foobar2));
        //    Debug.Assert(options.List[2].Equals(foobar3));

        //    Debug.Assert(options.Dictionary["1"].Equals(foobar1));
        //    Debug.Assert(options.Dictionary["2"].Equals(foobar2));
        //    Debug.Assert(options.Dictionary["3"].Equals(foobar3));

        //}
        static void Main()
        {
            var random = new Random();
            var optionsMonitor = new ServiceCollection()
                .AddOptions()
                .Configure<FoobarOptions>(TimeSpan.FromSeconds(1))
                .Configure<FoobarOptions>(foobar =>
                {
                    foobar.Foo = random.Next(10, 100);
                    foobar.Bar = random.Next(10, 100);
                })
                .BuildServiceProvider()
                .GetRequiredService<IOptionsMonitor<FoobarOptions>>();

            optionsMonitor.OnChange(foobar => Console.WriteLine($"[{DateTime.Now}]{foobar}"));
            Console.Read();
        }




    }

}
