﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ConsoleWeb
{
    class Program
    {
        static void Main(string[] args)
        {
            string txtip = "127.0.0.1";
            string txtport = "8080";
            Socket socketWatch = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            IPAddress ip = IPAddress.Parse(txtip);
            IPEndPoint port = new IPEndPoint(ip, Convert.ToInt32(txtport));

            socketWatch.Bind(port);
            socketWatch.Listen(10);

            while (true) {
                Socket socketSend = socketWatch.Accept();
                string strIp = socketSend.RemoteEndPoint.ToString();
                Console.WriteLine("获取到的ip："+strIp);
                byte[] buffer = new byte[2048];

                int count = socketSend.Receive(buffer);

                string str = Encoding.Default.GetString(buffer, 0, count);
                Console.WriteLine("连接内容："+str);
                string ss = "HTTP/1.0 200 OK\nContent-Type:text/html\n\n Welcome!Now Time:{0}";

                string ss1 = "Web Server System";

                DateTime dt = DateTime.Now;

                string nn = string.Format(ss, dt.ToString());

                socketSend.Send(Encoding.ASCII.GetBytes(nn+Environment.NewLine));

                socketSend.Send(Encoding.ASCII.GetBytes(ss1));

                socketSend.Close();

            }
        }
    }
}
