﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace DefaultCore2
{
    class Program
    {
        static void Main(string[] args)
        {
            WebHost.CreateDefaultBuilder(args)
                .Configure(app => app.Run(context => context.Response.WriteAsync("Hello World!")))
                .Build().Run();
        }
    }
}
