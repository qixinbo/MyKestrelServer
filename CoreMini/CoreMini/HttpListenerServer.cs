﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CoreMini
{
    public class HttpListenerServer : IServer
    {
        /// <summary>
        /// C:\project\SourceCode\corefx-2.2.1\src\System.Net.HttpListener\src\System\Net\HttpListener
        /// </summary>
        private readonly HttpListener _httpListener;
        private readonly string[] _urls;
        public HttpListenerServer(params string[] urls)
        {
            _httpListener = new HttpListener();
            _urls = urls.Any() ? urls : new string[] { "http://localhost:5000/" };
        }

        public async Task StartAsync(RequestDelegate handler)
        {
            //添加对应url的监听
            Array.ForEach(_urls, url => _httpListener.Prefixes.Add(url));
            _httpListener.Start();
            Console.WriteLine("Server started and is listening on: {0}", string.Join(';', _urls));
            while (true)
            {
                //异步等待一个到达的请求
                HttpListenerContext listenerContext = await _httpListener.GetContextAsync();
                HttpListenerFeature feature = new HttpListenerFeature(listenerContext);
                //IFeatureCollection 和HttpListenerFeature的作用就是HttpListenerContext和HttpContext之间的适配，把HttpContext和HttpListenerContext之间的转换问题通过接口方式解决了。
                IFeatureCollection features = new FeatureCollection()
                    .Set<IHttpRequestFeature>(feature)
                    .Set<IHttpResponseFeature>(feature);
                var httpContext = new HttpContext(features);
                //此处的handler就是我们在程序开始的部分注册的FooMiddleware BarMiddleware
                //如果按照这样去处理的话，我们的系统是单线程的吧？
                await handler(httpContext);
                listenerContext.Response.Close();
            }
        }
    }
    public static partial class Extensions
    {
        public static IWebHostBuilder UseHttpListener(this IWebHostBuilder builder, params string[] urls)
            => builder.UseServer(new HttpListenerServer(urls));
    }
}
