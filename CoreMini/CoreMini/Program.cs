﻿using System;
using System.Threading.Tasks;

namespace CoreMini
{
    class Program
    {
        public static async Task Main()
        {
            await new WebHostBuilder()
                //使用HttpListenerServer的类处理请求，内部HttpListener监听请求，然后
                .UseHttpListener()
                //把配置好中间件的IApplicationBuilder加到List<Action<IApplicationBuilder>> _configures的集合
                .Configure(app => app
                    //1 IApplicationBuilder类List<Func<RequestDelegate, RequestDelegate>> _middlewares     _middlewares.Add(middleware);把中间件都加入到这个集合中
                    //2 实际的core传入的是一个类型，类里面有Task Invoke(HttpContext context)方法，内部也是RequestDelegate实现
                    .Use(FooMiddleware)
                    .Use(BarMiddleware)
                    .Use(BazMiddleware))
                //Build执行new WebHost(_server, builder.Build())  
                // _server就是我们 HttpListenerServer那一步生成的监听server
                //这里builder.Build() 构建中间件的管道 这里会默认404，然后中间件遍历形成管道
                .Build()
                //_server.StartAsync(_handler)  这里_handler就是我们上一步构造的管道
                .StartAsync();
        }

        public static RequestDelegate FooMiddleware(RequestDelegate next)
            => async context =>
            {
                await context.Response.WriteAsync("Foo=>");
                await next(context);
            };

        public static RequestDelegate BarMiddleware(RequestDelegate next)
            => async context =>
            {
                await context.Response.WriteAsync("Bar=>");
                await next(context);
            };

        public static RequestDelegate BazMiddleware(RequestDelegate next)
            => context => context.Response.WriteAsync("Baz");
    }
}
