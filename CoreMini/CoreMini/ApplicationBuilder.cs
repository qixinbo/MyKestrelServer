﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreMini
{
    public interface  IApplicationBuilder
    {
        IApplicationBuilder Use(Func<RequestDelegate, RequestDelegate> middleware);
        RequestDelegate Build();
    }
    public class ApplicationBuilder : IApplicationBuilder
    {
        private readonly List<Func<RequestDelegate, RequestDelegate>> _middlewares = new List<Func<RequestDelegate, RequestDelegate>>();
        public RequestDelegate Build()
        {
            _middlewares.Reverse();
            return httpContext =>
            {
                //先全部返回404，然后看是否有对应的中间件去处理这个请求
                RequestDelegate next = _ => { 
                    _.Response.StatusCode = 404; 
                    return Task.CompletedTask; };
                foreach (var middleware in _middlewares)
                {
                    //func可以这样嵌套的执行
                    next = middleware(next);
                }
                return next(httpContext);
            };
        }

        public IApplicationBuilder Use(Func<RequestDelegate, RequestDelegate> middleware)
        {
            _middlewares.Add(middleware);
            return this;
        }
    }

}
