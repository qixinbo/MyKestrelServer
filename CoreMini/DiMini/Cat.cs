﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace DiMini
{
    public class ServiceRegistry
    {
        public Type ServiceType { get; }
        public Lifetime Lifetime { get; }
        public Func<Cat, Type[], object> Factory { get; }
        internal ServiceRegistry Next { get; set; }
        //进行注册的中间类
        public ServiceRegistry(Type serviceType, Lifetime lifetime, Func<Cat, Type[], object> factory)
        {
            ServiceType = serviceType;
            Lifetime = lifetime;
            Factory = factory;
        }
        //这个方法用来遍历要
        public IEnumerable<ServiceRegistry> AsEnumerable()
        {
            var list = new List<ServiceRegistry>();
            for (var self = this; self != null; self = self.Next)
            {
                list.Add(self);
            }
            return list;
        }
    }
    public class Cat : IServiceProvider, IDisposable
    {
        internal Cat Root;
        internal ConcurrentDictionary<Type, ServiceRegistry> Registries;
        private ConcurrentDictionary<ServiceRegistry, object> _services;
        private ConcurrentBag<IDisposable> _disposables;
        private volatile bool _disposed;

        public Cat()
        {
            Registries = new ConcurrentDictionary<Type, ServiceRegistry>();
            Root = this;
            _services = new ConcurrentDictionary<ServiceRegistry, object>();
            _disposables = new ConcurrentBag<IDisposable>();
        }
        internal Cat(Cat parent)
        {
            Root = parent.Root;
            Registries = Root.Registries;
            _services = new ConcurrentDictionary<ServiceRegistry, object>();
            _disposables = new ConcurrentBag<IDisposable>();
        }

        public Cat Register(ServiceRegistry registry)
        {
            EnsureNotDisposed();
            if (Registries.TryGetValue(registry.ServiceType, out var existing))
            {
                Registries[registry.ServiceType] = registry;
                registry.Next = existing;
            }
            else
            {
                Registries[registry.ServiceType] = registry;
            }
            return this;
        }

        public object GetService(Type serviceType)
        {
            EnsureNotDisposed();
            if (serviceType == typeof(Cat))
            {
                return this;
            }
            ServiceRegistry registry;
            if (serviceType.IsGenericType && serviceType.GetGenericTypeDefinition() == typeof(IEnumerable<>))//是不是集合类型，集合类型就返回所有注册的实例
            {
                var elementType = serviceType.GetGenericArguments()[0];
                if (!Registries.TryGetValue(elementType, out registry))
                {
                    return Array.CreateInstance(elementType, 0);
                }

                var registries = registry.AsEnumerable();//获取所有ServiceRegistry
                var services = registries.Select(it => GetServiceCore(it, new Type[0])).ToArray();
                Array array = Array.CreateInstance(elementType, services.Length);
                services.CopyTo(array, 0);
                return array;
            }

            if (serviceType.IsGenericType && !Registries.ContainsKey(serviceType))//是否已经注册这个类型
            {
                var definition = serviceType.GetGenericTypeDefinition();
                return Registries.TryGetValue(definition, out registry)
                    ? GetServiceCore(registry, serviceType.GetGenericArguments())
                    : null;
            }

            return Registries.TryGetValue(serviceType, out registry)
                    ? GetServiceCore(registry, new Type[0])
                    : null;
        }
        //获取依赖注入的核心方法
        private object GetServiceCore(ServiceRegistry registry, Type[] genericArguments)
        {
            var serviceType = registry.ServiceType;
            object GetOrCreate(ConcurrentDictionary<ServiceRegistry, object> services, ConcurrentBag<IDisposable> disposables)
            {
                if (services.TryGetValue(registry, out var service))
                {
                    return service;
                }
                service = registry.Factory(this, genericArguments);
                services[registry] = service;
                var disposable = service as IDisposable;
                if (null != disposable)
                {
                    disposables.Add(disposable);
                }
                return service;
            }

            switch (registry.Lifetime)
            {
                case Lifetime.Singlelton: return GetOrCreate(Root._services, Root._disposables);
                case Lifetime.Self: return GetOrCreate(_services, _disposables);
                default:
                {
                    var service = registry.Factory(this, genericArguments);
                    var disposable = service as IDisposable;
                    if (null != disposable)
                    {
                        _disposables.Add(disposable);
                    }
                    return service;
                }
            }
        }

        public void Dispose()
        {
            _disposed = true;
            foreach (var disposable in _disposables)
            {
                disposable.Dispose();
            }
            while (!_disposables.IsEmpty)
            {
                _disposables.TryTake(out _);
            }
            _services.Clear();
        }

        private void EnsureNotDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException("Cat");
            }
        }
    }
}
