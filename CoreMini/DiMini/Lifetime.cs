﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DiMini
{
    public enum Lifetime
    {
        Singlelton,
        Self,//某个容器区间内的单例 相当于core的Scoped
        Transient
    }
}
