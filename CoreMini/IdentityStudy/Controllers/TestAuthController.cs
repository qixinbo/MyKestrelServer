﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace IdentityStudy.Controllers
{
    public class TestAuthController: Controller
    {
        public IActionResult Index()
        {
            return Json(new
            {
                User.Identity.IsAuthenticated,
                User.Identity.AuthenticationType,
                Claims = User.Claims.Select(c => new {c.Type, c.Value})
            });
        }
    }
}
