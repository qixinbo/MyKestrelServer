﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace IdentityStudy.Controllers
{
    [Produces("application/json")]
    [Route("api/demo")]
    public class DemoController : Controller
    {
        [Authorize(Policy ="role-policy")]
        //[Authorize]
        [HttpGet]
        public object Get()
        {
            return new
            {
                User.Identity.Name,
                User.Identity.IsAuthenticated,
            };
        }
    }
}
