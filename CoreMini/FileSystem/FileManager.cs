﻿using Microsoft.Extensions.FileProviders;
using System;
using System.Text;
using System.Threading.Tasks;

namespace FileSystem
{
    public class FileManager : IFileManager
    {
        private readonly IFileProvider _fileProvider;  
        public FileManager(IFileProvider fileProvider) 
            => _fileProvider = fileProvider;
        public void ShowStructure(Action<int, string> render)
        {
            int layer = -1;
            Render("", ref layer, render);
        }
        private void Render(string subPath, ref int indent, 
            Action<int, string> render)
        {
            indent++;
            foreach (var fileInfo in _fileProvider
                .GetDirectoryContents(subPath))
            {
                render(indent, fileInfo.Name);
                if (fileInfo.IsDirectory)
                {
                    Render($@"{subPath}\{fileInfo.Name}".TrimStart('\\'), 
                        ref indent, render);
                }
            }
            indent--;
        }
        public async Task<string> ReadAllTextAsync(string path)
        {
            byte[] buffer;
            using (var stream = _fileProvider
                .GetFileInfo(path).CreateReadStream())
            {
                buffer = new byte[stream.Length];
                await stream.ReadAsync(buffer, 0, buffer.Length);
            }
            return Encoding.ASCII.GetString(buffer);
        }

    }

}
