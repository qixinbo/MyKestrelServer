﻿using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Primitives;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace FileSystem
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //    Action<int, string> print = (layer, name)
        //        => Console.WriteLine($"{new string(' ', layer * 4)}{name}");
        //    new ServiceCollection()
        //        .AddSingleton<IFileProvider>(new PhysicalFileProvider(@"E:\360Downloads\2.13"))
        //        .AddSingleton<IFileManager, FileManager>()
        //        .BuildServiceProvider()
        //        .GetService<IFileManager>()
        //        .ShowStructure(print);

        //    string content = new ServiceCollection()
        //        .AddSingleton<IFileProvider>(
        //            new PhysicalFileProvider(@"d:\test"))
        //        .AddSingleton<IFileManager, FileManager>()
        //        .BuildServiceProvider()
        //        .GetService<IFileManager>()
        //        .ReadAllTextAsync("data.txt").Result;
        //    Console.WriteLine(content);//这里只能识别英文
        //    Debug.Assert(content == File.ReadAllText(@"d:\test\data.txt"));
        //}

        static async Task Main()
        {
            var fileProvider = new PhysicalFileProvider(@"c:\test");
            string original = null;
            Action callback = async () =>
            {
                var stream = fileProvider.GetFileInfo("data.txt")
                    .CreateReadStream();
                {
                    var buffer = new byte[stream.Length];
                    await stream.ReadAsync(buffer, 0, buffer.Length);
                    string current = Encoding.ASCII.GetString(buffer);
                    if (current != original)
                    {
                        Console.WriteLine(original = current);
                    }
                }
            };
            ChangeToken.OnChange(
                () => fileProvider.Watch("data.txt"), callback);

            while (true)
            {
                File.WriteAllText(@"c:\test\data.txt", 
                    DateTime.Now.ToString());
                await Task.Delay(5000);
            }
        }

    }
}
