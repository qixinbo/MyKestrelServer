﻿using System;
using System.Threading.Tasks;

namespace FileSystem
{
    public interface IFileManager
    {
        void ShowStructure(Action<int, string> render);
        Task<string> ReadAllTextAsync(string path);
    }

}
