﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Microsoft.Extensions.DiagnosticAdapter;

namespace DiagnosticStudy
{
    public sealed class DiagnosticCollector
    {
        [DiagnosticName("ReceiveRequest")]
        public void OnReceiveRequest(HttpRequestMessage request, long timestamp) 
            => Console.WriteLine(
                $"Receive request. Url: {request.RequestUri}; Timstamp:{timestamp}");

        [DiagnosticName("SendReply")]
        public void OnSendReply(HttpResponseMessage response, TimeSpan elaped) 
            => Console.WriteLine(
                $"Send reply. Status code: {response.StatusCode}; Elaped: {elaped}");
    }

}
