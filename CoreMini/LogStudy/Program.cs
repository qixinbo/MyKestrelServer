﻿using System;
using System.Diagnostics;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace LogStudy
{
    class Program
    {
        //static void Main()
        //{
        //    var logger = new LoggerFactory()
        //        .AddConsole()
        //        .AddDebug()
        //        .CreateLogger("App.Program");

        //    var levels = (LogLevel[])Enum.GetValues(typeof(LogLevel));
        //    levels = levels.Where(it => it != LogLevel.None).ToArray();
        //    var eventId = 1;
        //    Array.ForEach(levels, level => logger.Log(level, eventId++, 
        //        "This is a/an {0} log message.", level));
        //    Console.Read();
        //}
        //static void Main()
        //{
            
        //    var listener = new FoobarEventListener();
        //    listener.EventSourceCreated += (sender, args) =>
        //    {
        //        if (args.EventSource.Name == "Microsoft-Extensions-Logging")
        //        {
        //            listener.EnableEvents(args.EventSource, EventLevel.LogAlways);
        //        }
        //    };
        //    listener.EventWritten += (sender, args) =>
        //    {
        //        if (args.EventName == "FormattedMessage")
        //        {
        //            var payload = args.Payload;
        //            var payloadNames = args.PayloadNames;
        //            var indexOfLevel = payloadNames.IndexOf("Level");
        //            var indexOfCategory = args.PayloadNames.IndexOf("LoggerName");
        //            var indexOfEventId = args.PayloadNames.IndexOf("EventId");
        //            var indexOfMessage = args.PayloadNames.IndexOf("FormattedMessage");
        //            Console.WriteLine($"{payload[indexOfLevel],-11}:{payload[indexOfCategory]}[{payload[indexOfEventId]}]");
        //            Console.WriteLine($"{"",-13}{payload[indexOfMessage]}");
        //        }
        //    };

        //    var logger = new LoggerFactory()
        //        .AddTraceSource(new SourceSwitch("default","All"),new DefaultTraceListener { LogFileName = "trace.log"})
        //        .AddEventSourceLogger()
        //        .CreateLogger<Program>();

        //    var levels = (LogLevel[])Enum.GetValues(typeof(LogLevel));
        //    levels = levels.Where(it => it != LogLevel.None).ToArray();
        //    var eventId = 1;
        //    Array.ForEach(levels, level => logger.Log(level, eventId++,  "This is a/an {level} log message.", level));
        //    Console.Read();
        //}

        private static Random _random;
        private static string _template;
        private static ILogger _logger;
        private static Action<ILogger, int, long, double, TimeSpan, Exception> _methodInvoked;

        static void Main()
        {
            _random = new Random();
            _template = "Method Foobar is invoked." +
                        "\n\t\tArguments: foo={foo}, bar={bar}" +
                        "\n\t\tReturn value: {returnValue}" +
                        "\n\t\tTime:{time}";
            _methodInvoked = LoggerMessage.Define
                <int, long, double, TimeSpan>(LogLevel.Trace, 3721, _template);
            _logger = new ServiceCollection()
                .AddLogging(builder => builder
                    .SetMinimumLevel(LogLevel.Trace)
                    .AddConsole())
                .BuildServiceProvider()
                .GetRequiredService<ILogger<Program>>();
            Foobar(_random.Next(), _random.Next());
            Foobar(_random.Next(), _random.Next());
            Console.Read();
        }


        static double Foobar(int foo, long bar)
        {
            var stopwatch = Stopwatch.StartNew();
            Task.Delay(_random.Next(100, 900)).Wait();
            var result = _random.Next();
            _methodInvoked(_logger, foo, bar, result, stopwatch.Elapsed, null);
            return result;
        }

        public class FoobarEventListener : EventListener { }



    }
}
