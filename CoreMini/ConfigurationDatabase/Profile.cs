﻿using System;

namespace ConfigurationDatabase
{
    public class Profile: IEquatable<Profile>
    {
        public Gender Gender { get; set; }
        public int Age { get; set; }
        public ContactInfo ContactInfo { get; set; }

        public Profile() {}
        public Profile(
            Gender gender, 
            int age, 
            string emailAddress, 
            string phoneNo)
        {
            Gender = gender;
            Age = age;
            ContactInfo = new ContactInfo
            {
                EmailAddress = emailAddress,
                PhoneNo = phoneNo
            };
        }       
        public bool Equals(Profile other)
        {
            return other == null
                ? false
                : this.Gender     == other.Gender &&
                  this.Age     == other.Age &&
                  this.ContactInfo.Equals(other.ContactInfo);
        }
    }

    public class ContactInfo
        : IEquatable<ContactInfo>
    {
        public string EmailAddress { get; set; }
        public string PhoneNo { get; set; }  
        public bool Equals(ContactInfo other)
        {
            return other == null
                ? false
                : this.EmailAddress     == other.EmailAddress &&
                  this.PhoneNo == other.PhoneNo;
        }
    }
    public enum Gender
    {
        Male,
        Female
    }
}
