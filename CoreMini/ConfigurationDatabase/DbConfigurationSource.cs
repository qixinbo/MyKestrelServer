﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace ConfigurationDatabase
{
    public class DbConfigurationSource : IConfigurationSource
    {
        // optionsBuilder => optionsBuilder.UseMySql(connectionString)这个委托，原来写在扩展中
        private Action<DbContextOptionsBuilder> _setup;
        private IDictionary<string, string> _initialSettings;

        public DbConfigurationSource(
            Action<DbContextOptionsBuilder> setup, 
            IDictionary<string, string> initialSettings = null)
        {
            _setup = setup;
            _initialSettings = initialSettings;
        }
        public IConfigurationProvider Build(
            IConfigurationBuilder builder)
            => new DbConfigurationProvider(_setup, _initialSettings);
    }

}
