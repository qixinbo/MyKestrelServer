﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Diagnostics;

namespace ConfigurationDatabase
{
    class Program
    {
        static void Main(string[] args)
        {
            var initialSettings = new Dictionary<string, string>
            {
                ["Gender"] = "Male",
                ["Age"] = "18",
                ["ContactInfo:EmailAddress"] = "foobar@outlook.com",
                ["ContactInfo:PhoneNo"] = "123456789"
            };

            var config = new ConfigurationBuilder()
                .AddJsonFile("connectionString.json")
                .AddDatabase("DefaultDb", initialSettings)
                .Build();

            var profile = new ServiceCollection()
                .AddOptions()
                .Configure<Profile>(config)
                .BuildServiceProvider()
                .GetService<IOptions<Profile>>()
                .Value;

            Debug.Assert(profile.Gender == Gender.Male);
            Debug.Assert(profile.Age == 18);
            Debug.Assert(profile.ContactInfo.EmailAddress == "foobar@outlook.com");
            Debug.Assert(profile.ContactInfo.PhoneNo == "123456789");

        }
    }
}
