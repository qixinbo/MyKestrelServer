﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace OptionsStudy
{
    class Program
    {
        //static void Main()
        //{
        //    var configuration = new ConfigurationBuilder()
        //        .SetBasePath(Directory.GetCurrentDirectory())
        //        .AddJsonFile("profile.json")
        //        .Build();
        //    var profile = new ServiceCollection()
        //        .AddOptions()
        //        .Configure<Profile>(configuration)
        //        .BuildServiceProvider()
        //        .GetRequiredService<IOptions<Profile>>()
        //        .Value;
        //    Console.WriteLine($"Gender: {profile.Gender}");
        //    Console.WriteLine($"Age: {profile.Age}");
        //    Console.WriteLine($"Email Address: {profile.ContactInfo.EmailAddress}");
        //    Console.WriteLine($"Phone No: {profile.ContactInfo.PhoneNo}");
        //}
        //static void Main()
        //{
        //    var configuration = new ConfigurationBuilder()
        //        .SetBasePath(Directory.GetCurrentDirectory())
        //        .AddJsonFile("profile.json")
        //        .Build();

        //    var serviceProvider = new ServiceCollection()
        //        .AddOptions()
        //        .Configure<Profile>("foo", configuration.GetSection("foo"))
        //        .Configure<Profile>("bar", configuration.GetSection("bar"))
        //        .BuildServiceProvider();

        //    Action<Profile> print = profile =>
        //    {
        //        Console.WriteLine($"Gender: {profile.Gender}");
        //        Console.WriteLine($"Age: {profile.Age}");
        //        Console.WriteLine($"Email Address: {profile.ContactInfo.EmailAddress}");
        //        Console.WriteLine($"Phone No: {profile.ContactInfo.PhoneNo}\n");
        //    };
        //    var optionsAccessor = serviceProvider.GetRequiredService<IOptionsSnapshot<Profile>>();
        //    print(optionsAccessor.Get("foo"));
        //    print(optionsAccessor.Get("bar"));
        //}
        //static void Main()
        //{
        //    var configuration = new ConfigurationBuilder()
        //        .SetBasePath(Directory.GetCurrentDirectory())
        //        .AddJsonFile(path: "profile.json", optional: false,reloadOnChange: true)
        //        .Build();

        //    new ServiceCollection()
        //        .AddOptions()
        //        .Configure<Profile>(configuration)
        //        .BuildServiceProvider()
        //        .GetRequiredService<IOptionsMonitor<Profile>>()
        //        .OnChange(profile =>
        //        {
        //            Console.WriteLine($"Gender: {profile.Gender}");
        //            Console.WriteLine($"Age: {profile.Age}");
        //            Console.WriteLine($"Email Address: {profile.ContactInfo.EmailAddress}");
        //            Console.WriteLine($"Phone No: {profile.ContactInfo.PhoneNo}\n");
        //        });
        //    Console.Read();
        //}
        static void Main()
        {
            var profile = new ServiceCollection()
                .AddOptions()
                .Configure<Profile>(it =>
                {
                    it.Gender         = Gender.Male;
                    it.Age         = 18;
                    it.ContactInfo     = new ContactInfo
                    {
                        PhoneNo         = "123456789",
                        EmailAddress     = "foobar@outlook.com"
                    };
                })
                .BuildServiceProvider()
                .GetRequiredService<IOptions<Profile>>()
                .Value;

            Console.WriteLine($"Gender: {profile.Gender}");
            Console.WriteLine($"Age: {profile.Age}");
            Console.WriteLine($"Email Address: {profile.ContactInfo.EmailAddress}");
            Console.WriteLine($"Phone No: {profile.ContactInfo.PhoneNo}\n");
        }

    }

}
