﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DiagnosticAdapter;

namespace DebuggerTest
{
    public class DatabaseSourceCollector
    {
        [DiagnosticName("CommandExecution")]
        public void OnCommandExecute(
            CommandType commandType, string commandText)
        {
            Console.WriteLine($"Event Name: CommandExecution");
            Console.WriteLine($"CommandType: {commandType}");
            Console.WriteLine($"CommandText: {commandText}");
        }
    }

}
