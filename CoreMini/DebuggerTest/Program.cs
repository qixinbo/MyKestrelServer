﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Tracing;
using System.IO;

namespace DebuggerTest
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //    var source = new TraceSource("Foobar", SourceLevels.Warning);
        //    source.Listeners.Add(new ConsoleTraceListener());
        //    var eventTypes = (TraceEventType[])Enum
        //        .GetValues(typeof(TraceEventType));
        //    var eventId = 1;
        //    Array.ForEach(eventTypes, it => source.TraceEvent(it, eventId++, 
        //        $"This is a {it} message."));
        //    Console.Read();

        //}

        //static void Main()
        //{
        //    var listener = new DatabaseSourceListener();
        //    DatabaseSource.Instance.OnCommandExecute(
        //        CommandType.Text, "SELECT * FROM T_USER");

        //}
        //=> DatabaseSource.Instance.OnCommandExecute(
        //    CommandType.Text, "SELECT * FROM T_USER");

        //static void Main()
        //{
        //    DiagnosticListener.AllListeners.Subscribe(
        //        new Observer<DiagnosticListener>(listener =>
        //        {
        //            if (listener.Name == "Artech-Data-SqlClient")
        //            {
        //                listener.Subscribe(
        //                    new Observer<KeyValuePair<string, object>>(eventData =>
        //                    {
        //                        Console.WriteLine($"Event Name: {eventData.Key}");
        //                        dynamic payload = eventData.Value;
        //                        Console.WriteLine(
        //                            $"CommandType: {payload.CommandType}");
        //                        Console.WriteLine(
        //                            $"CommandText: {payload.CommandText}");
        //                    }));
        //            }
        //        }));

        //    var source = new DiagnosticListener("Artech-Data-SqlClient");
        //    if (source.IsEnabled("CommandExecution"))
        //    {
        //        source.Write("CommandExecution",
        //            new
        //            {
        //                CommandType = CommandType.Text,
        //                CommandText = "SELECT * FROM T_USER"
        //            });
        //    }
        //}
        //static void Main()
        //{
        //    DiagnosticListener.AllListeners.Subscribe(
        //        new Observer<DiagnosticListener>(listener=> {
        //            if (listener.Name == "Artech-Data-SqlClient")
        //            {
        //                listener.SubscribeWithAdapter(new DatabaseSourceCollector());
        //            }
        //        }));

        //    var source = new DiagnosticListener("Artech-Data-SqlClient");
        //    source.Write("CommandExecution", 
        //        new 
        //        { 
        //            CommandType = CommandType.Text, 
        //            CommandText = "SELECT * FROM T_USER" 
        //        });
        //    Console.Read();
        //}
        //static void Main()
        //{
        //    var source = new TraceSource("Foobar", SourceLevels.All);
        //    source.Listeners.Clear();
        //    source.Listeners.Add(new DefaultTraceListener 
        //    { 
        //        LogFileName = "trace.log" 
        //    });
        //    var eventTypes = (TraceEventType[])Enum
        //        .GetValues(typeof(TraceEventType));
        //    var eventId = 1;
        //    Array.ForEach(eventTypes, it => 
        //        source.TraceEvent(it, eventId++, $"This is a {it} message."));
        //}
        static void Main()
        {
            var fileName = "trace.csv";
            File.AppendAllText(fileName, $"SourceName,EventType,EventId,Message,N/A,ProcessId ,LogicalOperationStack,ThreadId,DateTime,Timestamp,{Environment.NewLine}");

            using (var fileStream = new FileStream(fileName, FileMode.Append))
            {
                TraceOptions options = TraceOptions.Callstack | TraceOptions.DateTime | 
                                       TraceOptions.LogicalOperationStack | TraceOptions.ProcessId | 
                                       TraceOptions.ThreadId | TraceOptions.Timestamp;
                var listener = new DelimitedListTraceListener(fileStream) 
                    { TraceOutputOptions = options, Delimiter = "," };
                var source = new TraceSource("Foobar", SourceLevels.All);
                source.Listeners.Add(listener);
                var eventTypes = (TraceEventType[])Enum.GetValues(typeof(TraceEventType));                  
                for (int index = 0; index < eventTypes.Length; index++)
                {
                    var enventType = eventTypes[index];
                    var eventId = index + 1;
                    Trace.CorrelationManager.StartLogicalOperation($"Op{eventId}");
                    source.TraceEvent(enventType, eventId, $"This is a {enventType} message.");
                }
                source.Flush();
            }
        }
    }
    [EventSource(Name = "Artech-Data-SqlClient")]
    public sealed class DatabaseSource : EventSource
    {
        public static DatabaseSource Instance
            = new DatabaseSource();
        private DatabaseSource() { }
        [Event(1)]
        public void OnCommandExecute(
            CommandType commandType,
            string commandText)
            => WriteEvent(1, commandType, commandText);
    }

    public enum CommandType
    {
        Text,
        StoredProcedure
    }
}
