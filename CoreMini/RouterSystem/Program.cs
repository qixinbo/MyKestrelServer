﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;

namespace RouterSystem
{
    class Program
    {
        private static Dictionary<string, string> _cities = new Dictionary<string, string>
        {
            ["010"]  = "北京",
            ["028"]  = "成都",
            ["0512"] = "苏州"
        };
 
        public static async Task WeatherForecast(HttpContext context)
        {
            object rawCity;
            object rawDays;
            var values = context.GetRouteData().Values;
            string city = values.TryGetValue("city", out rawCity) ? rawCity.ToString() : "010";
            int days = values.TryGetValue("days", out rawDays) ? int.Parse(rawDays.ToString()) : 4;     
            
            WeatherReport report = new WeatherReport(city, days);
 
            context.Response.ContentType = "text/html";
            await context.Response.WriteAsync("<html><head><title>Weather</title></head><body>");
            await context.Response.WriteAsync($"<h3>{city}</h3>");
            foreach (var it in report.WeatherInfos)
            {
                await context.Response.WriteAsync($"{it.Key.ToString("yyyy-MM-dd")}:");
                await context.Response.WriteAsync($"{it.Value.Condition}({it.Value.LowTemperature}℃ ~ {it.Value.HighTemperature}℃)<br/><br/>");
            }
            
            await context.Response.WriteAsync("</body></html>");
        }

        static void Main(string[] args)
        {
            //string template = "weather/{city}/{days}";
            string template = @"weather/{city:regex(^0\d{{2,3}}$)}/{days:int:range(1,4)}";
            //表示可空
            //string template = "weather/{city?}/{days?}";
            //直接赋值默认参数
            //string template = "weather/{city=010}/{days=4}";
            new WebHostBuilder()
                .UseKestrel()
                .ConfigureServices(svcs => svcs.AddRouting())
                .Configure(app => app.UseRouter(builder => builder.MapGet(template, WeatherForecast)))
                .Build()
                .Run();
        }
    }
}
