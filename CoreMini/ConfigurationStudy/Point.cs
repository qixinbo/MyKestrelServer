﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace ConfigurationStudy
{
    [TypeConverter(typeof(PointTypeConverter))]
    public class Point
    {
        public double X { get; set; }
        public double Y { get; set; }
    }

    public class PointTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(
            ITypeDescriptorContext context, 
            Type sourceType) 
            => sourceType == typeof(string);

        public override object ConvertFrom(
            ITypeDescriptorContext context, 
            CultureInfo culture, 
            object value)
        {
            var split = value.ToString().Split(',');
            var x = double.Parse(split[0].Trim().TrimStart('('));
            var y = double.Parse(split[1].Trim().TrimEnd(')'));
            return new Point { X = x, Y = y };
        }
    }

}
