﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace MyQueryable
{
    public class MyQueryProvider : IQueryProvider
    {
        public IQueryable<TElement> CreateQuery<TElement>(Expression expression)
        {
            return new MyQueryable<TElement>(expression);
        }

        public TResult Execute<TResult>(Expression expression)
        {
            List<MyLambdaExpression> lambda = null;
            AnalysisExpression.VisitExpression2(expression, ref lambda);//解析取得表达式数中的表达式
            IEnumerable<Student> enumerable = null;
            //把遍历过来的lambda进行Compile ，使用得到的func进行筛选执行  
            //Compile就是把表达式树变成func
            for (int i = 0; i < lambda.Count; i++)
            {
                //把MyLambdaExpression转成Expression<Func<Student, bool>>类型
                //通过方法Compile()转成委托方法
                Func<Student, bool> func = (lambda[i] as Expression<Func<Student, bool>>).Compile();
                if (enumerable == null)
                    enumerable = Program.StudentArrary.Where(func);//取得IEnumerable
                else
                    enumerable = enumerable.Where(func);
            }
            dynamic obj = enumerable.ToList();//（注意：这个方法的整个处理过程，你可以换成解析sql执行数据库查询，或者生成url然后请求获取数据。）
            return (TResult)obj;
        }

        #region 

        public object Execute(Expression expression)
        {
            return new List<object>();
        }

        public IQueryable CreateQuery(Expression expression)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
    public class MyQueryable<T> : IQueryable<T>
    {
        public MyQueryable()
        {
            _provider = new MyQueryProvider();
            //先赋值一个常量的表达式
            _expression = Expression.Constant(this);
        }

        public MyQueryable(Expression expression)
        {
            _provider = new MyQueryProvider();
            _expression = expression;
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            var result = _provider.Execute<List<T>>(_expression);
            if (result == null)
                yield break;
            foreach (var item in result)
            {
                yield return item;
            }
        }

        #region 

        public Type ElementType
        {
            get { return typeof(T); }
        }

        private Expression _expression;
        public Expression Expression
        {
            get { return _expression; }
        }

        private IQueryProvider _provider;
        public IQueryProvider Provider
        {
            get { return _provider; }
        }

        public IEnumerator GetEnumerator()
        {
            return (Provider.Execute(Expression) as IEnumerable).GetEnumerator();
        }

        #endregion
    }

    //表达式解析
    public static class AnalysisExpression
    {
        public static void VisitExpression(Expression expression)
        {
            switch (expression.NodeType)
            {
                case ExpressionType.Call://执行方法
                    MethodCallExpression method = expression as MethodCallExpression;
                    Console.WriteLine("方法名:" + method.Method.Name);
                    for (int i = 0; i < method.Arguments.Count; i++)
                        VisitExpression(method.Arguments[i]);
                    break;
                case ExpressionType.Lambda://lambda表达式
                    MyLambdaExpression lambda = expression as MyLambdaExpression;
                    VisitExpression(lambda.Body);
                    break;
                case ExpressionType.Equal://相等比较
                case ExpressionType.AndAlso://and条件运算
                    BinaryExpression binary = expression as BinaryExpression;
                    Console.WriteLine("运算符:" + expression.NodeType.ToString());
                    VisitExpression(binary.Left);
                    VisitExpression(binary.Right);
                    break;
                case ExpressionType.Constant://常量值
                    ConstantExpression constant = expression as ConstantExpression;
                    Console.WriteLine("常量值:" + constant.Value.ToString());
                    break;
                case ExpressionType.MemberAccess:
                    MemberExpression Member = expression as MemberExpression;
                    Console.WriteLine("字段名称:{0}，类型:{1}", Member.Member.Name, Member.Type.ToString());
                    break;
                default:
                    Console.Write("UnKnow");
                    break;
            }
        }
        public static void VisitExpression2(Expression expression, ref List<MyLambdaExpression> lambdaOut)
        {
            if (lambdaOut == null)
                lambdaOut = new List<MyLambdaExpression>();
            switch (expression.NodeType)
            {
                case ExpressionType.Call://执行方法
                    MethodCallExpression method = expression as MethodCallExpression;
                    Console.WriteLine("方法名:" + method.Method.Name);
                    for (int i = 0; i < method.Arguments.Count; i++)
                        //继续嵌套添加到ref lambdaOut
                        VisitExpression2(method.Arguments[i], ref lambdaOut);
                    break;
                case ExpressionType.Lambda://lambda表达式
                    MyLambdaExpression lambda = expression as MyLambdaExpression;
                    lambdaOut.Add(lambda);
                    VisitExpression2(lambda.Body, ref lambdaOut);
                    break;
                case ExpressionType.Equal://相等比较
                case ExpressionType.AndAlso://and条件运算
                    BinaryExpression binary = expression as BinaryExpression;
                    Console.WriteLine("运算符:" + expression.NodeType.ToString());
                    VisitExpression2(binary.Left, ref lambdaOut);
                    VisitExpression2(binary.Right, ref lambdaOut);
                    break;
                case ExpressionType.Constant://常量值
                    ConstantExpression constant = expression as ConstantExpression;
                    Console.WriteLine("常量值:" + constant.Value.ToString());
                    break;
                case ExpressionType.MemberAccess:
                    MemberExpression Member = expression as MemberExpression;
                    Console.WriteLine("字段名称:{0}，类型:{1}", Member.Member.Name, Member.Type.ToString());
                    break;
                case ExpressionType.Quote:
                    UnaryExpression Unary = expression as UnaryExpression;
                    VisitExpression2(Unary.Operand, ref lambdaOut);
                    break;
                default:
                    Console.Write("UnKnow");
                    break;
            }
        }
    }
    public class Student
    {
        public string Name { get; set; }

        public int Age { get; set; }

        public string Address { get; set; }

        public string Sex { get; set; }
    }
}
