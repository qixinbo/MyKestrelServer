﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace MyQueryable
{
    class Program
    {
        //构造Student数组
        public static List<Student> StudentArrary = new List<Student>()
        {

                new Student(){Name="农码一生", Age=26, Sex="男", Address="长沙"},
                new Student(){Name="小明", Age=23, Sex="男", Address="岳阳"},
                new Student(){Name="嗨-妹子", Age=25, Sex="女", Address="四川"}
        };

        static void Main(string[] args)
        {
            Func<Student, bool> func = t => t.Name == "农码一生";

            Expression<Func<Student, bool>> expression = t => t.Name == "农码一生" && t.Sex == "男";
            AnalysisExpression.VisitExpression(expression);

            //Func<Student, bool> func = delegate (Student t) {return t.Name == "农码一生";};
            //Expression<Func<Student, bool>> expression = Expression.Lambda<Func<Student, bool>>(Expression.Equal(Expression.Property(
            //    CS$0$0000 = Expression.Parameter(typeof(Student), "t"),(MethodInfo) methodof(Student.get_Name)),Expression.Constant("农码一生", typeof(string)),
            //   false,(MethodInfo) methodof(string.op_Equality)), new ParameterExpression[] { CS$0$0000 });

            //source.Provider.CreateQuery<TSource>(Expression.Call(null, GetMethodInfo<IQueryable<TSource>, Expression<Func<TSource, bool>>, IQueryable<TSource>>(new Func<IQueryable<TSource>, Expression<Func<TSource, bool>>, IQueryable<TSource>>(Queryable.Where<TSource>), source, predicate), arguments));


            var aa = new MyQueryable<Student>();
            var bb = aa.Where(t => t.Name == "农码一生");
            var cc = bb.Where(t => t.Sex == "男");
            var dd = cc.AsEnumerable();
            var ee = cc.ToList();
            foreach (var item in ee)
            {
                Console.WriteLine("筛选后的名称"+item.Name);
            }
            //Expression<Func<Student, bool>> expression = t => t.Name == "农码一生";
            //Func<Student, bool> func = expression.Compile(); 

            Console.ReadKey();
        }
    }
}
