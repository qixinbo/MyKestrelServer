﻿using System;

namespace UnionDemo
{

    class Student : IEquatable<Student>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public int Score { get; set; }

        public bool Equals(Student other)
        {
            //Check whether the compared object is null.
            if (Object.ReferenceEquals(other, null)) return false;

            //Check whether the compared object references the same data.
            if (Object.ReferenceEquals(this, other)) return true;

            //Check whether the Students' properties are equal.
            return Id.Equals(other.Id) && Name.Equals(other.Name) && Class.Equals(other.Class) && Score.Equals(other.Score);
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.
        public override int GetHashCode()
        {
            //Get hash code for the Id field.
            int hashStudentId = Id.GetHashCode();

            //Get hash code for the Name field if it is not null.
            int hashStudentName = Name == null ? 0 : Name.GetHashCode();

            //Get hash code for the Class field if it is not null.
            int hashStudentClass = Class == null ? 0 : Class.GetHashCode();

            //Get hash code for the Score field.
            int hashStudentScore = Score.GetHashCode();

            //Calculate the hash code for the Student.
            return hashStudentId ^ hashStudentName ^ hashStudentClass ^ hashStudentScore;
        }
    }
}
