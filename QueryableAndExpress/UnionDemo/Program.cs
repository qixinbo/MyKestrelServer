﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UnionDemo
{
    class Program
    {
        static void Main(string[] args)
        {

            List<Student> stuList1 = new List<Student>()
            {
                new Student(){Id=1,Name="tiana0",Class="04机制春",Score=100},
                new Student(){Id=2,Name="xiaobo",Class="09计研",Score=80},
                new Student(){Id=3,Name="八戒",Class="09计研",Score=30}
            };

            List<Student> stuList2 = new List<Student>()
            {
                new Student(){Id=1,Name="tiana0",Class="04机制春",Score=100},
                new Student(){Id=2,Name="张三",Class="09计研",Score=100},
                new Student(){Id=1,Name="八戒",Class="09计研",Score=30}
            };

            IEnumerable<Student> unionList = stuList1.Union(stuList2);
            IEnumerable<Student> concatList = stuList1.Concat(stuList2);
            Console.WriteLine("stuList1:Id,Name,Class,Score");
            foreach (var s1 in unionList)
            {
                Console.WriteLine("{0},{1},{2},{3}", s1.Id, s1.Name, s1.Class, s1.Score);
            }
            Console.WriteLine("stuList2:Id,Name,Class,Score");
            foreach (var s2 in concatList)
            {
                Console.WriteLine("{0},{1},{2},{3}", s2.Id, s2.Name, s2.Class, s2.Score);
            }


        }
    }
}
