﻿using System;
using System.Linq.Expressions;

namespace ExpressionTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //ParameterExpression paraLeft = Expression.Parameter(typeof(int), "a");
            //ParameterExpression paraRight = Expression.Parameter(typeof(int), "b");

            //BinaryExpression binaryLeft = Expression.Multiply(paraLeft, paraRight);
            //ConstantExpression conRight = Expression.Constant(2, typeof(int));

            //BinaryExpression binaryBody = Expression.Add(binaryLeft, conRight);

            //Expression<Func<int, int, int>> lambda =
            //    Expression.Lambda<Func<int, int, int>>(binaryBody, paraLeft, paraRight);

            //Func<int, int, int> myLambda = lambda.Compile();

            //int result = myLambda(2, 3);
            //Console.WriteLine("result:" + result.ToString());

            //Console.Read();

            BinaryExpression body = Expression.Add(
       Expression.Constant(2),
       Expression.Constant(3));

            Expression<Func<int>> expression =
                Expression.Lambda<Func<int>>(body, null);

            Func<int> lambda = expression.Compile();

            Console.WriteLine(lambda());

        }
    }
}
