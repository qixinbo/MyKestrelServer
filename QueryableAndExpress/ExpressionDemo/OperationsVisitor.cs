﻿using System.Linq.Expressions;

namespace ExpressionDemo
{
    public class OperationsVisitor : ExpressionVisitor
    {
        public Expression Modify(Expression expression)
        {
            return Visit(expression);
        }

        protected override Expression VisitBinary(BinaryExpression b)
        {
            //如果是加法
            if (b.NodeType == ExpressionType.Add)
            {
                Expression left = this.Visit(b.Left);
                Expression right = this.Visit(b.Right);
                //改为减法
                return Expression.Subtract(left, right);
            }
            return base.VisitBinary(b);
        }
    }

}
