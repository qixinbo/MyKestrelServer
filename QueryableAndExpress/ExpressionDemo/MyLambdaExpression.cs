﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq.Expressions;
using System.Text;

namespace ExpressionDemo
{
    public abstract class MyLambdaExpression : Expression
    {
        private readonly Expression _body;

        internal MyLambdaExpression(Expression body)
        {
            _body = body;
        }

        /// <summary>
        /// Gets the static type of the expression that this <see cref="Expression"/> represents. (Inherited from <see cref="Expression"/>.)
        /// </summary>
        /// <returns>The <see cref="Type"/> that represents the static type of the expression.</returns>
        public sealed override Type Type => TypeCore;

        internal abstract Type TypeCore { get; }

        internal abstract Type PublicType { get; }

        /// <summary>
        /// Returns the node type of this <see cref="Expression"/>. (Inherited from <see cref="Expression"/>.)
        /// </summary>
        /// <returns>The <see cref="ExpressionType"/> that represents this expression.</returns>
        public sealed override ExpressionType NodeType => ExpressionType.Lambda;

        ///// <summary>
        ///// Gets the parameters of the lambda expression.
        ///// </summary>
        //public ReadOnlyCollection<ParameterExpression> Parameters => GetOrMakeParameters();

        /// <summary>
        /// Gets the name of the lambda expression.
        /// </summary>
        /// <remarks>Used for debugging purposes.</remarks>
        public string Name => NameCore;

        internal virtual string NameCore => null;

        /// <summary>
        /// Gets the body of the lambda expression.
        /// </summary>
        public Expression Body => _body;

        /// <summary>
        /// Gets the return type of the lambda expression.
        /// </summary>
        public Type ReturnType => Type.GetType();

        /// <summary>
        /// Gets the value that indicates if the lambda expression will be compiled with
        /// tail call optimization.
        /// </summary>
        public bool TailCall => TailCallCore;

        internal virtual bool TailCallCore => false;

   
        //internal virtual ReadOnlyCollection<ParameterExpression> GetOrMakeParameters()
        //{
        //    throw ContractUtils.Unreachable;
        //}

        //ParameterExpression IParameterProvider.GetParameter(int index) => GetParameter(index);

        //internal virtual ParameterExpression GetParameter(int index)
        //{
        //    throw ContractUtils.Unreachable;
        //}

        //int IParameterProvider.ParameterCount => ParameterCount;

        internal virtual int ParameterCount
        {
            get
            {
                throw new Exception();
            }
        }

        ///// <summary>
        ///// Produces a delegate that represents the lambda expression.
        ///// </summary>
        ///// <returns>A delegate containing the compiled version of the lambda.</returns>
        //public Delegate Compile()
        //{
        //    return Compile(preferInterpretation: false);
        //}

        ///// <summary>
        ///// Produces a delegate that represents the lambda expression.
        ///// </summary>
        ///// <param name="preferInterpretation">A <see cref="bool"/> that indicates if the expression should be compiled to an interpreted form, if available.</param>
        ///// <returns>A delegate containing the compiled version of the lambda.</returns>
        //public Delegate Compile(bool preferInterpretation)
        //{

        //    return new Interpreter.LightCompiler().CompileTop(this).CreateDelegate();

        //}

    }
}
