﻿using System;
using System.Linq.Expressions;

namespace ExpressionDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Expression<Func<int, int, int>> lambda = (a, b) => a + b * 2;
            var operationsVisitor = new OperationsVisitor();
            Expression modifyExpression = operationsVisitor.Modify(lambda);

            Console.WriteLine(modifyExpression.ToString());

        }
    }
}
