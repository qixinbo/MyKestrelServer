﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace LINQProviderDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            ConstantExpression conRight = Expression.Constant("TerryLee", typeof(string));
            // 将我们上面的代码块表达式
            Expression<Func<string>> lambdaExpression = Expression.Lambda<Func<string>>(conRight);
            QueryableData<String> mydata = new QueryableData<String>();

            var result = from d in mydata
                         select d;
            Console.WriteLine(    result.FirstOrDefault());

            //foreach (String item in result)
            //{
            //    Console.WriteLine(item);
            //}
        }
    }
}
