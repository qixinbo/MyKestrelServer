﻿using System.Collections;
using System.Collections.Generic;

namespace LINQProviderDemo
{
    public class MyData<T> : IEnumerable<T> where T : class
    {
        public IEnumerator<T> GetEnumerator()
        {
            return null;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return null;
        }

        // 其它成员
    }

}
