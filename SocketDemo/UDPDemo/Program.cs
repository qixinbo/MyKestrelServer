﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UDPDemo
{
    class Program
    {
        private static Socket sock;
        private static byte[] data;
        private static IPEndPoint iep1;
        static void Main(string[] args)
        {
            sock = new Socket(AddressFamily.InterNetwork,  SocketType.Dgram, ProtocolType.Udp);
            //如果是广播通常这样设定发送地址，一定要与接收端绑定的端口一致！
            iep1 = new IPEndPoint(IPAddress.Broadcast, 9050);
            string hostname = Dns.GetHostName();
            data = Encoding.ASCII.GetBytes(hostname);
            //设置socket，否则程序报错
            sock.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
            Thread t = new Thread(BroadcastMessage);
            t.Start();
            Console.ReadKey();
        }
        private static void BroadcastMessage()
        {
            while (true)
            {
                //向iep1发送广播数据
                sock.SendTo(data, iep1);
                Thread.Sleep(2000);
            }
        }
    }
}
