﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace UDPBroadcastClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Socket sock = new Socket(AddressFamily.InterNetwork,
             SocketType.Dgram, ProtocolType.Udp);
            //获取本机所有IP进行监听
            //IPAddress.Any表示本机ip，换言之，如果服务器绑定此地址，则表示侦听//本机所有ip对应的那个端口（本机可能有多个ip或只有一个ip）
            IPEndPoint iep = new IPEndPoint(IPAddress.Any, 9050);
            //绑定本机端口，用于接收UDP数据包
            sock.Bind(iep);
            //iep可以是任意IP,因为有多网卡情况的存在，在ReceiveFrom后会被赋值为发送端的地址
            EndPoint ep = (EndPoint)iep;
            Console.WriteLine("Ready to receive…");
            byte[] data = new byte[1024];
            //是一个阻塞方法，会一直等待数据，直到接收到数据才会往下执行
            //返回的地址ep是路由器地址，因为是由路由器转发的信息。
            int recv = sock.ReceiveFrom(data, ref ep);
            string stringData = Encoding.ASCII.GetString(data, 0, recv);
            Console.WriteLine("received: {0} from: {1}", stringData, ep.ToString());
            sock.Close();
            Console.ReadKey();

        }
    }
}
