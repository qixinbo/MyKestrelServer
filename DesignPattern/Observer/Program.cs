﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var boilWater = new BoilWater();
            boilWater.AddObserver(() =>
            {
                Console.WriteLine("Water is Boiled");
            });
            boilWater.AddObserver(() =>
            {
                Console.WriteLine("Do Other Things");
            });

            boilWater.Boil();
            Thread.Sleep(100);
        }
    }
    public class BoilWater
    {
        public int waterTemperature = 0;

        //  private event Action actions;
        //委托和事件的区别在于，可以在委托所属的类外面进行添加和删除操作；但是事件不行，事件只能在事件所属的类内部进行添加和删除操作。
        private Action actions;

        public void Boil()
        {
            for (int i = 0; i <=100; i++)
            {
                waterTemperature = i;
                //Thread.Sleep(100);
                if (waterTemperature == 100)
                {
                    actions();
                }
            }
        }

        public void AddObserver(Action runner)
        {
            actions += runner;
        }

        public void RemoveObserver(Action runner)
        {
            actions -= runner;
        }
    }



}
